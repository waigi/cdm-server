package com.vigo.cdm.domain;


import java.util.ArrayList;
import java.util.List;

public class NOfTask{

    private List<String> photoURLStrings = new ArrayList<String>();
    private List<String> texts = new ArrayList<String>();
    private List<String> audioURLStrings = new ArrayList<String>();

    public List<String> getPhotoURLStrings() {
        return photoURLStrings;
    }

    public void setPhotoURLStrings(List<String> photoURLStrings) {
        this.photoURLStrings = photoURLStrings;
    }

    public List<String> getTexts() {
        return texts;
    }

    public void setTexts(List<String> texts) {
        this.texts = texts;
    }

    public List<String> getAudioURLStrings() {
        return audioURLStrings;
    }

    public void setAudioURLStrings(List<String> audioURLStrings) {
        this.audioURLStrings = audioURLStrings;
    }
}
