package com.vigo.cdm.domain;

/**
 * Created by weill on 16/9/5.
 */
public class Text {

    private Integer id;            //文本id
    private String text;     // 文本内容
    private int positionX;     // 文本位置X
    private int positionY;      // 文本位置Y


    public Text(){

    }

    public Text(Integer id, String text, Integer positionX, Integer positionY){
        this.id = id;
        this.text = text;
        if(positionX!=null){
            this.positionX = positionX;
        }
        if(positionY!=null){
            this.positionY = positionY;
        }

    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
