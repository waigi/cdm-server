package com.vigo.cdm.domain;

/**
 * Created by weill on 16/9/5.
 */
public class Photo {

    private Integer id;               //照片id
    private String urlString;     // 照片URL
    private int positionX;     // 照片位置X
    private int positionY;     //照片位置Y

    public Photo(){

    }

    public Photo(String urlString){
        this.urlString = urlString;

    }

    public Photo(Integer id , String urlString, Integer positionX, Integer positionY){
        this.id = id;
        this.urlString = urlString;
        if(positionX!=null){
            this.positionX = positionX;
        }
        if(positionY!=null){
            this.positionY = positionY;
        }
    }


    public String getUrlString() {
        return urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
