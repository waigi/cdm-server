package com.vigo.cdm.domain;

import com.vigo.cdm.domain.util.DomainUtil;
import com.vigo.cdm.web.rest.util.FilePathUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A Promise.
 */

@Document(collection = "promise")
public class Promise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //AccountFamily Id
    private AccountFamily accountFamily;

    //private Task task;

    @Field("sequence_number")
    private int sequenceNumber = 0;

    @Field("promise_time")
    private Date promiseTime; // 承诺日期时间

    @Field("promise_start_time")
    private Date promiseStartTime; // 承诺日期时间

    @Field("promise_content")
    private String promiseContent;

    @Field("promise_status")
    private int promiseStatus = DomainUtil.promiseStatus_notStarted;  // 录音状态 notStarted = 0,started = 1,finished = 2

    @Field("promise_audio")
    private Audio promiseAudio;

    @Field("promise_guardian_audio")
    private Audio promiseGuardianAudio;

    @Field("promise_teacher_audio")
    private Audio promiseTeacherAudio;


    //人格诺
    @Field("personality_promise_status")
    private int personalityPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 人格诺录音状态 notStarted = 0,started = 1,finished = 2

    @Field("personality_start_time")
    private Date personalityStartTime;

    @Field("personality_child_audio")
    private Audio personalityChildAudio;

    @Field("personality_guardian_audio")
    private Audio personalityGuardianAudio;

    @Field("personality_teacher_audio")
    private Audio personalityTeacherAudio;

    // 标签诺
    @Field("tag_promise_status")
    private int tagPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 标签诺录音状态 notStarted = 0,started = 1,finished = 2

    @Field("tag_start_time")
    private Date tagStartTime;

    @Field("tag_child_audio")
    private Audio tagChildAudio;

    @Field("tag_guardian_audio")
    private Audio tagGuardianAudio;

    @Field("tag_teacher_audio")
    private Audio tagTeacherAudio;

    // 资源诺
    @Field("resource_promise_status")
    private int resourcePromiseStatus = DomainUtil.promiseStatus_notStarted;  // 资源诺录音状态 notStarted = 0,started = 1,finished = 2

    @Field("resource_start_time")
    private Date resourceStartTime;

    @Field("resource_child_audio")
    private Audio resourceChildAudio;

    @Field("resource_guardian_audio")
    private Audio resourceGuardianAudio;

    @Field("resource_teacher_audio")
    private Audio resourceTeacherAudio;

    // 禁足诺
    @Field("grounded_promise_status")
    private int groundedPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 禁足诺录音状态 notStarted = 0,started = 1,finished = 2

    @Field("grounded_start_time")
    private Date groundedStartTime;

    @Field("grounded_child_audio")
    private Audio groundedChildAudio;

    @Field("grounded_guardian_audio")
    private Audio groundedGuardianAudio;

    @Field("grounded_teacher_audio")
    private Audio groundedTeacherAudio;

    // 训诫诺
    @Field("criticize_promise_status")
    private int criticizePromiseStatus = DomainUtil.promiseStatus_notStarted;  // 训诫诺录音状态 notStarted = 0,started = 1,finished = 2

    @Field("criticize_start_time")
    private Date criticizeStartTime;

    @Field("criticize_child_audio")
    private Audio criticizeChildAudio;

    @Field("criticize_guardian_audio")
    private Audio criticizeGuardianAudio;

    @Field("criticize_teacher_audio")
    private Audio criticizeTeacherAudio;

    // 家庭大会
    @Field("family_meeting_promise_status")
    private int familyMeetingPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 家庭大会录音状态 notStarted = 0,started = 1,finished = 2

    @Field("family_meeting_audios")
    private List<Audio> familyMeetingAudios;

    @Field("family_meeting_audio_index")
    private int familyMeetingAudioIndex;

    @Field("family_meeting_start_time")
    private Date familyMeetingStartTime;


    @Field("f_tags")
    private List<String> fTags;

    @Field("i_tags")
    private List<String> iTags;

    @Field("k_tags")
    private List<String> kTags;

    @Field("e_tags")
    private List<String> eTags;

    @Field("a_tags")
    private List<String> aTags;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Promise promise = (Promise) o;
        if(promise.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, promise.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Promise{" +
            "id=" + id +
            '}';
    }
    public Promise(){
    }

    public Promise(String id){
        this.id=id;
    }

    public Date getPromiseTime() {
        return promiseTime;
    }

    public void setPromiseTime(Date promiseTime) {
        this.promiseTime = promiseTime;
    }

    public Audio getPromiseAudio() {
        return promiseAudio;
    }

    public void setPromiseAudio(Audio promiseAudio) {
        this.promiseAudio = promiseAudio;
    }

    public int getPersonalityPromiseStatus() {
        return personalityPromiseStatus;
    }

    public void setPersonalityPromiseStatus(int personalityPromiseStatus) {
        this.personalityPromiseStatus = personalityPromiseStatus;
    }

    public Audio getPersonalityChildAudio() {
        return personalityChildAudio;
    }

    public void setPersonalityChildAudio(Audio personalityChildAudio) {
        this.personalityChildAudio = personalityChildAudio;
    }

    public Audio getPersonalityGuardianAudio() {
        return personalityGuardianAudio;
    }

    public void setPersonalityGuardianAudio(Audio personalityGuardianAudio) {
        this.personalityGuardianAudio = personalityGuardianAudio;
    }

    public int getTagPromiseStatus() {
        return tagPromiseStatus;
    }

    public void setTagPromiseStatus(int tagPromiseStatus) {
        this.tagPromiseStatus = tagPromiseStatus;
    }

    public Audio getTagChildAudio() {
        return tagChildAudio;
    }

    public void setTagChildAudio(Audio tagChildAudio) {
        this.tagChildAudio = tagChildAudio;
    }

    public Audio getTagGuardianAudio() {
        return tagGuardianAudio;
    }

    public void setTagGuardianAudio(Audio tagGuardianAudio) {
        this.tagGuardianAudio = tagGuardianAudio;
    }

    public int getResourcePromiseStatus() {
        return resourcePromiseStatus;
    }

    public void setResourcePromiseStatus(int resourcePromiseStatus) {
        this.resourcePromiseStatus = resourcePromiseStatus;
    }

    public Audio getResourceChildAudio() {
        return resourceChildAudio;
    }

    public void setResourceChildAudio(Audio resourceChildAudio) {
        this.resourceChildAudio = resourceChildAudio;
    }

    public Audio getResourceGuardianAudio() {
        return resourceGuardianAudio;
    }

    public void setResourceGuardianAudio(Audio resourceGuardianAudio) {
        this.resourceGuardianAudio = resourceGuardianAudio;
    }

    public int getGroundedPromiseStatus() {
        return groundedPromiseStatus;
    }

    public void setGroundedPromiseStatus(int groundedPromiseStatus) {
        this.groundedPromiseStatus = groundedPromiseStatus;
    }

    public Audio getGroundedChildAudio() {
        return groundedChildAudio;
    }

    public void setGroundedChildAudio(Audio groundedChildAudio) {
        this.groundedChildAudio = groundedChildAudio;
    }

    public Audio getGroundedGuardianAudio() {
        return groundedGuardianAudio;
    }

    public void setGroundedGuardianAudio(Audio groundedGuardianAudio) {
        this.groundedGuardianAudio = groundedGuardianAudio;
    }

    public int getCriticizePromiseStatus() {
        return criticizePromiseStatus;
    }

    public void setCriticizePromiseStatus(int criticizePromiseStatus) {
        this.criticizePromiseStatus = criticizePromiseStatus;
    }

    public Audio getCriticizeChildAudio() {
        return criticizeChildAudio;
    }

    public void setCriticizeChildAudio(Audio criticizeChildAudio) {
        this.criticizeChildAudio = criticizeChildAudio;
    }

    public Audio getCriticizeGuardianAudio() {
        return criticizeGuardianAudio;
    }

    public void setCriticizeGuardianAudio(Audio criticizeGuardianAudio) {
        this.criticizeGuardianAudio = criticizeGuardianAudio;
    }

    public int getFamilyMeetingPromiseStatus() {
        return familyMeetingPromiseStatus;
    }

    public void setFamilyMeetingPromiseStatus(int familyMeetingPromiseStatus) {
        this.familyMeetingPromiseStatus = familyMeetingPromiseStatus;
    }

    public List<Audio> getFamilyMeetingAudios() {
        return familyMeetingAudios;
    }

    public void setFamilyMeetingAudios(List<Audio> familyMeetingAudios) {
        this.familyMeetingAudios = familyMeetingAudios;
    }

    public List<String> getfTags() {
        return fTags;
    }

    public void setfTags(List<String> fTags) {
        this.fTags = fTags;
    }

    public List<String> getiTags() {
        return iTags;
    }

    public void setiTags(List<String> iTags) {
        this.iTags = iTags;
    }

    public List<String> getkTags() {
        return kTags;
    }

    public void setkTags(List<String> kTags) {
        this.kTags = kTags;
    }

    public List<String> geteTags() {
        return eTags;
    }

    public void seteTags(List<String> eTags) {
        this.eTags = eTags;
    }

    public List<String> getaTags() {
        return aTags;
    }

    public void setaTags(List<String> aTags) {
        this.aTags = aTags;
    }

    public Audio getPersonalityTeacherAudio() {
        return personalityTeacherAudio;
    }

    public void setPersonalityTeacherAudio(Audio personalityTeacherAudio) {
        this.personalityTeacherAudio = personalityTeacherAudio;
    }

    public Audio getTagTeacherAudio() {
        return tagTeacherAudio;
    }

    public void setTagTeacherAudio(Audio tagTeacherAudio) {
        this.tagTeacherAudio = tagTeacherAudio;
    }

    public Audio getResourceTeacherAudio() {
        return resourceTeacherAudio;
    }

    public void setResourceTeacherAudio(Audio resourceTeacherAudio) {
        this.resourceTeacherAudio = resourceTeacherAudio;
    }

    public Audio getGroundedTeacherAudio() {
        return groundedTeacherAudio;
    }

    public void setGroundedTeacherAudio(Audio groundedTeacherAudio) {
        this.groundedTeacherAudio = groundedTeacherAudio;
    }

    public Audio getCriticizeTeacherAudio() {
        return criticizeTeacherAudio;
    }

    public void setCriticizeTeacherAudio(Audio criticizeTeacherAudio) {
        this.criticizeTeacherAudio = criticizeTeacherAudio;
    }

    public Audio getPromiseGuardianAudio() {
        return promiseGuardianAudio;
    }

    public void setPromiseGuardianAudio(Audio promiseGuardianAudio) {
        this.promiseGuardianAudio = promiseGuardianAudio;
    }

    public String getPromiseContent() {
        return promiseContent;
    }

    public void setPromiseContent(String promiseContent) {
        this.promiseContent = promiseContent;
    }

    public Audio getPromiseTeacherAudio() {
        return promiseTeacherAudio;
    }

    public void setPromiseTeacherAudio(Audio promiseTeacherAudio) {
        this.promiseTeacherAudio = promiseTeacherAudio;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    /*public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }*/

    public int getPromiseStatus() {
        return promiseStatus;
    }

    public void setPromiseStatus(int promiseStatus) {
        this.promiseStatus = promiseStatus;
    }

    public int getFamilyMeetingAudioIndex() {
        return familyMeetingAudioIndex;
    }

    public void setFamilyMeetingAudioIndex(int familyMeetingAudioIndex) {
        this.familyMeetingAudioIndex = familyMeetingAudioIndex;
    }

    public Date getPersonalityStartTime() {
        return personalityStartTime;
    }

    public void setPersonalityStartTime(Date personalityStartTime) {
        this.personalityStartTime = personalityStartTime;
    }

    public Date getTagStartTime() {
        return tagStartTime;
    }

    public void setTagStartTime(Date tagStartTime) {
        this.tagStartTime = tagStartTime;
    }

    public Date getResourceStartTime() {
        return resourceStartTime;
    }

    public void setResourceStartTime(Date resourceStartTime) {
        this.resourceStartTime = resourceStartTime;
    }

    public Date getGroundedStartTime() {
        return groundedStartTime;
    }

    public void setGroundedStartTime(Date groundedStartTime) {
        this.groundedStartTime = groundedStartTime;
    }

    public Date getCriticizeStartTime() {
        return criticizeStartTime;
    }

    public void setCriticizeStartTime(Date criticizeStartTime) {
        this.criticizeStartTime = criticizeStartTime;
    }

    public Date getFamilyMeetingStartTime() {
        return familyMeetingStartTime;
    }

    public void setFamilyMeetingStartTime(Date familyMeetingStartTime) {
        this.familyMeetingStartTime = familyMeetingStartTime;
    }

    public Date getPromiseStartTime() {
        return promiseStartTime;
    }

    public void setPromiseStartTime(Date promiseStartTime) {
        this.promiseStartTime = promiseStartTime;
    }
}
