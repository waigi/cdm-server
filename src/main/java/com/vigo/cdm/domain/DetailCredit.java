package com.vigo.cdm.domain;

import java.io.Serializable;

/**
 * Created by weill on 16/12/14.
 */
public class DetailCredit implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer type;
    private Integer rank;


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
