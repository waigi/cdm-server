package com.vigo.cdm.domain;

/**
 * Created by weill on 16/9/5.
 */
public class Audio {

    private Integer id;
    private String audioURLString;     // 录音URL
    private Integer audioLength;       // 录音长度，单位为秒
    private int positionX;
    private int positionY;

    public Audio(){

    }
    public Audio(String audioURLString, Integer audioLength){
        this.audioURLString = audioURLString;
        this.audioLength = audioLength;
    }
    public Audio(String audioURLString, Integer audioLength,Integer positionX, Integer positionY){
        this.audioURLString = audioURLString;
        this.audioLength = audioLength;
        if(positionX !=null){
           this.positionX = positionX;
        }
        if(positionY !=null){
            this.positionY = positionY;
        }
    }
    public Audio(Integer id, String audioURLString, Integer audioLength,Integer positionX, Integer positionY){
        this.id = id;
        this.audioURLString = audioURLString;
        this.audioLength = audioLength;
        if(positionX !=null){
            this.positionX = positionX;
        }
        if(positionY !=null){
            this.positionY = positionY;
        }
    }
    public Audio(String audioURLString){
        this.audioURLString = audioURLString;
    }
    public Integer getAudioLength() {
        return audioLength;
    }

    public void setAudioLength(Integer audioLength) {
        this.audioLength = audioLength;
    }

    public String getAudioURLString() {
        return audioURLString;
    }

    public void setAudioURLString(String audioURLString) {
        this.audioURLString = audioURLString;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
