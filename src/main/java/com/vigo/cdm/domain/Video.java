package com.vigo.cdm.domain;

/**
 * Created by weill on 16/9/5.
 */
public class Video {



    private Integer id;
    private String urlString;     // 录音URL
    private String thumbnailURLString;     // 视频预览图
    private Integer length;       // 录音长度，单位为秒
    private int positionX;
    private int positionY;

    public Video(){

    }
    public Video(String urlString, String thumbnailURLString){
        this.urlString = urlString;
        this.thumbnailURLString = thumbnailURLString;
    }

    public Video(Integer id , String urlString, String thumbnailURLString, Integer positionX, Integer positionY){
        this.id = id;
        this.urlString = urlString;
        this.thumbnailURLString = thumbnailURLString;
        if(positionX!=null){
            this.positionX = positionX;
        }
        if(positionY!=null){
            this.positionY = positionY;
        }
    }

    public String getUrlString() {
        return urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public String getThumbnailURLString() {
        return thumbnailURLString;
    }

    public void setThumbnailURLString(String thumbnailURLString) {
        this.thumbnailURLString = thumbnailURLString;
    }


    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }


    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
