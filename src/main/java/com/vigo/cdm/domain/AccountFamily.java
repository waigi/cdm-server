package com.vigo.cdm.domain;

import com.vigo.cdm.web.rest.dto.AccountFamilyDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Objects;

/**
 * A AccountFamily.
 */

@Document(collection = "account_family")
public class AccountFamily implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String name;

    @Field("description")
    private String description;

    @Field("student_avatar_url")
    private String studentAvatarURL;

    @Field("promise_sequence_number")
    private int promiseSequenceNumber = 0;

    @Field("family_activity_sequence_number")
    private int familyActivitySequenceNumber = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountFamily accountFamily = (AccountFamily) o;
        if(accountFamily.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, accountFamily.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AccountFamily{" +
            "id=" + id +
            ", description='" + description + "'" +
            '}';
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AccountFamily(AccountFamilyDTO accountFamilyDTO){

        this.id=accountFamilyDTO.getId();
        this.description = accountFamilyDTO.getDescription();
        this.name = accountFamilyDTO.getName();
    }

    /*public AccountFamily(String id){
        this.id = id;
    }*/
   public AccountFamily(){}

    public AccountFamily(String id){
        this.id = id;
    }


    public int getPromiseSequenceNumber() {
        return promiseSequenceNumber;
    }

    public void setPromiseSequenceNumber(int promiseSequenceNumber) {
        this.promiseSequenceNumber = promiseSequenceNumber;
    }

    public int getFamilyActivitySequenceNumber() {
        return familyActivitySequenceNumber;
    }

    public void setFamilyActivitySequenceNumber(int familyActivitySequenceNumber) {
        this.familyActivitySequenceNumber = familyActivitySequenceNumber;
    }


    public String getStudentAvatarURL() {
        return studentAvatarURL;
    }

    public void setStudentAvatarURL(String studentAvatarURL) {
        this.studentAvatarURL = studentAvatarURL;
    }
}
