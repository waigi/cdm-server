package com.vigo.cdm.domain.util;

/**
 * Created by weill on 16/9/5.
 */
public class DomainUtil {

    public static final int promiseStatus_notStarted = 0;
    public static final int promiseStatus_started = 1;
    public static final int promiseStatus_finished = 2;

    //public static final int teacher_type_course = 0;
    public static final int teacher_type_growing = 0;       //成长师
    public static final int teacher_type_course_senior = 1;     //高级课程师
    public static final int teacher_type_course_secondary = 2;      //中级课程师
    public static final int teacher_type_course_elementary = 3;     //初级课程师
    public static final int teacher_type_supervisor = 4;        //督导师

    public static final int task_category_initial = -1;
    public static final int task_category_study = 0;
    public static final int task_category_read = 1;
    public static final int task_category_skill = 2;
    public static final int task_category_rest = 3;
    public static final int task_category_diet = 4;
    public static final int task_category_practice = 5;
    public static final int task_category_entertainment = 6;
    public static final int task_category_friend = 7;
    public static final int task_category_dedication = 8;
    public static final int task_category_family = 9;
    public static final int task_category_promise = 10;
    public static final int task_category_introspection = 11;



}
