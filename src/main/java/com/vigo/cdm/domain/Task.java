package com.vigo.cdm.domain;

import com.vigo.cdm.domain.util.DomainUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A Task.
 */

@Document(collection = "task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("description")
    private String description;

    private Promise promise;

    //AccountFamily Id
    private AccountFamily accountFamily;

    @Field("task_category")
    private int taskCategory = DomainUtil.task_category_initial; //任务事况(内化-- 0:学习, 1:阅读, 2:技艺;  平衡-- 3:休息, 4:饮食, 5:修行;  外行-- 6:娱乐, 7:交友, 8:奉献)

    private Double discount = 1.0;

    //S
    @Field("planned_start_time")
    private Date plannedStartTime;
    @Field("planned_end_time")
    private Date plannedEndTime;
    @Field("actual_start_time")
    private Date actualStartTime;
    @Field("actual_end_time")
    private Date actualEndTime;

    //N
    @Field("n_photos")
    private List<Photo> nPhotos;
    @Field("n_texts")
    private List<Text> nTexts;
    @Field("n_audios")
    private List<Audio> nAudios;
    @Field("n_videos")
    private List<Video> nVideos;
    //private List<String> nVideoThumbnailURLStrings;

    //J
    @Field("j_photos")
    private List<Photo> jPhotos;
    @Field("j_texts")
    private List<Text> jTexts;
    @Field("j_audios")
    private List<Audio> jAudios;
    @Field("j_videos")
    private List<Video> jVideos;
    //private List<String> jVideoThumbnailURLStrings;
    @Field("j_demerits")
    private List<String> jDemerits;

    //P
    @Field("p_guardian_text")
    private String pGuardianText;
    @Field("p_self_text")
    private String pSelfText;
    @Field("p_teacher_text")
    private String pTeacherText;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    public Task(){

    }

    public Task(String id){
        this.id = id;
    }

    public Task(String id,AccountFamily accountFamily){
        this.id = id;
        this.accountFamily = accountFamily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Task task = (Task) o;
        if(task.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, task.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + id +
            ", description='" + description + "'" +
            '}';
    }



    public Date getPlannedStartTime() {
        return plannedStartTime;
    }

    public void setPlannedStartTime(Date plannedStartTime) {
        this.plannedStartTime = plannedStartTime;
    }

    public Date getPlannedEndTime() {
        return plannedEndTime;
    }

    public void setPlannedEndTime(Date plannedEndTime) {
        this.plannedEndTime = plannedEndTime;
    }

    public Date getActualStartTime() {
        return actualStartTime;
    }

    public void setActualStartTime(Date actualStartTime) {
        this.actualStartTime = actualStartTime;
    }

    public void setActualEndTime(Date actualEndTime) {
        this.actualEndTime = actualEndTime;
    }

    public Date getActualEndTime() {
        return actualEndTime;
    }


    public List<String> getjDemerits() {
        return jDemerits;
    }

    public void setjDemerits(List<String> jDemerits) {
        this.jDemerits = jDemerits;
    }

    public String getpGuardianText() {
        return pGuardianText;
    }

    public void setpGuardianText(String pGuardianText) {
        this.pGuardianText = pGuardianText;
    }

    public String getpSelfText() {
        return pSelfText;
    }

    public void setpSelfText(String pSelfText) {
        this.pSelfText = pSelfText;
    }

    public String getpTeacherText() {
        return pTeacherText;
    }

    public void setpTeacherText(String pTeacherText) {
        this.pTeacherText = pTeacherText;
    }

    public int getTaskCategory() {
        return taskCategory;
    }

    public void setTaskCategory(int taskCategory) {
        this.taskCategory = taskCategory;
    }

    public List<Audio> getnAudios() {
        return nAudios;
    }

    public void setnAudios(List<Audio> nAudios) {
        this.nAudios = nAudios;
    }

    public List<Audio> getjAudios() {
        return jAudios;
    }

    public void setjAudios(List<Audio> jAudios) {
        this.jAudios = jAudios;
    }

    public List<Video> getnVideos() {
        return nVideos;
    }

    public void setnVideos(List<Video> nVideos) {
        this.nVideos = nVideos;
    }

    public List<Video> getjVideos() {
        return jVideos;
    }

    public void setjVideos(List<Video> jVideos) {
        this.jVideos = jVideos;
    }

    public List<Photo> getnPhotos() {
        return nPhotos;
    }

    public void setnPhotos(List<Photo> nPhotos) {
        this.nPhotos = nPhotos;
    }

    public List<Text> getnTexts() {
        return nTexts;
    }

    public void setnTexts(List<Text> nTexts) {
        this.nTexts = nTexts;
    }

    public List<Photo> getjPhotos() {
        return jPhotos;
    }

    public void setjPhotos(List<Photo> jPhotos) {
        this.jPhotos = jPhotos;
    }

    public List<Text> getjTexts() {
        return jTexts;
    }

    public void setjTexts(List<Text> jTexts) {
        this.jTexts = jTexts;
    }

    public Promise getPromise() {
        return promise;
    }

    public void setPromise(Promise promise) {
        this.promise = promise;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
