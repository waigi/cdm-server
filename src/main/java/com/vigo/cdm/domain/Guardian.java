package com.vigo.cdm.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Guardian.
 */

@Document(collection = "guardian")
public class Guardian implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    private String relationship;

    @Field("photo_url")
    private String photoURL;

    private AccountFamily accountFamily;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Guardian guardian = (Guardian) o;
        if(guardian.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, guardian.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Guardian{" +
            "id=" + id +
            ", name='" + name + "'" +
            '}';
    }
    public Guardian(){

    }
    public Guardian(String id, String name, String relationship, String accountFamilyId){
        this.id = id;
        this.name = name;
        this.relationship = relationship;
        AccountFamily account_Family = new AccountFamily();
        account_Family.setId(accountFamilyId);
        this.accountFamily = account_Family;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }
}
