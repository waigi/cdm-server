package com.vigo.cdm.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A Credit.
 */

@Document(collection = "credit")
public class Credit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("review")
    private String review;
    @Field("grader_type")
    private Integer graderType;  //0,1,2自亲师
    @Field("credit_type")
    private Integer creditType;
    @Field("f_total")
    private Integer fTotal;
    @Field("i_total")
    private Integer iTotal;
    @Field("k_total")
    private Integer kTotal;
    @Field("e_total")
    private Integer aTotal;
    @Field("a_total")
    private Integer eTotal;

    private Double discount;

    private Double weight;
    @Field("task_date")
    private Date taskDate;
    //task id
    private Task task;
    @Field("f_details")
    private List<DetailCredit> fDetails;
    @Field("i_details")
    private List<DetailCredit> iDetails;
    @Field("k_details")
    private List<DetailCredit> kDetails;
    @Field("e_details")
    private List<DetailCredit> eDetails;
    @Field("a_details")
    private List<DetailCredit> aDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Credit credit = (Credit) o;
        if(credit.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, credit.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Credit{" +
            "id=" + id +
            ", review='" + review + "'" +
            '}';
    }

    public Integer getCreditType() {
        return creditType;
    }

    public void setCreditType(Integer creditType) {
        this.creditType = creditType;
    }

    public Integer getfTotal() {
        return fTotal;
    }

    public void setfTotal(Integer fTotal) {
        this.fTotal = fTotal;
    }

    public Integer getiTotal() {
        return iTotal;
    }

    public void setiTotal(Integer iTotal) {
        this.iTotal = iTotal;
    }

    public Integer getkTotal() {
        return kTotal;
    }

    public void setkTotal(Integer kTotal) {
        this.kTotal = kTotal;
    }

    public Integer getaTotal() {
        return aTotal;
    }

    public void setaTotal(Integer aTotal) {
        this.aTotal = aTotal;
    }

    public Integer geteTotal() {
        return eTotal;
    }

    public void seteTotal(Integer eTotal) {
        this.eTotal = eTotal;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
    }

    public List<DetailCredit> getfDetails() {
        return fDetails;
    }

    public void setfDetails(List<DetailCredit> fDetails) {
        this.fDetails = fDetails;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public List<DetailCredit> getiDetails() {
        return iDetails;
    }

    public void setiDetails(List<DetailCredit> iDetails) {
        this.iDetails = iDetails;
    }

    public List<DetailCredit> getkDetails() {
        return kDetails;
    }

    public void setkDetails(List<DetailCredit> kDetails) {
        this.kDetails = kDetails;
    }

    public List<DetailCredit> geteDetails() {
        return eDetails;
    }

    public void seteDetails(List<DetailCredit> eDetails) {
        this.eDetails = eDetails;
    }

    public List<DetailCredit> getaDetails() {
        return aDetails;
    }

    public void setaDetails(List<DetailCredit> aDetails) {
        this.aDetails = aDetails;
    }

    public Integer getGraderType() {
        return graderType;
    }

    public void setGraderType(Integer graderType) {
        this.graderType = graderType;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }


}
