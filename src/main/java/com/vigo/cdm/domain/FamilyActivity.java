package com.vigo.cdm.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A FamilyActivity.
 */

@Document(collection = "family_activity")
public class FamilyActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    //AccountFamily Id
    private AccountFamily accountFamily;

    //Task Id
    private Task task;

    private String name;

    @Field("family_activity_time")
    private Date familyActivityTime;

    @Field("sequence_number")
    private int sequenceNumber = 0;

    @Field("pre_activity_audios")
    private List<Audio> preActivityAudios;

    private List<Audio> audios;

    private List<Video> videos;

    private List<Text> texts;

    private List<Photo> photos;

    @Field("pre_activity_audio_index")
    private int preActivityAudioIndex;

    @Field("audio_index")
    private int audioIndex;

    @Field("video_index")
    private int videoIndex;

    @Field("text_index")
    private int textIndex;

    @Field("photo_index")
    private int photoIndex;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyActivity familyActivity = (FamilyActivity) o;
        if(familyActivity.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, familyActivity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FamilyActivity{" +
            "id=" + id +
            '}';
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Date getFamilyActivityTime() {
        return familyActivityTime;
    }

    public void setFamilyActivityTime(Date familyActivityTime) {
        this.familyActivityTime = familyActivityTime;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public List<Audio> getPreActivityAudios() {
        return preActivityAudios;
    }

    public void setPreActivityAudios(List<Audio> preActivityAudios) {
        this.preActivityAudios = preActivityAudios;
    }

    public List<Audio> getAudios() {
        return audios;
    }

    public void setAudios(List<Audio> audios) {
        this.audios = audios;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }



    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    public List<Text> getTexts() {
        return texts;
    }

    public void setTexts(List<Text> texts) {
        this.texts = texts;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public int getPreActivityAudioIndex() {
        return preActivityAudioIndex;
    }

    public void setPreActivityAudioIndex(int preActivityAudioIndex) {
        this.preActivityAudioIndex = preActivityAudioIndex;
    }

    public int getAudioIndex() {
        return audioIndex;
    }

    public void setAudioIndex(int audioIndex) {
        this.audioIndex = audioIndex;
    }

    public int getVideoIndex() {
        return videoIndex;
    }

    public void setVideoIndex(int videoIndex) {
        this.videoIndex = videoIndex;
    }

    public int getTextIndex() {
        return textIndex;
    }

    public void setTextIndex(int textIndex) {
        this.textIndex = textIndex;
    }

    public int getPhotoIndex() {
        return photoIndex;
    }

    public void setPhotoIndex(int photoIndex) {
        this.photoIndex = photoIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
