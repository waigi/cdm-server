package com.vigo.cdm.web.rest.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * A DTO representing a AccountFamily, with his authorities.
 */
public class AccountFamilyInfoDTO {

    private List<TeacherInfoDTO> teachers = new ArrayList<TeacherInfoDTO>();
    private List<GuardianInfoDTO> guardians = new ArrayList<GuardianInfoDTO>();


    public List<TeacherInfoDTO> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<TeacherInfoDTO> teachers) {
        this.teachers = teachers;
    }

    public List<GuardianInfoDTO> getGuardians() {
        return guardians;
    }

    public void setGuardians(List<GuardianInfoDTO> guardians) {
        this.guardians = guardians;
    }
}
