/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.vigo.cdm.web.rest.dto;
