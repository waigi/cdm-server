package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.AccountFamily;

import java.util.ArrayList;
import java.util.List;

/**
 * A DTO representing a AccountFamily, with his authorities.
 */
public class AccountFamilyDTO {

    private String id;

    private String name;

    private String description;


    private StudentDTO child;

    private List<GuardianDTO> guardianDTOList = new ArrayList<GuardianDTO>();



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<GuardianDTO> getGuardianDTOList() {
        return guardianDTOList;
    }

    public void setGuardianDTOList(List<GuardianDTO> guardianDTOList) {
        this.guardianDTOList = guardianDTOList;
    }

    public StudentDTO getChild() {
        return child;
    }

    public void setChild(StudentDTO child) {
        this.child = child;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
