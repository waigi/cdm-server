package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.domain.Teacher;
import com.vigo.cdm.domain.User;
import com.vigo.cdm.repository.UserRepository;
import com.vigo.cdm.service.TeacherService;
import com.vigo.cdm.service.UserService;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Teacher.
 */
@RestController
@RequestMapping("/api")
public class TeacherResource {

    private final Logger log = LoggerFactory.getLogger(TeacherResource.class);

    @Inject
    private TeacherService teacherService;

    @Inject
    private UserService userService;

    @Inject
    private UserRepository userRepository;
    /**
     * POST  /teachers : Create a new teacher.
     *
     * @param teacher the teacher to create
     * @return the ResponseEntity with status 201 (Created) and with body the new teacher, or with status 400 (Bad Request) if the teacher has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/teachers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Teacher> createTeacher(@RequestBody Teacher teacher) throws URISyntaxException {
        log.debug("REST request to save Teacher : {}", teacher);
        if (teacher.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("teacher", "idexists", "A new teacher cannot already have an ID")).body(null);
        }
        if (userRepository.findOneByLogin(teacher.getName().toLowerCase()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use"))
                .body(null);
        }

        Teacher result = teacherService.save(teacher);
        User user = userService.createUser(teacher.getName(), "ROLE_TEACHER", "123456", null, null, result);
        return ResponseEntity.created(new URI("/api/teachers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("teacher", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /teachers : Updates an existing teacher.
     *
     * @param teacher the teacher to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated teacher,
     * or with status 400 (Bad Request) if the teacher is not valid,
     * or with status 500 (Internal Server Error) if the teacher couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/teachers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Teacher> updateTeacher(@RequestBody Teacher teacher) throws URISyntaxException {
        log.debug("REST request to update Teacher : {}", teacher);
        if (teacher.getId() == null) {
            return createTeacher(teacher);
        }
        Teacher result = teacherService.save(teacher);
        //User user = userService.findOneByTeacherId(result.getId());
        //user.setTeacher(result);
        //userRepository.save(user);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("teacher", teacher.getId().toString()))
            .body(result);
    }

    /**
     * GET  /teachers : get all the teachers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of teachers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/teachers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Teacher>> getAllTeachers(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Teachers");
        Page<Teacher> page = teacherService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/teachers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        /*Teacher teacher = new Teacher();
        teacher.setName("name");
        List<AccountFamily> AccountFamilies = new ArrayList<AccountFamily>();
        AccountFamily accountFamily = new AccountFamily();
        accountFamily.setName("name1");
        accountFamily.setId("1");
        AccountFamily accountFamily2 = new AccountFamily();
        accountFamily2.setName("name2");
        AccountFamilies.add(accountFamily);
        AccountFamilies.add(accountFamily2);

        teacher.setAccountFamilies(AccountFamilies);
        teacher = teacherService.save(teacher);

        List<AccountFamily> accountFamilies2 = teacher.getAccountFamilies();
        AccountFamily accountFamily3 = new AccountFamily();
        accountFamily3.setName("name3");
        accountFamily3.setId("1");
        accountFamilies2.add(accountFamily3);
        teacher.setAccountFamilies(accountFamilies2);
        teacherService.save(teacher);
        return null;*/
    }

    /**
     * GET  /teachers : get all the teachers.
     *
     * @param accountFamilyId the accountFamily id
     * @return the ResponseEntity with status 200 (OK) and the list of teachers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/teachers/byAccountFamilyId",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Teacher>> getTeachersByAccountFamilyId(@RequestParam(value = "accountFamily_id", required = true)String accountFamilyId)
        throws URISyntaxException {
        log.debug("REST request to get a page of Teachers");
        List<Teacher> teachers = teacherService.findTeachersByAccountFamilyId(accountFamilyId);
        return new ResponseEntity<>(teachers, HttpStatus.OK);
    }

    /**
     * GET  /teachers/:id : get the "id" teacher.
     *
     * @param id the id of the teacher to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the teacher, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/teachers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Teacher> getTeacher(@PathVariable String id) {
        log.debug("REST request to get Teacher : {}", id);
        Teacher teacher = teacherService.findOne(id);
        return Optional.ofNullable(teacher)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /teachers/:id : delete the "id" teacher.
     *
     * @param id the id of the teacher to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/teachers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTeacher(@PathVariable String id) {
        log.debug("REST request to delete Teacher : {}", id);
        teacherService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("teacher", id.toString())).build();
    }

}
