package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.Credit;
import com.vigo.cdm.domain.DetailCredit;
import com.vigo.cdm.domain.Task;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

/**
 * Created by weill on 16/12/14.
 */
public class CreditDTO {

    private String id;

    private String review;

    private Integer graderType;

    private Integer creditType;

    private Double discount;

    private Integer fTotal;

    private Integer iTotal;

    private Integer kTotal;

    private Integer aTotal;

    private Integer eTotal;

    private Double weight;

    private Long taskDate;
    //task id
    private TaskDTO task;


    private List<DetailCredit> fDetails;

    private List<DetailCredit> iDetails;

    private List<DetailCredit> kDetails;

    private List<DetailCredit> eDetails;

    private List<DetailCredit> aDetails;

    public CreditDTO(){

    }

    public CreditDTO(Credit credit, TaskDTO taskDTO){
        this.id = credit.getId();
        this.task = taskDTO;
        this.weight = credit.getWeight();
        this.graderType = credit.getGraderType();
        this.fDetails = credit.getfDetails();
        this.iDetails = credit.getiDetails();
        this.kDetails = credit.getkDetails();
        this.eDetails = credit.geteDetails();
        this.aDetails = credit.getaDetails();
        this.discount = credit.getDiscount();
        this.fTotal = credit.getfTotal();
        this.iTotal = credit.getiTotal();
        this.kTotal = credit.getkTotal();
        this.eTotal = credit.geteTotal();
        this.aTotal = credit.getaTotal();
        this.creditType = credit.getCreditType();
        this.review = credit.getReview();
        if(credit.getTaskDate()!=null){
            this.taskDate = credit.getTaskDate().getTime();
        }
    }

    public CreditDTO(Credit credit){
        this.id = credit.getId();
        this.task = new TaskDTO(credit.getTask());
        this.weight = credit.getWeight();
        this.graderType = credit.getGraderType();
        this.fDetails = credit.getfDetails();
        this.iDetails = credit.getiDetails();
        this.kDetails = credit.getkDetails();
        this.eDetails = credit.geteDetails();
        this.aDetails = credit.getaDetails();
        this.discount = credit.getDiscount();
        this.fTotal = credit.getfTotal();
        this.iTotal = credit.getiTotal();
        this.kTotal = credit.getkTotal();
        this.eTotal = credit.geteTotal();
        this.aTotal = credit.getaTotal();
        this.creditType = credit.getCreditType();
        this.review = credit.getReview();
        if(credit.getTaskDate()!=null){
            this.taskDate = credit.getTaskDate().getTime();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public List<DetailCredit> getaDetails() {
        return aDetails;
    }

    public void setaDetails(List<DetailCredit> aDetails) {
        this.aDetails = aDetails;
    }

    public List<DetailCredit> geteDetails() {
        return eDetails;
    }

    public void seteDetails(List<DetailCredit> eDetails) {
        this.eDetails = eDetails;
    }

    public List<DetailCredit> getkDetails() {
        return kDetails;
    }

    public void setkDetails(List<DetailCredit> kDetails) {
        this.kDetails = kDetails;
    }

    public List<DetailCredit> getiDetails() {
        return iDetails;
    }

    public void setiDetails(List<DetailCredit> iDetails) {
        this.iDetails = iDetails;
    }

    public List<DetailCredit> getfDetails() {
        return fDetails;
    }

    public void setfDetails(List<DetailCredit> fDetails) {
        this.fDetails = fDetails;
    }

    public TaskDTO getTask() {
        return task;
    }

    public void setTask(TaskDTO task) {
        this.task = task;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Long getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Long taskDate) {
        this.taskDate = taskDate;
    }

    public Integer geteTotal() {
        return eTotal;
    }

    public void seteTotal(Integer eTotal) {
        this.eTotal = eTotal;
    }

    public Integer getaTotal() {
        return aTotal;
    }

    public void setaTotal(Integer aTotal) {
        this.aTotal = aTotal;
    }

    public Integer getkTotal() {
        return kTotal;
    }

    public void setkTotal(Integer kTotal) {
        this.kTotal = kTotal;
    }

    public Integer getiTotal() {
        return iTotal;
    }

    public void setiTotal(Integer iTotal) {
        this.iTotal = iTotal;
    }

    public Integer getfTotal() {
        return fTotal;
    }

    public void setfTotal(Integer fTotal) {
        this.fTotal = fTotal;
    }

    public Integer getCreditType() {
        return creditType;
    }

    public void setCreditType(Integer creditType) {
        this.creditType = creditType;
    }

    public Integer getGraderType() {
        return graderType;
    }

    public void setGraderType(Integer graderType) {
        this.graderType = graderType;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
