package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.domain.Guardian;
import com.vigo.cdm.domain.Student;
import com.vigo.cdm.domain.Teacher;

import java.util.ArrayList;
import java.util.List;

/**
 * A DTO representing a AccountFamily, with his authorities.
 */
public class AccountFamilyMemberDTO {

    private AccountFamily accountFamily;
    private Student student;
    private List<Teacher> teachers;
    private List<Guardian> guardians;

    public AccountFamilyMemberDTO(AccountFamily accountFamily, Student student, List<Guardian> guardians, List<Teacher> teachers){
        this.accountFamily = accountFamily;
        this.student = student;
        this.guardians = guardians;
        this.teachers = teachers;
    }

    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Guardian> getGuardians() {
        return guardians;
    }

    public void setGuardians(List<Guardian> guardians) {
        this.guardians = guardians;
    }
}
