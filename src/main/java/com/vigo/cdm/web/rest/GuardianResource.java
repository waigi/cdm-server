package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.Guardian;
import com.vigo.cdm.service.GuardianService;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Guardian.
 */
@RestController
@RequestMapping("/api")
public class GuardianResource {

    private final Logger log = LoggerFactory.getLogger(GuardianResource.class);
        
    @Inject
    private GuardianService guardianService;
    
    /**
     * POST  /guardians : Create a new guardian.
     *
     * @param guardian the guardian to create
     * @return the ResponseEntity with status 201 (Created) and with body the new guardian, or with status 400 (Bad Request) if the guardian has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/guardians",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Guardian> createGuardian(@RequestBody Guardian guardian) throws URISyntaxException {
        log.debug("REST request to save Guardian : {}", guardian);
        if (guardian.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("guardian", "idexists", "A new guardian cannot already have an ID")).body(null);
        }
        Guardian result = guardianService.save(guardian);
        return ResponseEntity.created(new URI("/api/guardians/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("guardian", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /guardians : Updates an existing guardian.
     *
     * @param guardian the guardian to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated guardian,
     * or with status 400 (Bad Request) if the guardian is not valid,
     * or with status 500 (Internal Server Error) if the guardian couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/guardians",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Guardian> updateGuardian(@RequestBody Guardian guardian) throws URISyntaxException {
        log.debug("REST request to update Guardian : {}", guardian);
        if (guardian.getId() == null) {
            return createGuardian(guardian);
        }
        Guardian result = guardianService.save(guardian);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("guardian", guardian.getId().toString()))
            .body(result);
    }

    /**
     * GET  /guardians : get all the guardians.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of guardians in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/guardians",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Guardian>> getAllGuardians(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Guardians");
        Page<Guardian> page = guardianService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/guardians");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /guardians/:id : get the "id" guardian.
     *
     * @param id the id of the guardian to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the guardian, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/guardians/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Guardian> getGuardian(@PathVariable String id) {
        log.debug("REST request to get Guardian : {}", id);
        Guardian guardian = guardianService.findOne(id);
        return Optional.ofNullable(guardian)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /guardians/:id : delete the "id" guardian.
     *
     * @param id the id of the guardian to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/guardians/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGuardian(@PathVariable String id) {
        log.debug("REST request to delete Guardian : {}", id);
        guardianService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("guardian", id.toString())).build();
    }

}
