package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.domain.Audio;
import com.vigo.cdm.domain.Promise;
import com.vigo.cdm.domain.Task;
import com.vigo.cdm.domain.util.DomainUtil;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A Promise.
 */

public class PromiseDTO{

    private String id;

    //AccountFamily Id
    private AccountFamily accountFamily;

    private Long promiseTime; // 承诺日期时间

    private Long promiseStartTime; // 承诺日期时间

    private Audio promiseAudio;

    //private Task task;

    private String promiseContent;

    private int promiseStatus = DomainUtil.promiseStatus_notStarted;

    private Audio promiseGuardianAudio;

    private Audio promiseTeacherAudio;

    private int sequenceNumber;

    //人格诺
    private int personalityPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 人格诺录音状态 notStarted = 0,started = 1,finished = 2

    private Long personalityStartTime;

    private Audio personalityChildAudio;

    private Audio personalityGuardianAudio;

    private Audio personalityTeacherAudio;

    // 标签诺
    private int tagPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 标签诺录音状态 notStarted = 0,started = 1,finished = 2

    private Long tagStartTime;

    private Audio tagChildAudio;

    private Audio tagGuardianAudio;

    private Audio tagTeacherAudio;

    // 资源诺
    private int resourcePromiseStatus = DomainUtil.promiseStatus_notStarted;  // 资源诺录音状态 notStarted = 0,started = 1,finished = 2

    private Long resourceStartTime;

    private Audio resourceChildAudio;

    private Audio resourceGuardianAudio;

    private Audio resourceTeacherAudio;

    // 禁足诺
    private int groundedPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 禁足诺录音状态 notStarted = 0,started = 1,finished = 2

    private Long groundedStartTime;

    private Audio groundedChildAudio;

    private Audio groundedGuardianAudio;

    private Audio groundedTeacherAudio;

    // 训诫诺
    private int criticizePromiseStatus = DomainUtil.promiseStatus_notStarted;  // 训诫诺录音状态 notStarted = 0,started = 1,finished = 2

    private Long criticizeStartTime;

    private Audio criticizeChildAudio;

    private Audio criticizeGuardianAudio;

    private Audio criticizeTeacherAudio;

    // 家庭大会
    private int familyMeetingPromiseStatus = DomainUtil.promiseStatus_notStarted;  // 家庭大会录音状态 notStarted = 0,started = 1,finished = 2

    private Long familyMeetingStartTime;
    private List<Audio> familyMeetingAudios;

    private List<String> fTags;

    private List<String> iTags;

    private List<String> kTags;

    private List<String> eTags;

    private List<String> aTags;


    public PromiseDTO(){

    }

    public PromiseDTO(Promise promise){
        this.id = promise.getId();
        this.accountFamily = promise.getAccountFamily();
        this.promiseAudio = promise.getPromiseAudio();
        if (promise.getPromiseTime() != null) {
            this.promiseTime = promise.getPromiseTime().getTime();
        }
        if (promise.getPromiseStartTime() != null) {
            this.promiseStartTime = promise.getPromiseStartTime().getTime();
        }
        //this.task = promise.getTask();
        this.promiseContent = promise.getPromiseContent();
        this.promiseGuardianAudio = promise.getPromiseGuardianAudio();
        this.promiseStatus = promise.getPromiseStatus();
        this.promiseTeacherAudio = promise.getPromiseTeacherAudio();
        this.sequenceNumber = promise.getSequenceNumber();

        this.personalityChildAudio = promise.getPersonalityChildAudio();
        this.personalityGuardianAudio = promise.getPersonalityGuardianAudio();
        this.personalityTeacherAudio = promise.getPersonalityTeacherAudio();
        this.personalityPromiseStatus = promise.getPersonalityPromiseStatus();
        if (promise.getPersonalityStartTime() != null) {
            this.personalityStartTime = promise.getPersonalityStartTime().getTime();
        }

        this.tagChildAudio = promise.getTagChildAudio();
        this.tagGuardianAudio = promise.getTagGuardianAudio();
        this.tagTeacherAudio = promise.getTagTeacherAudio();
        this.tagPromiseStatus = promise.getTagPromiseStatus();
        if (promise.getTagStartTime() != null) {
            this.tagStartTime = promise.getTagStartTime().getTime();
        }

        this.resourceChildAudio = promise.getResourceChildAudio();
        this.resourceGuardianAudio = promise.getResourceGuardianAudio();
        this.resourceTeacherAudio = promise.getResourceTeacherAudio();
        this.resourcePromiseStatus = promise.getResourcePromiseStatus();
        if (promise.getResourceStartTime() != null) {
            this.resourceStartTime = promise.getResourceStartTime().getTime();
        }

        this.criticizeChildAudio = promise.getCriticizeChildAudio();
        this.criticizeGuardianAudio = promise.getCriticizeGuardianAudio();
        this.criticizeTeacherAudio = promise.getCriticizeTeacherAudio();
        this.criticizePromiseStatus = promise.getCriticizePromiseStatus();
        if (promise.getCriticizeStartTime() != null) {
            this.criticizeStartTime = promise.getCriticizeStartTime().getTime();
        }

        this.groundedChildAudio = promise.getGroundedChildAudio();
        this.groundedGuardianAudio = promise.getGroundedGuardianAudio();
        this.groundedTeacherAudio = promise.getGroundedTeacherAudio();
        this.groundedPromiseStatus = promise.getGroundedPromiseStatus();
        if (promise.getGroundedStartTime() != null) {
            this.groundedStartTime = promise.getGroundedStartTime().getTime();
        }

        this.familyMeetingPromiseStatus = promise.getFamilyMeetingPromiseStatus();
        this.familyMeetingAudios = promise.getFamilyMeetingAudios();
        if (promise.getFamilyMeetingStartTime() != null) {
            this.familyMeetingStartTime = promise.getFamilyMeetingStartTime().getTime();
        }
        this.fTags = promise.getfTags();
        this.iTags = promise.getiTags();
        this.kTags = promise.getkTags();
        this.eTags = promise.geteTags();
        this.aTags = promise.getaTags();

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PromiseDTO promise = (PromiseDTO) o;
        if(promise.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, promise.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Promise{" +
            "id=" + id +
            '}';
    }



    public Long getPromiseTime() {
        return promiseTime;
    }

    public void setPromiseTime(Long promiseTime) {
        this.promiseTime = promiseTime;
    }

    public Audio getPromiseAudio() {
        return promiseAudio;
    }

    public void setPromiseAudio(Audio promiseAudio) {
        this.promiseAudio = promiseAudio;
    }

    public int getPersonalityPromiseStatus() {
        return personalityPromiseStatus;
    }

    public void setPersonalityPromiseStatus(int personalityPromiseStatus) {
        this.personalityPromiseStatus = personalityPromiseStatus;
    }

    public Audio getPersonalityChildAudio() {
        return personalityChildAudio;
    }

    public void setPersonalityChildAudio(Audio personalityChildAudio) {
        this.personalityChildAudio = personalityChildAudio;
    }

    public Audio getPersonalityGuardianAudio() {
        return personalityGuardianAudio;
    }

    public void setPersonalityGuardianAudio(Audio personalityGuardianAudio) {
        this.personalityGuardianAudio = personalityGuardianAudio;
    }

    public int getTagPromiseStatus() {
        return tagPromiseStatus;
    }

    public void setTagPromiseStatus(int tagPromiseStatus) {
        this.tagPromiseStatus = tagPromiseStatus;
    }

    public Audio getTagChildAudio() {
        return tagChildAudio;
    }

    public void setTagChildAudio(Audio tagChildAudio) {
        this.tagChildAudio = tagChildAudio;
    }

    public Audio getTagGuardianAudio() {
        return tagGuardianAudio;
    }

    public void setTagGuardianAudio(Audio tagGuardianAudio) {
        this.tagGuardianAudio = tagGuardianAudio;
    }

    public int getResourcePromiseStatus() {
        return resourcePromiseStatus;
    }

    public void setResourcePromiseStatus(int resourcePromiseStatus) {
        this.resourcePromiseStatus = resourcePromiseStatus;
    }

    public Audio getResourceChildAudio() {
        return resourceChildAudio;
    }

    public void setResourceChildAudio(Audio resourceChildAudio) {
        this.resourceChildAudio = resourceChildAudio;
    }

    public Audio getResourceGuardianAudio() {
        return resourceGuardianAudio;
    }

    public void setResourceGuardianAudio(Audio resourceGuardianAudio) {
        this.resourceGuardianAudio = resourceGuardianAudio;
    }

    public int getGroundedPromiseStatus() {
        return groundedPromiseStatus;
    }

    public void setGroundedPromiseStatus(int groundedPromiseStatus) {
        this.groundedPromiseStatus = groundedPromiseStatus;
    }

    public Audio getGroundedChildAudio() {
        return groundedChildAudio;
    }

    public void setGroundedChildAudio(Audio groundedChildAudio) {
        this.groundedChildAudio = groundedChildAudio;
    }

    public Audio getGroundedGuardianAudio() {
        return groundedGuardianAudio;
    }

    public void setGroundedGuardianAudio(Audio groundedGuardianAudio) {
        this.groundedGuardianAudio = groundedGuardianAudio;
    }

    public int getCriticizePromiseStatus() {
        return criticizePromiseStatus;
    }

    public void setCriticizePromiseStatus(int criticizePromiseStatus) {
        this.criticizePromiseStatus = criticizePromiseStatus;
    }

    public Audio getCriticizeChildAudio() {
        return criticizeChildAudio;
    }

    public void setCriticizeChildAudio(Audio criticizeChildAudio) {
        this.criticizeChildAudio = criticizeChildAudio;
    }

    public Audio getCriticizeGuardianAudio() {
        return criticizeGuardianAudio;
    }

    public void setCriticizeGuardianAudio(Audio criticizeGuardianAudio) {
        this.criticizeGuardianAudio = criticizeGuardianAudio;
    }

    public int getFamilyMeetingPromiseStatus() {
        return familyMeetingPromiseStatus;
    }

    public void setFamilyMeetingPromiseStatus(int familyMeetingPromiseStatus) {
        this.familyMeetingPromiseStatus = familyMeetingPromiseStatus;
    }

    public List<Audio> getFamilyMeetingAudios() {
        return familyMeetingAudios;
    }

    public void setFamilyMeetingAudios(List<Audio> familyMeetingAudios) {
        this.familyMeetingAudios = familyMeetingAudios;
    }

    public List<String> getfTags() {
        return fTags;
    }

    public void setfTags(List<String> fTags) {
        this.fTags = fTags;
    }

    public List<String> getiTags() {
        return iTags;
    }

    public void setiTags(List<String> iTags) {
        this.iTags = iTags;
    }

    public List<String> getkTags() {
        return kTags;
    }

    public void setkTags(List<String> kTags) {
        this.kTags = kTags;
    }

    public List<String> geteTags() {
        return eTags;
    }

    public void seteTags(List<String> eTags) {
        this.eTags = eTags;
    }

    public List<String> getaTags() {
        return aTags;
    }

    public void setaTags(List<String> aTags) {
        this.aTags = aTags;
    }

    public Audio getPersonalityTeacherAudio() {
        return personalityTeacherAudio;
    }

    public void setPersonalityTeacherAudio(Audio personalityTeacherAudio) {
        this.personalityTeacherAudio = personalityTeacherAudio;
    }

    public Audio getTagTeacherAudio() {
        return tagTeacherAudio;
    }

    public void setTagTeacherAudio(Audio tagTeacherAudio) {
        this.tagTeacherAudio = tagTeacherAudio;
    }

    public Audio getResourceTeacherAudio() {
        return resourceTeacherAudio;
    }

    public void setResourceTeacherAudio(Audio resourceTeacherAudio) {
        this.resourceTeacherAudio = resourceTeacherAudio;
    }

    public Audio getGroundedTeacherAudio() {
        return groundedTeacherAudio;
    }

    public void setGroundedTeacherAudio(Audio groundedTeacherAudio) {
        this.groundedTeacherAudio = groundedTeacherAudio;
    }

    public Audio getCriticizeTeacherAudio() {
        return criticizeTeacherAudio;
    }

    public void setCriticizeTeacherAudio(Audio criticizeTeacherAudio) {
        this.criticizeTeacherAudio = criticizeTeacherAudio;
    }

    public String getPromiseContent() {
        return promiseContent;
    }

    public void setPromiseContent(String promiseContent) {
        this.promiseContent = promiseContent;
    }

    public Audio getPromiseGuardianAudio() {
        return promiseGuardianAudio;
    }

    public void setPromiseGuardianAudio(Audio promiseGuardianAudio) {
        this.promiseGuardianAudio = promiseGuardianAudio;
    }

    public Audio getPromiseTeacherAudio() {
        return promiseTeacherAudio;
    }

    public void setPromiseTeacherAudio(Audio promiseTeacherAudio) {
        this.promiseTeacherAudio = promiseTeacherAudio;
    }





    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    /*public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }*/

    public int getPromiseStatus() {
        return promiseStatus;
    }

    public void setPromiseStatus(int promiseStatus) {
        this.promiseStatus = promiseStatus;
    }

    public Long getPersonalityStartTime() {
        return personalityStartTime;
    }

    public void setPersonalityStartTime(Long personalityStartTime) {
        this.personalityStartTime = personalityStartTime;
    }

    public Long getTagStartTime() {
        return tagStartTime;
    }

    public void setTagStartTime(Long tagStartTime) {
        this.tagStartTime = tagStartTime;
    }

    public Long getResourceStartTime() {
        return resourceStartTime;
    }

    public void setResourceStartTime(Long resourceStartTime) {
        this.resourceStartTime = resourceStartTime;
    }

    public Long getGroundedStartTime() {
        return groundedStartTime;
    }

    public void setGroundedStartTime(Long groundedStartTime) {
        this.groundedStartTime = groundedStartTime;
    }

    public Long getCriticizeStartTime() {
        return criticizeStartTime;
    }

    public void setCriticizeStartTime(Long criticizeStartTime) {
        this.criticizeStartTime = criticizeStartTime;
    }

    public Long getFamilyMeetingStartTime() {
        return familyMeetingStartTime;
    }

    public void setFamilyMeetingStartTime(Long familyMeetingStartTime) {
        this.familyMeetingStartTime = familyMeetingStartTime;
    }

    public Long getPromiseStartTime() {
        return promiseStartTime;
    }

    public void setPromiseStartTime(Long promiseStartTime) {
        this.promiseStartTime = promiseStartTime;
    }
}
