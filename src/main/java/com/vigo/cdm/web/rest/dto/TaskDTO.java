package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.*;
import com.vigo.cdm.domain.util.DomainUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * A DTO representing a user, with his authorities.
 */
public class TaskDTO {

    private String id;

    private String description;

    //AccountFamily Id
    private AccountFamily accountFamily;

    private int taskCategory = DomainUtil.task_category_initial;

    private Promise promise;

    private Double discount;

    private boolean hasCredit = false;
    //S
    private Long plannedStartTime;
    private Long plannedEndTime;
    private Long actualStartTime;
    private Long actualEndTime;

    //N
    private List<Photo> nPhotos;
    private List<Text> nTexts;
    private List<Audio> nAudios;
    //private List<String> nAudioURLStrings;
    private List<Video> nVideos;
    //private List<String> nVideoThumbnailURLStrings;

    //J
    private List<Photo> jPhotos;
    private List<Text> jTexts;
    private List<Audio> jAudios;
    //private List<String> jAudioURLStrings;
    private List<Video> jVideos;
    //private List<String> jVideoThumbnailURLStrings;
    private List<String> jDemerits;

    //P
    private String pGuardianText;
    private String pSelfText;
    private String pTeacherText;
    public TaskDTO(){

    }

    public TaskDTO(Task task){
        this.id = task.getId();

        this.description = task.getDescription();
        this.discount = task.getDiscount();
        //AccountFamily Id
        this.accountFamily = task.getAccountFamily();
        this.promise = task.getPromise();
        this.taskCategory = task.getTaskCategory();
        //S
        if(task.getPlannedStartTime()!=null){
            this.plannedStartTime = task.getPlannedStartTime().getTime();
        }
        if(task.getPlannedEndTime()!=null){
            this.plannedEndTime = task.getPlannedEndTime().getTime();
        }
        if(task.getActualStartTime() !=null){
            this.actualStartTime = task.getActualStartTime().getTime();
        }
        if(task.getActualEndTime() !=null){
            this.actualEndTime = task.getActualEndTime().getTime();
        }

        //N
        this.nPhotos = task.getnPhotos();
        this.nTexts = task.getnTexts();
        this.nAudios = task.getnAudios();
        this.nVideos = task.getnVideos();
        //this.nVideoThumbnailURLStrings = task.getnVideoThumbnailURLStrings();

        //J
        this.jPhotos = task.getjPhotos();
        this.jTexts = task.getjTexts();
        this.jAudios = task.getjAudios();
        this.jVideos = task.getjVideos();
        //this.jVideoThumbnailURLStrings = task.getjVideoThumbnailURLStrings();
        this.jDemerits = task.getjDemerits();

        //p
        this.pGuardianText = task.getpGuardianText();
        this.pSelfText = task.getpSelfText();
        this.pTeacherText = task.getpTeacherText();

    }


    /*public TaskDTO(Long id, String description, AccountFamily accountFamily,
                   Long plannedStartTime, Long plannedEndTime, Long actualStartTime, Long actualEndTime,
                   List<String> nPhotoURLStrings, List<String> nTexts, List<String> nAudioURLStrings, List<String> nVideoURLStrings,
                   List<String> jPhotoURLStrings, List<String> jTexts, List<String> jAudioURLStrings, List<String> jVideoURLStrings){
        this.id;

        this.description;

        //AccountFamily Id
        this.accountFamily;

        //S
        this.plannedStartTime;
        this.plannedEndTime;
        this.actualStartTime;
        this.actualEndTime;

        //N
        this.nPhotoURLStrings;
        this.nTexts;
        this.nAudioURLStrings;
        this.nVideoURLStrings;

        //J
        this.jPhotoURLStrings;
        this.jTexts;
        this.jAudioURLStrings;
        this.jVideoURLStrings;

    }*/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    public Long getPlannedStartTime() {
        return plannedStartTime;
    }

    public void setPlannedStartTime(Long plannedStartTime) {
        this.plannedStartTime = plannedStartTime;
    }

    public Long getPlannedEndTime() {
        return plannedEndTime;
    }

    public void setPlannedEndTime(Long plannedEndTime) {
        this.plannedEndTime = plannedEndTime;
    }

    public Long getActualStartTime() {
        return actualStartTime;
    }

    public void setActualStartTime(Long actualStartTime) {
        this.actualStartTime = actualStartTime;
    }

    public Long getActualEndTime() {
        return actualEndTime;
    }

    public void setActualEndTime(Long actualEndTime) {
        this.actualEndTime = actualEndTime;
    }





    public List<String> getjDemerits() {
        return jDemerits;
    }

    public void setjDemerits(List<String> jDemerits) {
        this.jDemerits = jDemerits;
    }

    public String getpGuardianText() {
        return pGuardianText;
    }

    public void setpGuardianText(String pGuardianText) {
        this.pGuardianText = pGuardianText;
    }

    public String getpSelfText() {
        return pSelfText;
    }

    public void setpSelfText(String pSelfText) {
        this.pSelfText = pSelfText;
    }

    public String getpTeacherText() {
        return pTeacherText;
    }

    public void setpTeacherText(String pTeacherText) {
        this.pTeacherText = pTeacherText;
    }



    public int getTaskCategory() {
        return taskCategory;
    }

    public void setTaskCategory(int taskCategory) {
        this.taskCategory = taskCategory;
    }

    public List<Audio> getnAudios() {
        return nAudios;
    }

    public void setnAudios(List<Audio> nAudios) {
        this.nAudios = nAudios;
    }

    public List<Audio> getjAudios() {
        return jAudios;
    }

    public void setjAudios(List<Audio> jAudios) {
        this.jAudios = jAudios;
    }

    public List<Photo> getnPhotos() {
        return nPhotos;
    }

    public void setnPhotos(List<Photo> nPhotos) {
        this.nPhotos = nPhotos;
    }

    public List<Text> getnTexts() {
        return nTexts;
    }

    public void setnTexts(List<Text> nTexts) {
        this.nTexts = nTexts;
    }

    public List<Video> getnVideos() {
        return nVideos;
    }

    public void setnVideos(List<Video> nVideos) {
        this.nVideos = nVideos;
    }

    public List<Photo> getjPhotos() {
        return jPhotos;
    }

    public void setjPhotos(List<Photo> jPhotos) {
        this.jPhotos = jPhotos;
    }

    public List<Text> getjTexts() {
        return jTexts;
    }

    public void setjTexts(List<Text> jTexts) {
        this.jTexts = jTexts;
    }

    public List<Video> getjVideos() {
        return jVideos;
    }

    public void setjVideos(List<Video> jVideos) {
        this.jVideos = jVideos;
    }

    public Promise getPromise() {
        return promise;
    }

    public void setPromise(Promise promise) {
        this.promise = promise;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public boolean getHasCredit() {
        return hasCredit;
    }

    public void setHasCredit(boolean hasCredit) {
        this.hasCredit = hasCredit;
    }
}
