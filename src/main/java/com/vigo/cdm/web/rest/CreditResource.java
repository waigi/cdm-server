package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.domain.Credit;
import com.vigo.cdm.domain.Task;
import com.vigo.cdm.service.CreditService;
import com.vigo.cdm.service.TaskService;
import com.vigo.cdm.web.rest.dto.CreditDTO;
import com.vigo.cdm.web.rest.dto.CreditsInWeek;
import com.vigo.cdm.web.rest.dto.TaskDTO;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing Credit.
 */
@RestController
@RequestMapping("/api")
public class CreditResource {

    private final Logger log = LoggerFactory.getLogger(CreditResource.class);

    @Inject
    private CreditService creditService;

    @Inject
    private TaskService taskService;
    /**
     * POST  /credits : Create a new credit.
     *
     * @param credit the credit to create
     * @return the ResponseEntity with status 201 (Created) and with body the new credit, or with status 400 (Bad Request) if the credit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/credits",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Credit> createCredit(@RequestBody Credit credit) throws URISyntaxException {
        log.debug("REST request to save Credit : {}", credit);
        if (credit.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("credit", "idexists", "A new credit cannot already have an ID")).body(null);
        }
        Credit result = creditService.save(credit);
        return ResponseEntity.created(new URI("/api/credits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("credit", result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /credits : Create a new credit.
     *
     * @param task_id the credit to create
     * @return the ResponseEntity with status 201 (Created) and with body the new credit, or with status 400 (Bad Request) if the credit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/credits/createByTask",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Credit> createCreditByTaskId(@RequestParam(value = "account_family_id", required = true) String account_family_id,
                                                       @RequestParam(value = "task_id", required = true) String task_id) throws URISyntaxException {
        log.debug("REST request to save Credit by task id: {}", task_id);
        Credit credit = new Credit();
        credit.setTask(new Task(task_id, new AccountFamily(account_family_id)));
        Credit result = creditService.save(credit);
        return ResponseEntity.created(new URI("/api/credits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("credit", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /credits : Updates an existing credit.
     *
     * @param credit the credit to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated credit,
     * or with status 400 (Bad Request) if the credit is not valid,
     * or with status 500 (Internal Server Error) if the credit could not be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/credits",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Credit> updateCredit(@RequestBody Credit credit) throws URISyntaxException {
        log.debug("REST request to update Credit : {}", credit);
        if (credit.getId() == null) {
            return createCredit(credit);
        }
        Credit result = creditService.save(credit);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("credit", credit.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /credits : Updates an existing credit.
     *
     * @param credit the credit to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated credit,
     * or with status 400 (Bad Request) if the credit is not valid,
     * or with status 500 (Internal Server Error) if the credit couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/credits/updateAndCover",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CreditDTO> updateAndCoverCredit(@RequestBody Credit credit) throws URISyntaxException {
        log.debug("REST request to update Credit : {}", credit);
        /*if (credit.getId() == null) {
            if(credit.getTask()==null && credit.getTask().getId()==null){
                return new ResponseEntity<>(new CreditDTO(),HttpStatus.NOT_FOUND);
            }
            credit = new Credit();

            return new ResponseEntity<>(new CreditDTO(),HttpStatus.NOT_FOUND);
        }*/
        if(credit.getGraderType()==null || credit.getTask()==null || credit.getTask().getId()==null|| credit.getTask().getAccountFamily() ==null
            || credit.getTask().getAccountFamily().getId() ==null){
            return new ResponseEntity<>(new CreditDTO(),HttpStatus.NOT_FOUND);
        }
        if(credit.getTask().getPlannedStartTime()!=null){
            credit.setTaskDate(credit.getTask().getPlannedStartTime());
        }

        Credit result = creditService.save(credit);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("credit", credit.getId().toString()))
            .body(new CreditDTO(result));
    }

    /**
     * PUT  /credits : Updates an existing credit.
     *
     * @param credit the credit to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated credit,
     * or with status 400 (Bad Request) if the credit is not valid,
     * or with status 500 (Internal Server Error) if the credit couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/credits/update",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CreditDTO> updateCreditNotNull(@RequestBody Credit credit) throws URISyntaxException {
        log.debug("REST request to update Credit : {}", credit);

        if(credit.getGraderType()==null || credit.getTask()==null || credit.getTask().getId()==null|| credit.getTask().getAccountFamily() ==null
            || credit.getTask().getAccountFamily().getId() ==null){
            return new ResponseEntity<>(new CreditDTO(),HttpStatus.NOT_FOUND);
        }
        Credit result = creditService.findOneByTaskIdAndGraderType(credit.getTask().getId(), credit.getGraderType());
        Task task = taskService.findOne(credit.getTask().getId());
        if(task == null){
            return new ResponseEntity<>(new CreditDTO(),HttpStatus.NOT_FOUND);
        }
        TaskDTO taskDTO = new TaskDTO(task);
        if (result == null) {
            if(credit.getTask().getPlannedStartTime()!=null){
                credit.setTaskDate(credit.getTask().getPlannedStartTime());
            }
            credit.setDiscount(credit.getTask().getDiscount());
            if(credit.getfTotal()!=null || credit.getiTotal()!=null
            || credit.getkTotal()!=null || credit.geteTotal()!=null || credit.getaTotal()!=null){
                taskDTO.setHasCredit(true);
            }else{
                taskDTO.setHasCredit(false);
            }

            result =creditService.save(credit);
        }else{
            result.setDiscount(credit.getTask().getDiscount());
            //credit.setHasCredit(true);
            //result =  creditService.findOne(credit.getId());
            result = onlyUpdateCredit(result, credit);
            if(result.getfTotal()!=null || result.getiTotal()!=null
                || result.getkTotal()!=null || result.geteTotal()!=null || result.getaTotal()!=null){
                taskDTO.setHasCredit(true);
            }else{
                taskDTO.setHasCredit(false);
            }
            result = creditService.save(result);
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("credit", result.getId().toString()))
            .body(new CreditDTO(result,taskDTO));
    }

    public Credit onlyUpdateCredit(Credit credit, Credit creditParam){
        if(creditParam.getfDetails()!=null){
            credit.setfDetails(creditParam.getfDetails());
        }
        if(creditParam.getiDetails()!=null){
            credit.setiDetails(creditParam.getiDetails());
        }
        if(creditParam.getkDetails()!=null){
            credit.setkDetails(creditParam.getkDetails());
        }
        if(creditParam.geteDetails()!=null){
            credit.seteDetails(creditParam.geteDetails());
        }
        if(creditParam.getaDetails()!=null){
            credit.setaDetails(creditParam.getaDetails());
        }

        if(creditParam.getfTotal()!=null) {
            credit.setfTotal(creditParam.getfTotal());
        }
        if(creditParam.getiTotal()!=null) {
            credit.setiTotal(creditParam.getiTotal());
        }
        if(creditParam.getkTotal()!=null) {
            credit.setkTotal(creditParam.getkTotal());
        }
        if(creditParam.geteTotal()!=null) {
            credit.seteTotal(creditParam.geteTotal());
        }
        if(creditParam.getaTotal()!=null) {
            credit.setaTotal(creditParam.getaTotal());
        }
        /*if(creditParam.getGraderType()!=null){
            credit.setGraderType(creditParam.getGraderType());
        }*/
        if(creditParam.getCreditType()!=null){
            credit.setCreditType(creditParam.getCreditType());
        }
        if(creditParam.getReview()!=null){
            credit.setReview(creditParam.getReview());
        }
        if(creditParam.getTaskDate()!=null){
            credit.setTaskDate(creditParam.getTaskDate());
        }
        if(creditParam.getWeight()!=null){
            credit.setWeight(creditParam.getWeight());
        }
        if(creditParam.getTask()!=null){
            if(credit.getTask()==null){
                credit.setTask(new Task());
            }
            if(creditParam.getTask().getId()!=null){
                credit.getTask().setId(creditParam.getTask().getId());
            }
            if(creditParam.getTask().getAccountFamily()!=null) {
                if (creditParam.getTask().getAccountFamily().getId() != null) {
                    credit.getTask().setAccountFamily(creditParam.getTask().getAccountFamily());
                }
            }
            if(creditParam.getTask().getPlannedStartTime()!=null){
                credit.setTaskDate(creditParam.getTask().getPlannedStartTime());
            }

        }
        return credit;
    }
    /**
     * GET  /credits : get all the credits.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of credits in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/credits",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Credit>> getAllCredits(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Credits");
        Page<Credit> page = creditService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/credits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /credits/:id : get the "id" credit.
     *
     * @param task_id the id of the credit to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the credit, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/credits/getByTaskIdAndGraderType",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
        public ResponseEntity<CreditDTO> getCreditByTaskId(@RequestParam(value = "task_id", required = true) String task_id,
                                                           @RequestParam(value = "grader_type", required = true) Integer grader_type) {
        log.debug("REST request to get Credit by task id: {}", task_id);
        Credit credit = creditService.findOneByTaskIdAndGraderType(task_id,grader_type);
        return Optional.ofNullable(credit)
            .map(result -> new ResponseEntity<>(
                new CreditDTO(result),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(new CreditDTO(),HttpStatus.OK));
    }


    /**
     * GET  /credits/:id : get the "id" credit.
     *
     * @param account_family_id the id of the credit to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the credit, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/credits/reportSearchCriteria",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CreditsInWeek>> getReportCredits(@RequestParam(value = "account_family_id", required = true) String account_family_id,
                                                            @RequestParam(value = "grader_type", required = true) Integer graderType,
                                                            @RequestParam(value = "cycle_days_number", required = true) Integer cycle_days_number,
                                                            @RequestParam(value = "start_date", required = true) String start_date,
                                                            @RequestParam(value = "end_date", required = true) String end_date
                                                            ) {
        //log.debug("REST request to get Credit by task id: {}", account_family_id);
        //Credit credit = creditService.findOneByTaskId(task_id);
        List<Credit> credits = creditService.findCreditsByAccountFamilyIdAndGraderTypeAndTaskDateBetween(account_family_id, graderType, start_date, end_date);
        List<CreditsInWeek> creditsInWeeks = reportSearchCriteria(credits, cycle_days_number, start_date, end_date);
        return new ResponseEntity<>(creditsInWeeks,HttpStatus.OK);
    }

    public List<CreditsInWeek> reportSearchCriteria(List<Credit> credits, Integer cycleDaysNumber, String startDate, String endDate){
        List<CreditsInWeek> creditsInWeeks = new ArrayList<CreditsInWeek>(0);
        if(credits == null||credits.isEmpty()){
            return creditsInWeeks;
        }
        long fromTime = Long.valueOf(startDate);
        long endTime = Long.valueOf(endDate);
        if(fromTime >= endTime){
            return creditsInWeeks;
        }
        long toDate = fromTime + 24*60*60*1000*cycleDaysNumber;

        //CreditDTO creditDTO = new CreditDTO();
        while(toDate < endTime){
            //toCalendar.set(Calendar.DATE, toCalendar.get(Calendar.DATE) + cycleDaysNumber-1);

            CreditsInWeek creditsInWeek = new CreditsInWeek();
            for(Credit credit :credits){
                if( credit.getTaskDate().getTime() >= fromTime && credit.getTaskDate().getTime() < toDate){
                    creditsInWeek.getCredits().add(new CreditDTO(credit));
                }
            }
            creditsInWeek.setStartDate(fromTime);
            creditsInWeek.setEndDate(toDate);
            fromTime = toDate;
            toDate = toDate + 24*60*60*1000*cycleDaysNumber;
            creditsInWeeks.add(creditsInWeek);
        }
        if(fromTime < endTime){
            CreditsInWeek creditsInWeek = new CreditsInWeek();
            for(Credit credit :credits){
                if( credit.getTaskDate().getTime() >= fromTime && credit.getTaskDate().getTime() < endTime){
                    creditsInWeek.getCredits().add(new CreditDTO(credit));
                }
            }
            creditsInWeek.setStartDate(fromTime);
            creditsInWeek.setEndDate(endTime);
            creditsInWeeks.add(creditsInWeek);
        }
        return creditsInWeeks;
    }
    /**
     * GET  /credits/:id : get the "id" credit.
     *
     * @param id the id of the credit to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the credit, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/credits/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Credit> getCredit(@PathVariable String id) {
        log.debug("REST request to get Credit : {}", id);
        Credit credit = creditService.findOne(id);
        return Optional.ofNullable(credit)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /credits/:id : delete the "id" credit.
     *
     * @param id the id of the credit to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/credits/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCredit(@PathVariable String id) {
        log.debug("REST request to delete Credit : {}", id);
        creditService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("credit", id.toString())).build();
    }

}
