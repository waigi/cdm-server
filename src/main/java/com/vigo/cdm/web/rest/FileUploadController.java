package com.vigo.cdm.web.rest;

/**
 * Created by Can on 20/10/2015.
 */
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.Task;
import com.vigo.cdm.service.TaskService;
import com.vigo.cdm.web.rest.util.FilePathUtil;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class FileUploadController {

    @Inject
    private TaskService taskService;
    /*@RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam("file") MultipartFile file){
        String name = null;
        //MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        //MultipartFile file = multipartRequest.getFile("file");
        //return null;
        if (!file.isEmpty()) {
            if (StringUtils.isBlank(name)) {
                name = "pdftemplate/" + file.getOriginalFilename();
            }
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(name)));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " + name + "!";
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }*/
    @RequestMapping(value="/tasks/downloadStreamingMedia", method=RequestMethod.GET)
    public void getDownload(@RequestParam(value = "file_path", required = true) String file_path,
                            @RequestParam(value = "file_name", required = true) String file_name,
                            HttpServletResponse response) throws IOException {

        // Get your file stream from wherever.
        InputStream myStream = new FileInputStream(file_path);
        // Set the content type and attachment header.
        response.addHeader("Content-disposition", "attachment;filename="+file_name);
        response.setContentType("txt/plain");

        // Copy the stream to the response's output stream.
        IOUtils.copy(myStream, response.getOutputStream());
        response.flushBuffer();
    }

    @RequestMapping(value = "/uploadTest",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Task> uploadFileTask(@RequestParam(value = "task_type", required = true) int task_type,
                                               @RequestParam(value = "file_type", required = true) int file_type,
                                               @RequestParam(value = "task_id", required = true) String task_id,
                                               @RequestParam(value="file",required = true) MultipartFile file,
                                               HttpServletRequest request
    ) throws IOException {
        String basePath =request.getSession().getServletContext().getRealPath("/");
        String baseUrl = request.getScheme() + // "http"
            "://" +                                // "://"
            request.getServerName() +              // "myhost"
            ":" +                                  // ":"
            request.getServerPort() +              // "80"
            request.getContextPath();               // "/myContextPath" or "" if deployed in root context
        //System.out.println(baseUrl);
        Task task = taskService.findOne(task_id);
        if (!file.isEmpty()) {
            String folderRelativePath = uploadFileTaskRelativePathURL(task_type, file_type, task.getAccountFamily().getId(), task_id);
            UUID uuid = UUID.randomUUID();
            String fileName = uuid.toString() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            String fileRelativePath = folderRelativePath + FilePathUtil.pathForwardSlash+ fileName;
            String fileAbsolutePath = basePath + fileRelativePath;
            if(saveFileFromInputStream(file, basePath, folderRelativePath, fileRelativePath)){
                //filePath = baseUrl + "/" + filePath;
                //List<String> fileURLs = new ArrayList<String>();
                //fileURLs.add(filePath);
                task = updateTask(task_type, file_type, task, baseUrl, fileRelativePath, fileAbsolutePath ,fileName);
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("task", task.getId().toString()))
                    .body(task);
            }else{
                new ResponseEntity<>(task,HttpStatus.NOT_MODIFIED);
            }
        }
        return new ResponseEntity<>(task,HttpStatus.NOT_MODIFIED);
    }

    public Task updateTask(int task_type, int file_type, Task task, String baseUrl, String fileRelativePath, String fileAbsolutePath ,String fileName){
        String urlPhoto = baseUrl+ "/" +fileRelativePath;
        String  urlStreamingMedia = baseUrl + "/tasks/downloadStreamingMedia?file_path="+fileAbsolutePath+"&file_name="+fileName;
        List<String> filePhotoURL = new ArrayList<String>();
        filePhotoURL.add(urlPhoto);
        List<String> fileStreamingMediaURL = new ArrayList<String>();
        fileStreamingMediaURL.add(urlStreamingMedia);
        /*if(task_type == FilePathUtil.task_type_n){
            if(file_type == FilePathUtil.file_type_photo){
                task.setnPhotos(filePhotoURL);
            }else if(file_type == FilePathUtil.file_type_Audio){
                task.setnAudioURLStrings(fileStreamingMediaURL);
            }else if(file_type == FilePathUtil.file_type_Video){
                task.setnVideoURLStrings(fileStreamingMediaURL);
            }
        }else if(task_type == FilePathUtil.task_type_j){
            if(file_type == FilePathUtil.file_type_photo){
                task.setjPhotoURLStrings(filePhotoURL);
            }else if(file_type == FilePathUtil.file_type_Audio){
                task.setjAudioURLStrings(fileStreamingMediaURL);
            }else if(file_type == FilePathUtil.file_type_Video){
                task.setjVideoURLStrings(fileStreamingMediaURL);
            }
        }*/
        task = taskService.save(task);
        return task;
    }
    public String uploadFileTaskRelativePathURL(int task_type, int file_type, String accountFamilyId, String task_id){
        String filePath = FilePathUtil.pathConstant+accountFamilyId+FilePathUtil.pathForwardSlash+task_id;
        if(task_type == FilePathUtil.task_type_n){
            filePath = filePath  + FilePathUtil.pathNOfTask;
        }else if(task_type == FilePathUtil.task_type_j){
            filePath = filePath  + FilePathUtil.pathJOfTask;
        }
        if(file_type == FilePathUtil.file_type_photo){
            filePath = filePath  + FilePathUtil.pathPhoto;
        }else if(file_type == FilePathUtil.file_type_Audio){
            filePath = filePath  + FilePathUtil.pathAudio;
        }else if(file_type == FilePathUtil.file_type_Video){
            filePath = filePath  + FilePathUtil.pathVideo;
        }


        return filePath;
    }

    public Boolean saveFileFromInputStream(MultipartFile file,String basePath, String folderPath, String filePath) {
        Boolean result = false;
        if (file==null || file.isEmpty()) {
            return result;
        }
        File fileDownload = new File(basePath + folderPath);
        if(!fileDownload.exists()){
            if(!fileDownload.mkdirs()){
                return result;
            }
        }
        try {
            InputStream stream = file.getInputStream();
            FileOutputStream fs = new FileOutputStream(basePath + filePath);
            byte[] buffer = new byte[1024 * 1024];
            int bytesum = 0;
            int byteread = 0;
            while ((byteread = stream.read(buffer)) != -1) {
                bytesum += byteread;
                fs.write(buffer, 0, byteread);
                fs.flush();
            }
            fs.close();
            stream.close();
            result = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
