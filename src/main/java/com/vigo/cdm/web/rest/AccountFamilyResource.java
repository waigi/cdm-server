package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.*;
import com.vigo.cdm.repository.GuardianRepository;
import com.vigo.cdm.repository.StudentRepository;
import com.vigo.cdm.repository.UserRepository;
import com.vigo.cdm.service.*;
import com.vigo.cdm.web.rest.dto.*;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing AccountFamily.
 */
@RestController
@RequestMapping("/api")
public class AccountFamilyResource {

    private final Logger log = LoggerFactory.getLogger(AccountFamilyResource.class);

    @Inject
    private AccountFamilyService accountFamilyService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private StudentService studentService;

    @Inject
    private GuardianService guardianService;

    @Inject
    private TeacherService teacherService;

    @Inject
    private UserService userService;
    /**
     * POST  /account-families : Create a new accountFamily.
     *
     * @param accountFamilyDTO the accountFamily to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accountFamily, or with status 400 (Bad Request) if the accountFamily has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/account-families",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountFamily> createAccountFamily(@RequestBody AccountFamilyDTO accountFamilyDTO) throws URISyntaxException {

        log.debug("REST request to save AccountFamily : {}", accountFamilyDTO);
        if (accountFamilyDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("accountFamily", "idexists", "A new accountFamily cannot already have an ID")).body(null);
        }

        if (userRepository.findOneByLogin(accountFamilyDTO.getChild().getLogin().toLowerCase()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use"))
                .body(null);
        }
        for(GuardianDTO guardianDTO : accountFamilyDTO.getGuardianDTOList()){
            if (userRepository.findOneByLogin(accountFamilyDTO.getChild().getLogin().toLowerCase()).isPresent()) {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use"))
                    .body(null);
            }
        }
        AccountFamily result = new AccountFamily(accountFamilyDTO);
        //AccountFamily result = new AccountFamily();
        result = accountFamilyService.save(result);
        Student student = new Student(accountFamilyDTO.getChild().getId(), accountFamilyDTO.getChild().getName(), result.getId());
        student = studentService.save(student);
        //List<Guardian> guardianList = new ArrayList<Guardian>();
        for(GuardianDTO guardianDTO : accountFamilyDTO.getGuardianDTOList()) {
            Guardian guardian = new Guardian(guardianDTO.getId(), guardianDTO.getName(), guardianDTO.getRelationship(), result.getId());
            guardian = guardianService.save(guardian);
            //guardianList.add(guardian);
            User user = userService.createUser(guardianDTO.getLogin(),"ROLE_GUARDIAN","123456",null,guardian,null);
        }
        User userChild = userService.createUser(accountFamilyDTO.getChild().getLogin(),"ROLE_STUDENT","123456",student,null,null);
        return ResponseEntity.created(new URI("/api/account-families/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("accountFamily", result.getId().toString()))
            .body(result);
        //accountFamily.getChild().
        //User
        //return null;
        /*log.debug("REST request to save AccountFamily : {}", accountFamily);
        if (accountFamily.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("accountFamily", "idexists", "A new accountFamily cannot already have an ID")).body(null);
        }
        AccountFamily result = accountFamilyService.save(accountFamily);
        return ResponseEntity.created(new URI("/api/account-families/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("accountFamily", result.getId().toString()))
            .body(result);*/
    }

    /**
     * PUT  /account-families : Updates an existing accountFamily.
     *
     * @param accountFamily the accountFamily to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accountFamily,
     * or with status 400 (Bad Request) if the accountFamily is not valid,
     * or with status 500 (Internal Server Error) if the accountFamily couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/account-families",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountFamily> updateAccountFamily(@RequestBody AccountFamily accountFamily) throws URISyntaxException {

        log.debug("REST request to update AccountFamily : {}", accountFamily);
       /* if (accountFamily.getId() == null) {
            return createAccountFamily(accountFamily);
        }*/
        AccountFamily result = accountFamilyService.save(accountFamily);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("accountFamily", accountFamily.getId().toString()))
            .body(result);
    }

    /**
     * GET  /account-families : get all the accountFamilies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of accountFamilies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/account-families",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<AccountFamily>> getAllAccountFamilies(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of AccountFamilies");
        Page<AccountFamily> page = accountFamilyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/account-families");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /account-families : get all the accountFamilies.
     *
     * @param accountFamily_id the accountFamily Id
     * @return the ResponseEntity with status 200 (OK) and the list of accountFamilies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/account-families/getAccountFamilyInfoDTO",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountFamilyInfoDTO> getAccountFamilyInfoDTO(@RequestParam(value = "accountFamily_id", required = true)String accountFamily_id)
        throws URISyntaxException {
        log.debug("REST request accountFamilyInfoDTO");
        List<Teacher> teachers = teacherService.findTeachersByAccountFamilyId(accountFamily_id);
        List<Guardian> guardians = guardianService.findGuardiansByAccountFamilyId(accountFamily_id);
        AccountFamilyInfoDTO accountFamilyInfoDTO = createAccountFamilyInfoDTO(teachers, guardians);
        return new ResponseEntity<>(accountFamilyInfoDTO, HttpStatus.OK);
    }

    /**
     * GET  /account-families : get all the accountFamilies.
     *
     * @param teacher_id the teacher Id
     * @return the ResponseEntity with status 200 (OK) and the list of accountFamilies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/student-infos/byTeacherId",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<StudentInfoDTO>> getStudentInfoDTOs(@RequestParam(value = "teacher_id", required = true)String teacher_id)
        throws URISyntaxException {
        log.debug("REST request studentInfoDTOs");
        Teacher teacher = teacherService.findOne(teacher_id);
        List<StudentInfoDTO> studentInfoDTOs = createStudentInfoDTOs(teacher);
        return new ResponseEntity<>(studentInfoDTOs, HttpStatus.OK);
    }

    /**
     * GET  /account-families/:id : get the "id" accountFamily.
     *
     * @param id the id of the accountFamily to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountFamily, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/account-families/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountFamilyMemberDTO> getAccountFamily(@PathVariable String id) {
        AccountFamily accountFamily = accountFamilyService.findOne(id);
        if(accountFamily == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<Teacher> teachers = teacherService.findTeachersByAccountFamilyId(id);
        List<Guardian> guardians = guardianService.findGuardiansByAccountFamilyId(id);
        Student student = studentService.findOneByAccountFamilyId(id);
        AccountFamilyMemberDTO accountFamilyMemberDTO = new AccountFamilyMemberDTO(accountFamily, student, guardians, teachers);
        return new ResponseEntity<>(accountFamilyMemberDTO, HttpStatus.OK);
        /*log.debug("REST request to get AccountFamily : {}", id);
        AccountFamily accountFamily = accountFamilyService.findOne(id);
        return Optional.ofNullable(accountFamily)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));*/
    }

    /**
     * GET  /account-families/:id : get the "id" accountFamily.
     *
     * @param account_family_id the id of the accountFamily to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountFamily, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/account-families/getExpiryDate",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExpiryDateDTO> getAccountFamilyExpiryDate(@RequestParam(value = "account_family_id", required = true)String account_family_id) {
        int expiryDate = 1;
        ExpiryDateDTO expiryDateDTO = new ExpiryDateDTO(expiryDate);
        return new ResponseEntity<>(expiryDateDTO, HttpStatus.OK);
    }

    /**
     * DELETE  /account-families/:id : delete the "id" accountFamily.
     *
     * @param id the id of the accountFamily to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/account-families/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAccountFamily(@PathVariable String id) {
        log.debug("REST request to delete AccountFamily : {}", id);
        accountFamilyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("accountFamily", id.toString())).build();
    }

    public AccountFamilyInfoDTO createAccountFamilyInfoDTO(List<Teacher> teachers, List<Guardian> guardians){
        AccountFamilyInfoDTO accountFamilyInfoDTO = new AccountFamilyInfoDTO();
        if(teachers!=null){
            for(Teacher teacher : teachers){
                TeacherInfoDTO teacherInfoDTO = new TeacherInfoDTO();
                //User user = userService.findOneByTeacherId(teacher.getId());
                teacherInfoDTO.setName(teacher.getName());
                teacherInfoDTO.setTeacherType(teacher.getTeacherType());
                /*if(user != null){
                    teacherInfoDTO.setPhotoURL(user.getPhotoURL());
                }*/
                teacherInfoDTO.setPhotoURL(teacher.getPhotoURL());
                /*if(teacherInfoDTO.getPhotoURL()==null){
                    teacherInfoDTO.setPhotoURL("http://120.76.233.204:8080/cdm/content/images/hipster.png");
                }*/
                accountFamilyInfoDTO.getTeachers().add(teacherInfoDTO);
            }
        }
        if(guardians!=null){
            for(Guardian guardian: guardians){
                GuardianInfoDTO guardianInfoDTO = new GuardianInfoDTO();
                //User user = userService.findOneByGuardianId(guardian.getId());
                guardianInfoDTO.setName(guardian.getName());
                guardianInfoDTO.setRelationship(guardian.getRelationship());
                /*if(user != null){
                    guardianInfoDTO.setPhotoURL(user.getPhotoURL());
                }*/
                guardianInfoDTO.setPhotoURL(guardian.getPhotoURL());
                /*if(guardianInfoDTO.getPhotoURL()==null){
                    guardianInfoDTO.setPhotoURL("http://120.76.233.204:8080/cdm/content/images/hipster.png");
                }*/
                accountFamilyInfoDTO.getGuardians().add(guardianInfoDTO);
            }
        }
        return accountFamilyInfoDTO;
    }

    public List<StudentInfoDTO> createStudentInfoDTOs(Teacher teacher){
        List<StudentInfoDTO> studentInfoDTOs = new ArrayList<StudentInfoDTO>();
        List<AccountFamily> accountFamilies = teacher.getAccountFamilies();
        if(accountFamilies!=null){
            for(AccountFamily accountFamily : accountFamilies){
                StudentInfoDTO studentInfoDTO = new StudentInfoDTO();
                Student student = studentService.findOneByAccountFamilyId(accountFamily.getId());
                studentInfoDTO.setName(student.getName());
                studentInfoDTO.setAccountFamilyName(accountFamily.getName());
                studentInfoDTO.setAccountFamilyId(accountFamily.getId());
                accountFamily = accountFamilyService.findOne(accountFamily.getId());
                studentInfoDTO.setPhotoURL(accountFamily.getStudentAvatarURL());
                /*Optional<User> userOptional = userService.findOneByStudentId(student.getId());
                if(userOptional.isPresent()){
                    studentInfoDTO.setPhotoURL(userOptional.get().getPhotoURL());
                }*/
                //studentInfoDTO.setPhotoURL(accountFamily.getStudentAvatarURL());
                /*if(studentInfoDTO.getPhotoURL()==null){
                    studentInfoDTO.setPhotoURL("http://120.76.233.204:8080/cdm/content/images/hipster.png");
                }*/
                studentInfoDTOs.add(studentInfoDTO);
            }
        }
        return studentInfoDTOs;
    }
}
