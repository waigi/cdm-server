package com.vigo.cdm.web.rest.dto;

/**
 * Created by weill on 16/11/28.
 */
public class DeleteResponse {

    private boolean success;


    public DeleteResponse(boolean response){
        this.success = response;
    }
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
