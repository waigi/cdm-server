package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.Lists;
import com.vigo.cdm.domain.*;
import com.vigo.cdm.domain.util.DomainUtil;
import com.vigo.cdm.service.TaskService;
import com.vigo.cdm.web.rest.dto.DeleteResponse;
import com.vigo.cdm.web.rest.dto.TaskDTO;
import com.vigo.cdm.web.rest.util.FilePathUtil;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing Task.
 */
@RestController
@RequestMapping("/api")
public class TaskResource {

    private final Logger log = LoggerFactory.getLogger(TaskResource.class);

    @Inject
    private TaskService taskService;

    /*@RequestMapping(value = "/tasks",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Task> createTask(@RequestBody Task task) throws URISyntaxException {
        log.debug("REST request to save Task : {}", task);
        if (task.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("task", "idexists", "A new task cannot already have an ID")).body(null);
        }
        Task result = taskService.save(task);
        return ResponseEntity.created(new URI("/api/tasks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("task", result.getId().toString()))
            .body(result);
    }*/
    /**
     * POST  /tasks : Create a new task.
     *
     * @param   accountFamilyId is the accountFamily id
     * @return the ResponseEntity with status 201 (Created) and with body the new task, or with status 400 (Bad Request) if the task has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tasks/generateTask/{accountFamilyId}",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TaskDTO> createTask(@PathVariable String accountFamilyId) throws URISyntaxException {
        log.debug("REST request to save Task for accountFamily id: {}", accountFamilyId);
        if (accountFamilyId == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("task", "accountFamilyIdNull", "A new task cannot have not an family account ID")).body(null);
        }
        Task result = new Task();
        AccountFamily accountFamily = new AccountFamily();
        accountFamily.setId(accountFamilyId);
        result.setAccountFamily(accountFamily);
        result = taskService.save(result);
        return ResponseEntity.created(new URI("/api/tasks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("task", result.getId()))
            .body(new TaskDTO(result));
    }

    /*@RequestMapping(value="/tasks/downloadMedia", method=RequestMethod.GET)
    public void getDownloadStreamingMedia(@RequestParam(value = "file_path", required = true) String file_path,
                            @RequestParam(value = "file_name", required = true) String file_name,
                            HttpServletResponse response) throws IOException {

        // Get your file stream from wherever.
        InputStream myStream = new FileInputStream(file_path);
        // Set the content type and attachment header.
        response.addHeader("Content-disposition", "attachment;filename="+file_name);
        response.setContentType("txt/plain");

        // Copy the stream to the response's output stream.
        IOUtils.copy(myStream, response.getOutputStream());
        response.flushBuffer();
    }*/

    /**
     * PUT  /tasks : Updates an existing task.
     *
     * @param task the task to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@RequestMapping(value = "/tasks",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Task> updateTask(@RequestBody Task task) throws URISyntaxException {
        log.debug("REST request to update Task : {}", task);
        if (task.getId() == null) {
            return createTask(task);
        }
        Task result = taskService.save(task);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("task", task.getId().toString()))
            .body(result);
    }*/

    /**
     * PUT  /tasks : Updates an existing task. When the field of task is empty, it does not cover
     *
     * @param task the task to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tasks/update",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TaskDTO> updateTask(@RequestBody Task task) throws URISyntaxException {
        log.debug("REST request to update Task : {}", task);
        if (task.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Task result = taskService.findOne(task.getId());
        result = onlyUpdateTask(result, task);
        result = taskService.save(result);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("task", task.getId().toString()))
            .body(new TaskDTO(result));
    }

    @RequestMapping(value = "/tasks/updateAllTime",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TaskDTO> updateTaskAllTime(@RequestBody Task task){
        log.debug("REST request to update Task : {}", task);
        if (task.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Task result = taskService.findOne(task.getId());
            result.setPlannedStartTime(task.getPlannedStartTime());
            result.setPlannedEndTime(task.getPlannedEndTime());
            result.setActualStartTime(task.getActualStartTime());
            result.setActualEndTime(task.getActualEndTime());
            result.setDiscount(countTaskDiscount(result.getPlannedStartTime(),result.getPlannedEndTime(),
                result.getActualStartTime(),result.getActualEndTime(), result.getAccountFamily().getId(), result.getTaskCategory()));
        result = taskService.save(result);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("task", task.getId().toString()))
            .body(new TaskDTO(result));
    }

    public Task onlyUpdateTask(Task result, Task taskParam){
        if(taskParam.getDescription()!=null){
            result.setDescription(taskParam.getDescription());
        }

        //AccountFamily Id
        if(taskParam.getAccountFamily()!=null){
            if(result.getAccountFamily()==null){
                result.setAccountFamily(new AccountFamily());
            }
            if(taskParam.getAccountFamily().getId()!=null){
                result.getAccountFamily().setId(taskParam.getAccountFamily().getId());
            }
            if(taskParam.getAccountFamily().getName()!=null){
                result.getAccountFamily().setName(taskParam.getAccountFamily().getName());
            }
            if(taskParam.getAccountFamily().getDescription()!=null){
                result.getAccountFamily().setDescription(taskParam.getAccountFamily().getDescription());
            }
        }

        if(taskParam.getTaskCategory()!= DomainUtil.task_category_initial){
            result.setTaskCategory(taskParam.getTaskCategory());
        }
        if(taskParam.getPromise()!= null && taskParam.getPromise().getId()!=null){
            result.setPromise(taskParam.getPromise());
        }
        //S
        if(taskParam.getPlannedStartTime()!=null){
            result.setPlannedStartTime(taskParam.getPlannedStartTime());
        }
        if(taskParam.getPlannedEndTime()!=null){
            result.setPlannedEndTime(taskParam.getPlannedEndTime());
        }
        if(taskParam.getActualStartTime() !=null){
            result.setActualStartTime(taskParam.getActualStartTime());
        }
        if(taskParam.getActualEndTime() !=null){
            result.setActualEndTime(taskParam.getActualEndTime());
            result.setDiscount(countTaskDiscount(result.getPlannedStartTime(),result.getPlannedEndTime(),
                result.getActualStartTime(),result.getActualEndTime(), result.getAccountFamily().getId(), result.getTaskCategory()));
        }

        //N
        if(taskParam.getnPhotos() != null){
            result.setnPhotos(taskParam.getnPhotos());
        }
        if(taskParam.getnTexts() != null){
            result.setnTexts(taskParam.getnTexts());
        }
        if(taskParam.getnAudios() != null){
            result.setnAudios(taskParam.getnAudios());
        }
        if(taskParam.getnVideos() != null){
            result.setnVideos(taskParam.getnVideos());
        }
        /*if(taskParam.getDiscount() != null){
            result.setDiscount(taskParam.getDiscount());
        }*/
        /*if(taskParam.getnVideoThumbnailURLStrings() != null && !taskParam.getnVideoThumbnailURLStrings().isEmpty()){
            result.setnVideoThumbnailURLStrings(taskParam.getnVideoThumbnailURLStrings());
        }*/
        //J
        if(taskParam.getjPhotos() != null){
            result.setjPhotos(taskParam.getjPhotos());
        }
        if(taskParam.getjTexts() != null){
            result.setjTexts(taskParam.getjTexts());
        }
        if(taskParam.getjAudios() != null){
            result.setjAudios(taskParam.getjAudios());
        }
        if(taskParam.getjVideos() != null){
            result.setjVideos(taskParam.getjVideos());
        }
        if(taskParam.getjDemerits() != null){
            result.setjDemerits(taskParam.getjDemerits());
        }

        if(taskParam.getpGuardianText() !=null){
            result.setpGuardianText(taskParam.getpGuardianText());
        }
        if(taskParam.getpSelfText() !=null){
            result.setpSelfText(taskParam.getpSelfText());
        }
        if(taskParam.getpTeacherText() !=null){
            result.setpTeacherText(taskParam.getpTeacherText());
        }
        return result;
    }

    public Double countTaskDiscount(Date plannedStartTime, Date plannedEndTime, Date actualStartTime, Date actualEndTime, String accountFamilyId, int taskCategory){
        double result = 1.0;
        if(plannedStartTime == null || plannedEndTime == null
            ||actualStartTime == null || actualEndTime == null){
            return result;
        }
        if(taskCategory != DomainUtil.task_category_initial && taskCategory != DomainUtil.task_category_family && taskCategory != DomainUtil.task_category_promise) {

            if(accountFamilyId != null && plannedStartTime.equals(actualStartTime) && actualEndTime.equals(plannedEndTime)){
                Date dateTime = new Date();
                if(plannedEndTime.before(dateTime) && actualEndTime.before(dateTime)) {
                    List<Task> tasks = taskService.findTasksByAccountFamilyIdAndPlannedStartTimeFromAWeek(accountFamilyId, plannedStartTime);
                    if (tasks != null) {
                        double taskDiscountTotal = 0;
                        int count = 0;
                        for (Task task : tasks) {
                            if (task.getTaskCategory() != DomainUtil.task_category_initial && task.getTaskCategory() != DomainUtil.task_category_family
                                && task.getTaskCategory() != DomainUtil.task_category_promise && task.getActualStartTime()!=null
                                && task.getActualStartTime() != null) {
                                taskDiscountTotal += task.getDiscount();
                                count++;
                            }
                        }
                        if (count != 0 && taskDiscountTotal != 0) {
                            return taskDiscountTotal / count;
                        }
                    }
                }
            }
        }
        long a1 = plannedStartTime.getTime();
        long a2 = plannedEndTime.getTime();
        long b1 = actualStartTime.getTime();
        long b2 = actualEndTime.getTime();
        long union = b2 + a2 - b1 - a1;
        double intersection = 0.0;
        if(b2 > a2 && b1 >= a1){
            intersection = a2 - b1;
        }else if(b2 > a2 && b1 < a1){
            intersection = a2 - a1;
        }else if(b2 <= a2 && b1>=a1){
            intersection = b2 - b1;
        }else if(b2 <= a2 && b1< a1){
            intersection = b2 - a1;
        }
        if(intersection <0){
            intersection = 0.0;
        }
        result = intersection/(union-intersection);
        result = ((double)Math.round(result*10))/10;
        //result = (result*100)/100;
        return result;
    }
    /**
     * PUT  /tasks : Updates an existing task. All fields Cover!
     *
     * @param task the task to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tasks/updateAndCover",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TaskDTO> updateAndCoverTask(@RequestBody Task task) throws URISyntaxException {
        log.debug("REST request to update Task : {}", task);
        if (task.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        task.setDiscount(countTaskDiscount(task.getPlannedStartTime(), task.getPlannedEndTime(), task.getActualStartTime(), task.getActualEndTime(),task.getAccountFamily().getId(),task.getTaskCategory()));
        Task result = taskService.save(task);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("task", task.getId().toString()))
            .body(new TaskDTO(result));
    }


    /**
     * POST  /tasks/upload : upload the file for task.
     *
     * @param task_type is the task type(1:N, 2:J)
     * @param file_type is the file type(1:Photo, 2:Audio, 3:Video)
     * @param task_id is the task id(String)
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     *
     */
    @RequestMapping(value = "/tasks/upload",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TaskDTO> uploadFileTask(@RequestParam(value = "task_type", required = true) int task_type,
                                               @RequestParam(value = "file_type", required = true) int file_type,
                                               @RequestParam(value = "task_id", required = true) String task_id,
                                               @RequestParam(value="file",required = true) MultipartFile file,
                                               @RequestParam(value = "audio_length", required = false, defaultValue = "0") Integer audio_length,
                                               HttpServletRequest request
                                               ) {
        String basePath =request.getSession().getServletContext().getRealPath("/");  //根路径
        String baseUrl = request.getScheme() + // "http"
            "://" +                                // "://"
            request.getServerName() +              // "myhost"
            ":" +                                  // ":"
            request.getServerPort() +              // "80"
            request.getContextPath();               // "/myContextPath" or "" if deployed in root context
        //System.out.println(baseUrl);
        Task task = taskService.findOne(task_id);
        if (!file.isEmpty()) {
            //文件夹相对路径
            String folderRelativePath = uploadFileTaskRelativePathURL(task_type, file_type, task.getAccountFamily().getId(), task_id);
            String folderAbsolutePath = basePath + folderRelativePath;
            UUID uuid = UUID.randomUUID();
            String fileName = uuid.toString() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            String imageName = uuid +".jpg";

            //String imageBaseRelativePath = "/content/images/logo-jhipster.png";
            String imageRelativePath = folderRelativePath + FilePathUtil.pathForwardSlash+ imageName;
            String imageAbsolutePath = basePath + imageRelativePath;

            //文件相对路径
            String fileRelativePath = folderRelativePath + FilePathUtil.pathForwardSlash+ fileName;
            //文件绝对路径
            String fileAbsolutePath = basePath + fileRelativePath;

            if(saveFileFromInputStream(file, basePath, folderRelativePath, fileRelativePath)){
                //filePath = baseUrl + "/" + filePath;
                //List<String> fileURLs = new ArrayList<String>();
                //fileURLs.add(filePath);
                task = updateTask(task_type, file_type, task, baseUrl, fileRelativePath, fileAbsolutePath ,fileName,imageRelativePath, imageAbsolutePath,audio_length);
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("task", task.getId().toString()))
                    .body(new TaskDTO(task));
            }else{
                new ResponseEntity<>(new TaskDTO(task),HttpStatus.NOT_MODIFIED);
            }
        }
        return new ResponseEntity<>(new TaskDTO(task),HttpStatus.NOT_MODIFIED);
    }

    /**
     * GET  /tasks : get all the tasks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tasks in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/tasks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TaskDTO>> getAllTasks(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tasks");
        Page<Task> page = taskService.findAll(pageable);
        List<TaskDTO> taskDTOs = page.getContent().stream()
            .map(TaskDTO::new)
            .collect(Collectors.toList());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tasks");
        return new ResponseEntity<>(taskDTOs, headers, HttpStatus.OK);
    }

    /**
     * GET  /tasks : get all the tasks of the family account.
     *
     * @param accountFamilyId
     * @return the ResponseEntity with status 200 (OK) and the list of tasks in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/tasks/familyTasks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TaskDTO>> getFamilyTasks(@RequestParam(value = "accountFamily_id", required = true) String accountFamilyId,
                                                        @RequestParam(value = "planned_startTime", required = true) String planned_startTime,
                                                        @RequestParam(value = "planned_endTime", required = true) String planned_endTime) {
        log.debug("REST request to get a page of Tasks");
        if(accountFamilyId == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        //List<Task> tasks = taskService.findTasksByAccountFamilyIdAndPlannedStartTime(accountFamilyId, planned_startTime);
        List<Task> tasks = taskService.findTasksByAccountFamilyIdAndPlannedStartTimeAndPlannedEndTime(accountFamilyId, planned_startTime, planned_endTime);
        List<TaskDTO> taskDTOs = tasks.stream()
            .map(TaskDTO::new)
            .collect(Collectors.toList());
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tasks");
        return new ResponseEntity<>(taskDTOs, HttpStatus.OK);
    }

    /**
     * GET  /tasks/:id : get the "id" task.
     *
     * @param id the id of the task to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the task, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/tasks/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TaskDTO> getTask(@PathVariable String id) {
        log.debug("REST request to get Task : {}", id);
        Task task = taskService.findOne(id);
        return Optional.ofNullable(task)
            .map(result -> new ResponseEntity<>(
                new TaskDTO(task),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tasks/:id : delete the "id" task.
     *
     * @param id the id of the task to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/tasks/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DeleteResponse> deleteTask(@PathVariable String id) {
        log.debug("REST request to delete Task : {}", id);
        taskService.delete(id);
        //return new ResponseEntity<>(new DeleteResponse(true), HttpStatus.OK);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert("task id", id))
            .body(new DeleteResponse(true));
        //return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("task", id.toString())).build();
    }

    public Task updateTask(int task_type, int file_type, Task task, String baseUrl, String fileRelativePath, String fileAbsolutePath, String fileName
                            ,String imageRelativePath, String imageAbsolutePath,Integer audio_length){
        String urlPhoto = baseUrl + "/"+ fileRelativePath;
        //String  urlStreamingMedia = baseUrl + "/tasks/downloadStreamingMedia?file_path="+fileAbsolutePath+"&file_name="+fileName;

        if(task_type == FilePathUtil.task_type_n){
            if(file_type == FilePathUtil.file_type_photo){
                if(task.getnPhotos()==null){
                    List<Photo> nPhotos = new ArrayList<Photo>();
                    nPhotos.add(new Photo(urlPhoto));
                    task.setnPhotos(nPhotos);
                }else{
                    task.getnPhotos().add(new Photo(urlPhoto));
                }
                //task.getnPhotoURLStrings().add(urlPhoto);
            }else if(file_type == FilePathUtil.file_type_Audio){
                if(task.getnAudios()==null){
                    List<Audio> nAudios = new ArrayList<Audio>();
                    nAudios.add(new Audio(urlPhoto, audio_length));
                    task.setnAudios(nAudios);
                }else{
                    task.getnAudios().add(new Audio(urlPhoto, audio_length));
                }
                //task.getnAudioURLStrings().add(urlPhoto);
            }else if(file_type == FilePathUtil.file_type_Video){
                /*if(task.getnVideos()==null){
                    List<Video> nVideos = new ArrayList<Video>();
                    nVideos.add(new Video(urlPhoto));
                    task.setnVideoURLStrings(nVideoURLStrings);
                }else{
                    task.getnVideoURLStrings().add(urlPhoto);
                }*/
                if(saveVideoThumbnail(fileAbsolutePath, imageAbsolutePath)){
                    if(task.getnVideos()==null){
                        List<Video> nVideos = new ArrayList<Video>();
                        nVideos.add(new Video(urlPhoto ,baseUrl + "/"+ imageRelativePath));
                        task.setnVideos(nVideos);
                    }else{
                        task.getnVideos().add(new Video(urlPhoto ,baseUrl + "/"+ imageRelativePath));
                    }
                }else{
                    if(task.getnVideos()==null){
                        List<Video> nVideos = new ArrayList<Video>();
                        nVideos.add(new Video(urlPhoto ,baseUrl +"/content/images/hipster.png"));
                        task.setnVideos(nVideos);
                    }else{
                        task.getnVideos().add(new Video(urlPhoto ,baseUrl +"/content/images/hipster.png"));
                    }
                }
                /*if(saveVideoThumbnail(fileAbsolutePath, imageAbsolutePath)){
                    if(task.getnVideoThumbnailURLStrings()==null){
                        List<String> nVideoThumbnailURLStrings = new ArrayList<String>();
                        nVideoThumbnailURLStrings.add(baseUrl + "/"+ imageRelativePath);
                        task.setnVideoThumbnailURLStrings(nVideoThumbnailURLStrings);
                    }else{
                        task.getnVideoThumbnailURLStrings().add(baseUrl + "/"+ imageRelativePath);
                    }
                }else{
                    if(task.getnVideoThumbnailURLStrings()==null){
                        List<String> nVideoThumbnailURLStrings = new ArrayList<String>();
                        nVideoThumbnailURLStrings.add(baseUrl + "/content/images/hipster.png");
                        task.setnVideoThumbnailURLStrings(nVideoThumbnailURLStrings);
                    }else{
                        task.getnVideoThumbnailURLStrings().add(baseUrl + "/content/images/hipster.png");
                    }
                }*/
            }
        }else if(task_type == FilePathUtil.task_type_j){
            if(file_type == FilePathUtil.file_type_photo){
                if(task.getjPhotos()==null){
                    List<Photo> jPhotos = new ArrayList<Photo>();
                    jPhotos.add(new Photo(urlPhoto));
                    task.setjPhotos(jPhotos);
                }else{
                    task.getjPhotos().add(new Photo(urlPhoto));
                }
                //task.getjPhotoURLStrings().add(urlPhoto);
            }else if(file_type == FilePathUtil.file_type_Audio){
                //task.getjAudioURLStrings().add(urlPhoto);
                if(task.getnAudios()==null){
                    List<Audio> jAudios = new ArrayList<Audio>();
                    jAudios.add(new Audio(urlPhoto, audio_length));
                    task.setjAudios(jAudios);
                }else{
                    task.getjAudios().add(new Audio(urlPhoto, audio_length));
                }
            }else if(file_type == FilePathUtil.file_type_Video){
                if(saveVideoThumbnail(fileAbsolutePath, imageAbsolutePath)){
                    if(task.getnVideos() ==null){
                        List<Video> jVideos = new ArrayList<Video>();
                        jVideos.add(new Video(urlPhoto ,baseUrl + "/"+ imageRelativePath));
                        task.setjVideos(jVideos);
                    }else{
                        task.getjVideos().add(new Video(urlPhoto ,baseUrl + "/"+ imageRelativePath));
                    }
                }else{
                    if(task.getnVideos() ==null){
                        List<Video> jVideos = new ArrayList<Video>();
                        jVideos.add(new Video(urlPhoto ,baseUrl +"/content/images/hipster.png"));
                        task.setjVideos(jVideos);
                    }else{
                        task.getjVideos().add(new Video(urlPhoto ,baseUrl +"/content/images/hipster.png"));
                    }
                }
                /*if(task.getjVideoURLStrings()==null){
                    List<String> jVideoURLStrings = new ArrayList<String>();
                    jVideoURLStrings.add(urlPhoto);
                    task.setjVideoURLStrings(jVideoURLStrings);
                }else{
                    task.getjVideoURLStrings().add(urlPhoto);
                }
                if(saveVideoThumbnail(fileAbsolutePath, imageAbsolutePath)){
                    if(task.getjVideoThumbnailURLStrings()==null){
                        List<String> jVideoThumbnailURLStrings = new ArrayList<String>();
                        jVideoThumbnailURLStrings.add(baseUrl + "/"+ imageRelativePath);
                        task.setjVideoThumbnailURLStrings(jVideoThumbnailURLStrings);
                    }else{
                        task.getjVideoThumbnailURLStrings().add(baseUrl + "/"+ imageRelativePath);
                    }
                }else{
                    if(task.getjVideoThumbnailURLStrings()==null){
                        List<String> jVideoThumbnailURLStrings = new ArrayList<String>();
                        jVideoThumbnailURLStrings.add(baseUrl + "/content/images/hipster.png");
                        task.setjVideoThumbnailURLStrings(jVideoThumbnailURLStrings);
                    }else{
                        task.getjVideoThumbnailURLStrings().add(baseUrl + "/content/images/hipster.png");
                    }
                }*/
            }
        }
        task = taskService.save(task);
        return task;
    }

    //文件夹相对路径
    public String uploadFileTaskRelativePathURL(int task_type, int file_type, String accountFamilyId, String task_id){
        String filePath = FilePathUtil.pathConstant+accountFamilyId+FilePathUtil.pathForwardSlash+task_id;
        if(task_type == FilePathUtil.task_type_n){
            filePath = filePath  + FilePathUtil.pathNOfTask;
        }else if(task_type == FilePathUtil.task_type_j){
            filePath = filePath  + FilePathUtil.pathJOfTask;
        }
        if(file_type == FilePathUtil.file_type_photo){
            filePath = filePath  + FilePathUtil.pathPhoto;
        }else if(file_type == FilePathUtil.file_type_Audio){
            filePath = filePath  + FilePathUtil.pathAudio;
        }else if(file_type == FilePathUtil.file_type_Video){
            filePath = filePath  + FilePathUtil.pathVideo;
        }
        /*File fileDownload = new File(filePath);
        if(!fileDownload.exists()){
            if(fileDownload.mkdirs()){
                System.out.println("path create");
            }
        }*/

        return filePath;
    }

    public Boolean saveFileFromInputStream(MultipartFile file,String basePath, String folderPath, String filePath) {
        Boolean result = false;
        if (file==null || file.isEmpty()) {
            return result;
        }
        File fileUpload = new File(basePath + folderPath);
        if(!fileUpload.exists()){
            if(!fileUpload.mkdirs()){
                return result;
            }
        }
        try {
            InputStream stream = file.getInputStream();
            FileOutputStream fs = new FileOutputStream(basePath + filePath);
            byte[] buffer = new byte[1024 * 1024];
            int bytesum = 0;
            int byteread = 0;
            while ((byteread = stream.read(buffer)) != -1) {
                bytesum += byteread;
                fs.write(buffer, 0, byteread);
                fs.flush();
            }
            fs.close();
            stream.close();
            result = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Boolean saveVideoThumbnail(String videoPath,String imagePath) {
        //String videoPath = "/Users/will/Movies/W.mov";
        //String imagePath = folderRelativePath + +".jpg";
        //ffmpeg -i xxx.mp4 -y -f image2 -t 0.001 -s 125x125 xxx.jpg
        List<String> cmd = new java.util.ArrayList<String>();
        cmd.add("ffmpeg");// 视频提取工具的位置
        cmd.add("-i");
        cmd.add(videoPath);
        cmd.add("-y");
        cmd.add("-f");
        cmd.add("image2");
        cmd.add("-t");
        cmd.add("0.001");
        cmd.add("-s");
        cmd.add("137x137");
        cmd.add(imagePath);
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(cmd);
        try {
            builder.start();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


    }

    /*public static void main(String[] args){

            double result = 0.0;
            long a1 = 1484375700000;
            long a2 = 1484376000000;
            long b1 = 1484375709933;
            long b2 = 1484375883137;
            long union = b2 + a2 - b1 - a1;
            long intersection = 0;
            if(b2 > a2 && b1 >= a1){
                intersection = a2 - b1;
            }else if(b2 > a2 && b1 < a1){
                intersection = a2 - a1;
            }else if(b2 <= a2 && b1>=a1){
                intersection = b2 - b1;
            }else if(b2 <= a2 && b1< a1){
                intersection = b2 - a1;
            }
            if(intersection <0){
                intersection = 0;
            }
            result = intersection/(union-intersection);
            result = ((double)Math.round(result*10))/10;
        System.out.println(result);


    }*/

    /*public static void main(String[] args){
        Date dates = new Date();
        dates.getTime();
        System.out.println(dates.getTime());

        Date startTime = new Date(Long.valueOf("1489374259000"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        System.out.println(startTime);

        *//*Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
        Date fromDate = fromCalendar.getTime();

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(fromDate);
        toCalendar.set(Calendar.DATE, toCalendar.get(Calendar.DATE) + 1);
        Date toDate = toCalendar.getTime();


        System.out.println("fromData="+fromDate);
        System.out.println("toDate="+toDate);*//*


    }*/
}
