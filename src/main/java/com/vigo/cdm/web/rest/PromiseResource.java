package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.domain.Audio;
import com.vigo.cdm.domain.Promise;
import com.vigo.cdm.domain.Task;
import com.vigo.cdm.domain.util.DomainUtil;
import com.vigo.cdm.service.AccountFamilyService;
import com.vigo.cdm.service.PromiseService;
import com.vigo.cdm.service.TaskService;
import com.vigo.cdm.web.rest.dto.PromiseDTO;
import com.vigo.cdm.web.rest.util.FilePathUtil;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing Promise.
 */
@RestController
@RequestMapping("/api")
public class PromiseResource {

    private final Logger log = LoggerFactory.getLogger(PromiseResource.class);

    @Inject
    private PromiseService promiseService;

    @Inject
    private AccountFamilyService accountFamilyService;

    @Inject
    private TaskService taskService;
    /**
     * POST  /promises : Create a new promise.
     * @param task_id is the task id
     * @return the ResponseEntity with status 201 (Created) and with body the new promise, or with status 400 (Bad Request) if the promise has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/promises/generatePromise",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> createPromise(@RequestParam(value = "task_id", required = true) String task_id) throws URISyntaxException {
        //log.debug("REST request to save Promise for accountFamily id: {}", accountFamilyId);
        Task task= taskService.findOne(task_id);
        if (task == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("task", "taskNull", "do not find task by task ID")).body(null);
        }

        AccountFamily accountFamily = accountFamilyService.findOne(task.getAccountFamily().getId());

        if (accountFamily == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("promise", "accountFamilyIdNull", "A new promise cannot have not an family account ID")).body(null);
        }
        Promise result = new Promise();

        accountFamily.setPromiseSequenceNumber(accountFamily.getPromiseSequenceNumber() + 1);
        accountFamilyService.save(accountFamily);
        result.setSequenceNumber(accountFamily.getPromiseSequenceNumber());
        result.setAccountFamily(accountFamily);
        result.setPromiseTime(new Date());
        //result.setTask(new Task(taskId));
        result = promiseService.save(result);
        task.setPromise(new Promise(result.getId()));
        taskService.save(task);

        return ResponseEntity.created(new URI("/api/promises/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("promise", result.getId().toString()))
            .body(new PromiseDTO(result));
    }

    /**
     * PUT  /promises : task Embed an existing promise. When the field of promise is empty, it does not Embed
     *
     * @param task_id the task to update
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/promises/taskEmbedPromise",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> taskEmbedPromise(@RequestParam(value = "task_id", required = true) String task_id,
                                                         @RequestParam(value = "promise_id", required = true) String promise_id) {
        Task task= taskService.findOne(task_id);
        Promise promise = promiseService.findOne(promise_id);

        if (task == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("task", "taskNull", "do not find task by task ID")).body(null);
        }
        if (promise == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("promise", "promiseNull", "do not find promise by task ID")).body(null);
        }
        task.setPromise(new Promise(promise_id));
        task = taskService.save(task);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("task id", task.getId().toString()))
            .body(new PromiseDTO(promise));
    }
    /**
     * PUT  /promises : Updates an existing promise.
     *
     * @param promise the promise to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated promise,
     * or with status 400 (Bad Request) if the promise is not valid,
     * or with status 500 (Internal Server Error) if the promise couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@RequestMapping(value = "/promises",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Promise> updatePromise(@RequestBody Promise promise) throws URISyntaxException {
        log.debug("REST request to update Promise : {}", promise);
        *//*if (promise.getId() == null) {
            return createPromise(promise);
        }*//*
        Promise result = promiseService.save(promise);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("promise", promise.getId().toString()))
            .body(result);
    }*/

    /**
     * PUT  /promises : Updates an existing promise. When the field of task is empty, it does not cover
     *
     * @param promise the promise to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/promises/update",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> updatePromise(@RequestBody Promise promise) throws URISyntaxException {
        log.debug("REST request to update Promise : {}", promise);
        if (promise.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Promise result = promiseService.findOne(promise.getId());
        result = onlyUpdatePromise(result, promise);
        result = promiseService.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("promise", promise.getId().toString()))
            .body(new PromiseDTO(result));
    }

    /**
     * PUT  /tasks : Updates an existing promise. All fields Cover!
     *
     * @param promise the task to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/promises/updateAndCover",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> updateAndCoverTask(@RequestBody Promise promise) throws URISyntaxException {
        log.debug("REST request to update promise : {}", promise);
        if (promise.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Promise result = promiseService.save(promise);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("task", promise.getId().toString()))
            .body(new PromiseDTO(result));
    }

    public Promise onlyUpdatePromise(Promise result, Promise promiseParam){

        //AccountFamily Id
        if(promiseParam.getAccountFamily()!=null){
            /*if(result.getAccountFamily()==null){
                result.setAccountFamily(new AccountFamily());
            }*/
            if(promiseParam.getAccountFamily().getId()!=null){
                result.getAccountFamily().setId(promiseParam.getAccountFamily().getId());
            }
            //result.setAccountFamily(promiseParam.getAccountFamily());
            if(promiseParam.getAccountFamily().getName()!=null){
                result.getAccountFamily().setName(promiseParam.getAccountFamily().getName());
            }
            if(promiseParam.getAccountFamily().getDescription()!=null){
                result.getAccountFamily().setDescription(promiseParam.getAccountFamily().getDescription());
            }
        }
        if(promiseParam.getPromiseAudio()!=null){
            result.setPromiseAudio(promiseParam.getPromiseAudio());
        }
        if(promiseParam.getPromiseGuardianAudio()!=null){
            result.setPromiseGuardianAudio(promiseParam.getPromiseGuardianAudio());
        }
        if(promiseParam.getPromiseTeacherAudio()!=null){
            result.setPromiseTeacherAudio(promiseParam.getPromiseTeacherAudio());
        }

        if(promiseParam.getPromiseContent()!=null){
            result.setPromiseContent(promiseParam.getPromiseContent());
        }
        /*if(promiseParam.getTask()!=null){
            result.setTask(promiseParam.getTask());
        }*/
        if (promiseParam.getPromiseStatus()!= DomainUtil.promiseStatus_notStarted){
            result.setPromiseStatus(promiseParam.getPromiseStatus());
        }
        if(promiseParam.getPromiseTime() != null){
            result.setPromiseTime(promiseParam.getPromiseTime());
        }
        if(promiseParam.getPromiseStartTime() != null){
            result.setPromiseStartTime(promiseParam.getPromiseStartTime());
        }
        //Personality
        if(promiseParam.getPersonalityPromiseStatus()!= DomainUtil.promiseStatus_notStarted){
            result.setPersonalityPromiseStatus(promiseParam.getPersonalityPromiseStatus());
        }
        if(promiseParam.getPersonalityStartTime() != null){
            result.setPersonalityStartTime(promiseParam.getPersonalityStartTime());
        }
        if(promiseParam.getPersonalityChildAudio()!=null){
            result.setPersonalityChildAudio(promiseParam.getPersonalityChildAudio());
        }
        if(promiseParam.getPersonalityGuardianAudio()!=null){
            result.setPersonalityGuardianAudio(promiseParam.getPersonalityGuardianAudio());
        }
        if(promiseParam.getPersonalityTeacherAudio()!=null){
            result.setPersonalityTeacherAudio(promiseParam.getPersonalityTeacherAudio());
        }
        //tag
        if(promiseParam.getTagPromiseStatus()!=DomainUtil.promiseStatus_notStarted){
            result.setTagPromiseStatus(promiseParam.getTagPromiseStatus());
        }
        if(promiseParam.getTagStartTime() != null){
            result.setTagStartTime(promiseParam.getTagStartTime());
        }
        if(promiseParam.getTagChildAudio()!=null){
            result.setTagChildAudio(promiseParam.getTagChildAudio());
        }
        if(promiseParam.getTagGuardianAudio()!=null){
            result.setTagGuardianAudio(promiseParam.getTagGuardianAudio());
        }
        if(promiseParam.getTagTeacherAudio()!=null){
            result.setTagTeacherAudio(promiseParam.getTagTeacherAudio());
        }
        //resource
        if(promiseParam.getResourcePromiseStatus()!=DomainUtil.promiseStatus_notStarted){
            result.setResourcePromiseStatus(promiseParam.getResourcePromiseStatus());
        }
        if(promiseParam.getResourceStartTime() !=null){
            result.setResourceStartTime(promiseParam.getResourceStartTime());
        }
        if(promiseParam.getResourceChildAudio()!=null){
            result.setResourceChildAudio(promiseParam.getResourceChildAudio());
        }
        if(promiseParam.getResourceGuardianAudio()!=null){
            result.setResourceGuardianAudio(promiseParam.getResourceGuardianAudio());
        }
        if(promiseParam.getResourceTeacherAudio()!=null){
            result.setResourceTeacherAudio(promiseParam.getResourceTeacherAudio());
        }

        //grounded
        if(promiseParam.getGroundedPromiseStatus()!=DomainUtil.promiseStatus_notStarted){
            result.setGroundedPromiseStatus(promiseParam.getGroundedPromiseStatus());
        }
        if(promiseParam.getGroundedStartTime() !=null){
            result.setGroundedStartTime(promiseParam.getGroundedStartTime());
        }
        if(promiseParam.getGroundedChildAudio()!=null) {
            result.setGroundedChildAudio(promiseParam.getGroundedChildAudio());
        }
        if(promiseParam.getGroundedGuardianAudio()!=null) {
            result.setGroundedGuardianAudio(promiseParam.getGroundedGuardianAudio());
        }
        if(promiseParam.getGroundedTeacherAudio()!=null){
            result.setGroundedTeacherAudio(promiseParam.getGroundedTeacherAudio());
        }
        //criticize
        if(promiseParam.getCriticizePromiseStatus()!=DomainUtil.promiseStatus_notStarted){
            result.setCriticizePromiseStatus(promiseParam.getCriticizePromiseStatus());
        }
        if(promiseParam.getCriticizeStartTime()!=null){
            result.setCriticizeStartTime(promiseParam.getCriticizeStartTime());
        }
        if(promiseParam.getCriticizeChildAudio()!=null) {
            result.setCriticizeChildAudio(promiseParam.getCriticizeChildAudio());
        }
        if(promiseParam.getCriticizeGuardianAudio()!=null) {
            result.setCriticizeGuardianAudio(promiseParam.getCriticizeGuardianAudio());
        }
        if(promiseParam.getCriticizeTeacherAudio()!=null){
            result.setCriticizeTeacherAudio(promiseParam.getCriticizeTeacherAudio());
        }

        //FamilyMeeting
        if(promiseParam.getFamilyMeetingPromiseStatus()!=DomainUtil.promiseStatus_notStarted){
            result.setFamilyMeetingPromiseStatus(promiseParam.getFamilyMeetingPromiseStatus());
        }

        if(promiseParam.getFamilyMeetingAudios() != null && !promiseParam.getFamilyMeetingAudios().isEmpty()){
            result.setFamilyMeetingAudios(promiseParam.getFamilyMeetingAudios());
        }
        if(promiseParam.getFamilyMeetingAudioIndex()!=0){
            result.setFamilyMeetingAudioIndex(promiseParam.getFamilyMeetingAudioIndex());
        }
        if(promiseParam.getFamilyMeetingStartTime()!=null){
            result.setFamilyMeetingStartTime(promiseParam.getFamilyMeetingStartTime());
        }
        if(promiseParam.getfTags() != null) {
            result.setfTags(promiseParam.getfTags());
        }

        if(promiseParam.getiTags() != null) {
            result.setiTags(promiseParam.getiTags());
        }

        if(promiseParam.getkTags() != null) {
            result.setkTags(promiseParam.getkTags());
        }

        if(promiseParam.geteTags() != null) {
            result.seteTags(promiseParam.geteTags());
        }

        if(promiseParam.getkTags() != null){
            result.setkTags(promiseParam.getkTags());
        }

        //result = updatePromiseStatus(result);
        return result;
    }

    /**
     * POST  /tasks/upload : upload the file for task.
     *
     * @param promise_type is the task type(0:promise oneself, 1:Personality, 2:Tag, 3:Resource, 4:Grounded, 5:Criticize, 6:FamilyMeeting)
     * @param promise_person_type is the task type(1:child, 2:guardian)
     * @param promise_id is the task id(String)
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     *
     */
    @RequestMapping(value = "/promises/upload",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> uploadFilePromise(@RequestParam(value = "promise_type", required = true) int promise_type,
                                                     @RequestParam(value = "promise_person_type", required = true) int promise_person_type,
                                                  @RequestParam(value = "promise_id", required = true) String promise_id,
                                                  @RequestParam(value="file",required = true) MultipartFile file,
                                                  @RequestParam(value = "audio_length", required = false) Integer audio_length,
                                                  @RequestParam(value = "position_x", required = false, defaultValue = "0") Integer position_x,
                                                  @RequestParam(value = "position_y", required = false, defaultValue="0") Integer position_y,
                                                  HttpServletRequest request
    ) {
        String basePath =request.getSession().getServletContext().getRealPath("/");  //根路径
        String baseUrl = request.getScheme() + // "http"
            "://" +                                // "://"
            request.getServerName() +              // "myhost"
            ":" +                                  // ":"
            request.getServerPort() +              // "80"
            request.getContextPath();               // "/myContextPath" or "" if deployed in root context
        Promise promise = promiseService.findOne(promise_id);
        if (!file.isEmpty()) {
            //文件夹相对路径
            String folderRelativePath = uploadFilePromiseRelativePathURL(promise_type, promise_person_type, promise.getAccountFamily().getId(), promise_id);
            String folderAbsolutePath = basePath + folderRelativePath;
            UUID uuid = UUID.randomUUID();
            String fileName = uuid.toString() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

            //文件相对路径
            String fileRelativePath = folderRelativePath + FilePathUtil.pathForwardSlash+ fileName;
            //文件绝对路径
            String fileAbsolutePath = basePath + fileRelativePath;

            if(saveFileFromInputStream(file, basePath, folderRelativePath, fileRelativePath)){
                //filePath = baseUrl + "/" + filePath;
                //List<String> fileURLs = new ArrayList<String>();
                //fileURLs.add(filePath);
                promise = updatePromise(promise_type, promise_person_type, promise, baseUrl, fileRelativePath,audio_length, position_x, position_y);
                //promise = updatePromiseStatus(promise);
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("promise", promise.getId().toString()))
                    .body(new PromiseDTO(promise));
            }else{
                //promise = updatePromiseStatus(promise);
                new ResponseEntity<>(new PromiseDTO(promise),HttpStatus.NOT_MODIFIED);
            }
        }
        //promise = updatePromiseStatus(promise);
        return new ResponseEntity<>(new PromiseDTO(promise),HttpStatus.NOT_MODIFIED);
    }
    /**
     * GET  /promises : get all the promises.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of promises in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/promises",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Promise>> getAllPromises(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Promises");
        Page<Promise> page = promiseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/promises");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /tasks : get all the tasks of the family account.
     *
     * @param accountFamilyId
     * @return the ResponseEntity with status 200 (OK) and the list of tasks in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/promises/familyPromises",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PromiseDTO>> getFamilyPromises(@RequestParam(value = "accountFamily_id", required = true) String accountFamilyId) {
        log.debug("REST request to get a page of Tasks");
        if(accountFamilyId == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<Promise> promises = promiseService.findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(accountFamilyId);
        List<PromiseDTO> promiseDTOs = promises.stream()
            .map(PromiseDTO::new)
            .collect(Collectors.toList());
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tasks");
        return new ResponseEntity<>(promiseDTOs, HttpStatus.OK);
    }


    /**
     * GET  /promises/:id : get the "id" promise.
     *
     * @param id the id of the promise to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the promise, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/promises/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> getPromise(@PathVariable String id) {
        log.debug("REST request to get Promise : {}", id);
        Promise promise = promiseService.findOne(id);
        return Optional.ofNullable(promise)
            .map(result -> new ResponseEntity<>(
                new PromiseDTO(result),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /promises/:id : get the "id" promise.
     *
     * @param taskId the id of the promise to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the promise, or with status 404 (Not Found)
     */
    /*@RequestMapping(value = "/promises/byTask",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PromiseDTO> getPromiseByTaskId(@RequestParam(value = "task_id", required = true) String taskId) {
        log.debug("REST request to get Promise : {}", taskId);
        Promise promise = promiseService.findOneByTaskId(taskId);
        return Optional.ofNullable(promise)
            .map(result -> new ResponseEntity<>(
                new PromiseDTO(result),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(new PromiseDTO(), HttpStatus.OK));
    }*/

    /**
     * DELETE  /promises/:id : delete the "id" promise.
     *
     * @param id the id of the promise to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/promises/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePromise(@PathVariable String id) {
        log.debug("REST request to delete Promise : {}", id);
        promiseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("promise", id.toString())).build();
    }

    //文件夹相对路径
    public String uploadFilePromiseRelativePathURL(int promise_type, int promise_person_type, String accountFamilyId, String promise_id){
        String filePath = FilePathUtil.pathConstantOfPromise+accountFamilyId+FilePathUtil.pathForwardSlash+promise_id;
        if(promise_type == FilePathUtil.promise_type_promise){
            //filePath = filePath  + FilePathUtil.pathOfPromiseAudio;
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                filePath = filePath  + FilePathUtil.pathOfPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                filePath = filePath  + FilePathUtil.pathOfPromiseGuardianAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                filePath = filePath  + FilePathUtil.pathOfPromiseTeacherAudio;
            }
        }else if(promise_type == FilePathUtil.promise_type_personality){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                filePath = filePath  + FilePathUtil.pathOfPersonalityChildPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                filePath = filePath  + FilePathUtil.pathOfPersonalityGuardianPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                filePath = filePath  + FilePathUtil.pathOfPersonalityTeacherPromiseAudio;
            }

        }else if(promise_type == FilePathUtil.promise_type_tag){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                filePath = filePath  + FilePathUtil.pathOfTagChildPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                filePath = filePath  + FilePathUtil.pathOfTagGuardianPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                filePath = filePath  + FilePathUtil.pathOfTagTeacherPromiseAudio;
            }
        }else if(promise_type == FilePathUtil.promise_type_resource){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                filePath = filePath  + FilePathUtil.pathOfResourceChildPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                filePath = filePath  + FilePathUtil.pathOfResourceGuardianPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                filePath = filePath  + FilePathUtil.pathOfResourceTeacherPromiseAudio;
            }
        }else if(promise_type == FilePathUtil.promise_type_grounded){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                filePath = filePath  + FilePathUtil.pathOfGroundedChildPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                filePath = filePath  + FilePathUtil.pathOfGroundedGuardianPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                filePath = filePath  + FilePathUtil.pathOfGroundedTeacherPromiseAudio;
            }
        }else if(promise_type == FilePathUtil.promise_type_criticize){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                filePath = filePath  + FilePathUtil.pathOfCriticizeChildPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                filePath = filePath  + FilePathUtil.pathOfCriticizeGuardianPromiseAudio;
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                filePath = filePath  + FilePathUtil.pathOfCriticizeTeacherPromiseAudio;
            }
        }else if(promise_type == FilePathUtil.promise_type_family_meeting){
            filePath = filePath  + FilePathUtil.pathOfFamilyMeetingAudio;
        }
        return filePath;
    }

    public Boolean saveFileFromInputStream(MultipartFile file,String basePath, String folderPath, String filePath) {
        Boolean result = false;
        if (file==null || file.isEmpty()) {
            return result;
        }
        File fileUpload = new File(basePath + folderPath);
        if(!fileUpload.exists()){
            if(!fileUpload.mkdirs()){
                return result;
            }
        }
        try {
            InputStream stream = file.getInputStream();
            FileOutputStream fs = new FileOutputStream(basePath + filePath);
            byte[] buffer = new byte[1024 * 1024];
            int bytesum = 0;
            int byteread = 0;
            while ((byteread = stream.read(buffer)) != -1) {
                bytesum += byteread;
                fs.write(buffer, 0, byteread);
                fs.flush();
            }
            fs.close();
            stream.close();
            result = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Promise updatePromise(int promise_type, int promise_person_type, Promise promise, String baseUrl, String fileRelativePath, Integer audio_length, int position_x, int position_y){
        String urlPath = baseUrl + "/" + fileRelativePath;
        if(promise_type == FilePathUtil.promise_type_promise){
            //promise.setPromiseAudio(new Audio(urlPath, audio_length));
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                promise.setPromiseAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                promise.setPromiseGuardianAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                promise.setPromiseTeacherAudio(new Audio(urlPath, audio_length));
            }
        }else if(promise_type == FilePathUtil.promise_type_personality){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                promise.setPersonalityChildAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                promise.setPersonalityGuardianAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                promise.setPersonalityTeacherAudio(new Audio(urlPath, audio_length));
            }

        }else if(promise_type == FilePathUtil.promise_type_tag){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                promise.setTagChildAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                promise.setTagGuardianAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                promise.setTagTeacherAudio(new Audio(urlPath, audio_length));
            }
        }else if(promise_type == FilePathUtil.promise_type_resource){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                promise.setResourceChildAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                promise.setResourceGuardianAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                promise.setResourceTeacherAudio(new Audio(urlPath, audio_length));
            }
        }else if(promise_type == FilePathUtil.promise_type_grounded){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                promise.setGroundedChildAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                promise.setGroundedGuardianAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                promise.setGroundedTeacherAudio(new Audio(urlPath, audio_length));
            }
        }else if(promise_type == FilePathUtil.promise_type_criticize){
            if(promise_person_type == FilePathUtil.promise_person_type_child){
                promise.setCriticizeChildAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_guardian){
                promise.setCriticizeGuardianAudio(new Audio(urlPath, audio_length));
            }else if(promise_person_type == FilePathUtil.promise_person_type_teacher){
                promise.setCriticizeTeacherAudio(new Audio(urlPath, audio_length));
            }
        }else if(promise_type == FilePathUtil.promise_type_family_meeting){
            if(promise.getFamilyMeetingAudios()==null){
                List<Audio> familyMeetingAudios = new ArrayList<Audio>();
                promise.setFamilyMeetingAudioIndex(1);
                familyMeetingAudios.add(new Audio(1,urlPath, audio_length, position_x, position_y));
                promise.setFamilyMeetingAudios(familyMeetingAudios);
            }else{

                    boolean isExitPositionX = false;
                    for (Audio audio : promise.getFamilyMeetingAudios()) {
                        if (audio.getPositionX() == position_x && audio.getPositionY() == position_y) {
                            audio.setAudioLength(audio_length);
                            audio.setAudioURLString(urlPath);
                            isExitPositionX = true;
                            break;
                        }
                    }
                    if(!isExitPositionX){
                        promise.setFamilyMeetingAudioIndex(promise.getFamilyMeetingAudioIndex()+1);
                        promise.getFamilyMeetingAudios().add(new Audio(promise.getFamilyMeetingAudioIndex(),urlPath, audio_length, position_x, position_y));
                    }
                //promise.getFamilyMeetingAudios().add(new Audio(urlPath, audio_length, position_x, position_y));
            }
        }
        updatePromiseStatus(promise);
        promise = promiseService.save(promise);
        return promise;
    }

    public Promise updatePromiseStatus(Promise promise){

        if(promise.getPromiseAudio()!=null&&promise.getPromiseGuardianAudio()!=null&&promise.getPromiseTeacherAudio()!=null){
            promise.setPromiseStatus(DomainUtil.promiseStatus_finished);
        }else if(promise.getPromiseAudio()==null && promise.getPromiseGuardianAudio()==null && promise.getPromiseTeacherAudio()==null){
            promise.setPromiseStartTime(null);
            promise.setPromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else{
            if(promise.getPromiseStartTime()==null){
                promise.setPromiseStartTime(new Date());
            }
            promise.setPromiseStatus(DomainUtil.promiseStatus_started);
        }

        if(promise.getPersonalityChildAudio()!=null&&promise.getPersonalityGuardianAudio()!=null&&promise.getPersonalityTeacherAudio()!=null){
            promise.setPersonalityPromiseStatus(DomainUtil.promiseStatus_finished);
        }else if(promise.getPersonalityChildAudio()==null && promise.getPersonalityGuardianAudio()==null && promise.getPersonalityTeacherAudio()==null){
            promise.setPersonalityStartTime(null);
            promise.setPersonalityPromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else{
            if(promise.getPersonalityStartTime()==null){
                promise.setPersonalityStartTime(new Date());
            }
            promise.setPersonalityPromiseStatus(DomainUtil.promiseStatus_started);
        }

        if(promise.getTagChildAudio()!=null&&promise.getTagGuardianAudio()!=null&&promise.getTagTeacherAudio()!=null){
            promise.setTagPromiseStatus(DomainUtil.promiseStatus_finished);
        }else if(promise.getTagChildAudio()==null && promise.getTagGuardianAudio()==null && promise.getTagTeacherAudio()==null){
            promise.setTagStartTime(null);
            promise.setTagPromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else{
            if(promise.getTagStartTime()==null){
                promise.setTagStartTime(new Date());
            }
            promise.setTagPromiseStatus(DomainUtil.promiseStatus_started);
        }

        if(promise.getResourceChildAudio()!=null&&promise.getResourceGuardianAudio()!=null&&promise.getResourceTeacherAudio()!=null){
            promise.setResourcePromiseStatus(DomainUtil.promiseStatus_finished);
        }else if(promise.getResourceChildAudio()==null && promise.getResourceGuardianAudio()==null && promise.getResourceTeacherAudio()==null){
            promise.setResourceStartTime(null);
            promise.setResourcePromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else{
            if(promise.getResourceStartTime()==null){
                promise.setResourceStartTime(new Date());
            }
            promise.setResourcePromiseStatus(DomainUtil.promiseStatus_started);
        }

        if(promise.getGroundedChildAudio()!=null&&promise.getGroundedGuardianAudio()!=null&&promise.getGroundedTeacherAudio()!=null){
            promise.setGroundedPromiseStatus(DomainUtil.promiseStatus_finished);
        }else if(promise.getGroundedChildAudio()==null && promise.getGroundedGuardianAudio()==null && promise.getGroundedTeacherAudio()==null){
            promise.setGroundedStartTime(null);
            promise.setGroundedPromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else{
            if(promise.getGroundedStartTime()==null){
                promise.setGroundedStartTime(new Date());
            }
            promise.setGroundedPromiseStatus(DomainUtil.promiseStatus_started);
        }

        if(promise.getCriticizeChildAudio()!=null&&promise.getCriticizeGuardianAudio()!=null&&promise.getCriticizeTeacherAudio()!=null){
            promise.setCriticizePromiseStatus(DomainUtil.promiseStatus_finished);
        }else if(promise.getCriticizeChildAudio()==null && promise.getCriticizeGuardianAudio()==null && promise.getCriticizeTeacherAudio()==null){
            promise.setCriticizeStartTime(null);
            promise.setCriticizePromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else{
            if(promise.getCriticizeStartTime()==null){
                promise.setCriticizeStartTime(new Date());
            }
            promise.setCriticizePromiseStatus(DomainUtil.promiseStatus_started);
        }
        if(promise.getFamilyMeetingAudios()== null || promise.getFamilyMeetingAudios().isEmpty()){
            promise.setFamilyMeetingPromiseStatus(DomainUtil.promiseStatus_notStarted);
        }else if(promise.getFamilyMeetingAudios().size() >= 12){
            promise.setFamilyMeetingStartTime(null);
            promise.setFamilyMeetingPromiseStatus(DomainUtil.promiseStatus_finished);
        }else{
            if(promise.getFamilyMeetingStartTime()==null){
                promise.setFamilyMeetingStartTime(new Date());
            }
            promise.setFamilyMeetingPromiseStatus(DomainUtil.promiseStatus_started);
        }
        return promise;
    }
}
