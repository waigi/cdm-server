package com.vigo.cdm.web.rest.dto;

/**
 * Created by weill on 17/1/13.
 */
public class ExpiryDateDTO {

    private int days;

    public ExpiryDateDTO(){

    }

    public ExpiryDateDTO(int days){
        this.days = days;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
