package com.vigo.cdm.web.rest.util;

/**
 * Created by weill on 16/8/8.
 */
public class FilePathUtil {

    //1:N, 2:J
    public static final int task_type_n= 1;
    public static final int task_type_j= 2;

    //(1:Photo, 2:Audio, 3:Video)
    public static final int file_type_photo= 1;
    public static final int file_type_Audio= 2;
    public static final int file_type_Video= 3;


    public static final String pathConstant= "family/task/";
    public static final String pathForwardSlash= "/";
    public static final String pathNOfTask= "/n";
    public static final String pathJOfTask= "/j";
    public static final String pathPhoto= "/photo";
    public static final String pathAudio= "/audio";
    public static final String pathVideo= "/video";

    public static final int family_activity_type_pre_audio = 0;
    public static final int family_activity_type_photo = 1;
    public static final int family_activity_type_audio = 2;
    public static final int family_activity_type_video = 3;
    public static final int family_activity_type_text = 4;


    //1:Personality, 2:Tag, 3:Resource, 4:Grounded, 5:Criticize, 6:FamilyMeeting
    public static final int promise_type_promise = 0;
    public static final int promise_type_personality= 1;
    public static final int promise_type_tag= 2;
    public static final int promise_type_resource= 3;
    public static final int promise_type_grounded= 4;
    public static final int promise_type_criticize= 5;
    public static final int promise_type_family_meeting= 6;

    //1:child, 2:guardian
    public static final int promise_person_type_other= 0;
    public static final int promise_person_type_child= 1;
    public static final int promise_person_type_guardian= 2;
    public static final int promise_person_type_teacher= 3;

    public static final String pathOfUserPhoto= "family/photo";

    public static final String pathConstantOfPromise= "family/promise/";
    public static final String pathConstantOfFamilyActivity= "family/activity/";

    public static final String pathOfFamilyActivityPreAudio= "/preaudio";
    public static final String pathOfFamilyActivityPhoto= "/photo";
    public static final String pathOfFamilyActivityAudio= "/audio";
    public static final String pathOfFamilyActivityVideo= "/video";


    public static final String pathOfPromiseAudio= "/promise/audio/child";
    public static final String pathOfPromiseGuardianAudio= "/promise/audio/guardian";
    public static final String pathOfPromiseTeacherAudio= "/promise/audio/teacher";

    public static final String pathOfPersonalityChildPromiseAudio= "/personality/audio/child";
    public static final String pathOfPersonalityGuardianPromiseAudio= "/personality/audio/guardian";
    public static final String pathOfPersonalityTeacherPromiseAudio= "/personality/audio/teacher";

    public static final String pathOfTagChildPromiseAudio= "/tag/audio/child";
    public static final String pathOfTagGuardianPromiseAudio= "/tag/audio/guardian";
    public static final String pathOfTagTeacherPromiseAudio= "/tag/audio/teacher";

    public static final String pathOfResourceChildPromiseAudio= "/resource/audio/child";
    public static final String pathOfResourceGuardianPromiseAudio= "/resource/audio/guardian";
    public static final String pathOfResourceTeacherPromiseAudio= "/resource/audio/teacher";

    public static final String pathOfGroundedChildPromiseAudio= "/grounded/audio/child";
    public static final String pathOfGroundedGuardianPromiseAudio= "/grounded/audio/guardian";
    public static final String pathOfGroundedTeacherPromiseAudio= "/grounded/audio/teacher";

    public static final String pathOfCriticizeChildPromiseAudio= "/criticize/audio/child";
    public static final String pathOfCriticizeGuardianPromiseAudio= "/criticize/audio/guardian";
    public static final String pathOfCriticizeTeacherPromiseAudio= "/criticize/audio/teacher";

    public static final String pathOfFamilyMeetingAudio= "/familymeeting/audio";

}
