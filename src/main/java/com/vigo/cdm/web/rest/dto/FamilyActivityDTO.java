package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.*;
import com.vigo.cdm.domain.util.DomainUtil;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A Promise.
 */

public class FamilyActivityDTO {

    private String id;

    private AccountFamily accountFamily;

    private String name;
    //Task Id
    private Task task;

    private Long familyActivityTime;

    private int sequenceNumber;

    private List<Audio> preActivityAudios;

    private List<Audio> audios;

    private List<Video> videos;

    private List<Text> texts;

    private List<Photo> photos;


    public FamilyActivityDTO(){

    }

    public FamilyActivityDTO(FamilyActivity familyActivity){
        this.id = familyActivity.getId();
        this.accountFamily = familyActivity.getAccountFamily();
        this.task = familyActivity.getTask();
        this.name = familyActivity.getName();
        this.sequenceNumber = familyActivity.getSequenceNumber();
        this.preActivityAudios = familyActivity.getPreActivityAudios();
        this.audios = familyActivity.getAudios();
        this.videos = familyActivity.getVideos();
        this.texts = familyActivity.getTexts();
        this.photos = familyActivity.getPhotos();

        if (familyActivity.getFamilyActivityTime() != null) {
            this.familyActivityTime = familyActivity.getFamilyActivityTime().getTime();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyActivityDTO promise = (FamilyActivityDTO) o;
        if(promise.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, promise.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Promise{" +
            "id=" + id +
            '}';
    }


    public AccountFamily getAccountFamily() {
        return accountFamily;
    }

    public void setAccountFamily(AccountFamily accountFamily) {
        this.accountFamily = accountFamily;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getFamilyActivityTime() {
        return familyActivityTime;
    }

    public void setFamilyActivityTime(Long familyActivityTime) {
        this.familyActivityTime = familyActivityTime;
    }

    public List<Audio> getPreActivityAudios() {
        return preActivityAudios;
    }

    public void setPreActivityAudios(List<Audio> preActivityAudios) {
        this.preActivityAudios = preActivityAudios;
    }

    public List<Audio> getAudios() {
        return audios;
    }

    public void setAudios(List<Audio> audios) {
        this.audios = audios;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }


    public List<Text> getTexts() {
        return texts;
    }

    public void setTexts(List<Text> texts) {
        this.texts = texts;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
