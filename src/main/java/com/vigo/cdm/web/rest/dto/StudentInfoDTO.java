package com.vigo.cdm.web.rest.dto;


import com.vigo.cdm.config.Constants;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * A DTO representing a Student, with his authorities.
 */
public class StudentInfoDTO {

    private String name;
    private String accountFamilyId;
    private String accountFamilyName;
    private String photoURL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountFamilyName() {
        return accountFamilyName;
    }

    public void setAccountFamilyName(String accountFamilyName) {
        this.accountFamilyName = accountFamilyName;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getAccountFamilyId() {
        return accountFamilyId;
    }

    public void setAccountFamilyId(String accountFamilyId) {
        this.accountFamilyId = accountFamilyId;
    }
}
