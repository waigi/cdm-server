package com.vigo.cdm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.vigo.cdm.domain.*;
import com.vigo.cdm.service.AccountFamilyService;
import com.vigo.cdm.service.FamilyActivityService;
import com.vigo.cdm.web.rest.dto.FamilyActivityDTO;
import com.vigo.cdm.web.rest.util.FilePathUtil;
import com.vigo.cdm.web.rest.util.HeaderUtil;
import com.vigo.cdm.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing FamilyActivity.
 */
@RestController
@RequestMapping("/api")
public class FamilyActivityResource {

    private final Logger log = LoggerFactory.getLogger(FamilyActivityResource.class);

    @Inject
    private FamilyActivityService familyActivityService;

    @Inject
    private AccountFamilyService accountFamilyService;
    /**
     * POST  /family-activities : Create a new familyActivity.
     *
     * @param accountFamilyId and taskId the familyActivity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new familyActivity, or with status 400 (Bad Request) if the familyActivity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/family-activities",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> createFamilyActivity(@RequestParam(value = "accountFamilyId", required = true) String accountFamilyId,@RequestParam(value = "taskId", required = false) String taskId) throws URISyntaxException {
        log.debug("REST request to save Promise for accountFamily id: {}", accountFamilyId);
        AccountFamily accountFamily = accountFamilyService.findOne(accountFamilyId);
        if (accountFamilyId == null || accountFamily == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("promise", "accountFamilyIdNull", "A new promise cannot have not an family account ID")).body(null);
        }
        FamilyActivity result = new FamilyActivity();

        accountFamily.setFamilyActivitySequenceNumber(accountFamily.getFamilyActivitySequenceNumber() + 1);
        accountFamilyService.save(accountFamily);
        result.setSequenceNumber(accountFamily.getFamilyActivitySequenceNumber());
        result.setAccountFamily(new AccountFamily(accountFamilyId));
        result.setFamilyActivityTime(new Date());
        if(taskId!=null){
            result.setTask(new Task(taskId));
        }
        result = familyActivityService.save(result);
        return ResponseEntity.created(new URI("/api/familyActivitys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("familyActivitys", result.getId().toString()))
            .body(new FamilyActivityDTO(result));
    }

    /**
     * PUT  /family-activities : Updates an existing familyActivity.
     *
     * @param familyActivity the familyActivity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated familyActivity,
     * or with status 400 (Bad Request) if the familyActivity is not valid,
     * or with status 500 (Internal Server Error) if the familyActivity couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
   /* @RequestMapping(value = "/family-activities",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivity> updateFamilyActivity(@RequestBody FamilyActivity familyActivity) throws URISyntaxException {
        log.debug("REST request to update FamilyActivity : {}", familyActivity);
        if (familyActivity.getId() == null) {
            return createFamilyActivity(familyActivity);
        }
        FamilyActivity result = familyActivityService.save(familyActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("familyActivity", familyActivity.getId().toString()))
            .body(result);
    }*/

    /**
     * PUT  /familyActivity : Updates an existing familyActivity. When the field of familyActivity is empty, it does not cover
     *
     * @param familyActivity the familyActivity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/family-activities/update",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> updateFamilyActivity(@RequestBody FamilyActivity familyActivity) throws URISyntaxException {
        log.debug("REST request to update Promise : {}", familyActivity);
        if (familyActivity.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        FamilyActivity result = familyActivityService.findOne(familyActivity.getId());
        result = onlyUpdateFamilyActivity(result, familyActivity);
        result = familyActivityService.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("familyActivity", familyActivity.getId().toString()))
            .body(new FamilyActivityDTO(result));
    }

    /**
     * PUT  /tasks : Updates an existing promise. All fields Cover!
     *
     * @param familyActivity the familyActivity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/family-activities/updateAndCover",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> updateAndCoverTask(@RequestBody FamilyActivity familyActivity) throws URISyntaxException {
        log.debug("REST request to update familyActivity : {}", familyActivity);
        if (familyActivity.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        FamilyActivity result = familyActivityService.save(familyActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("familyActivity", familyActivity.getId().toString()))
            .body(new FamilyActivityDTO(result));
    }

    /**
     * POST  /family-activities/uploadText : upload the text for family-activitie.
     *
     * @param family_activity_id is the task type(1:child, 2:guardian)
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     *
     */
    @RequestMapping(value = "/family-activities/newText",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> updateFamilyActivityText(@RequestParam(value="family_activity_id",required=true) String family_activity_id,
                                                                      @RequestParam(value="family_activity_text",required=true) String family_activity_text,
                                                                      @RequestParam(value="position_x",required=true) Integer position_x,
                                                                      @RequestParam(value="position_y",required=true) Integer position_y) throws URISyntaxException{
        log.debug("REST request to update FamilyActivity : {}", family_activity_id);
        FamilyActivity familyActivity = familyActivityService.findOne(family_activity_id);
        if(familyActivity == null){
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        if(familyActivity.getTexts() == null){
            List<Text> texts = new ArrayList<Text>();
            familyActivity.setTexts(texts);
        }
        familyActivity.setTextIndex(familyActivity.getTextIndex()+1);
        familyActivity.getTexts().add(new Text(familyActivity.getTextIndex(), family_activity_text, position_x, position_y));
        familyActivity = familyActivityService.save(familyActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("familyActivity", family_activity_id))
            .body(new FamilyActivityDTO(familyActivity));
    }

    /*@RequestMapping(value = "/family-activities/uploadText",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> updateFamilyActivityText(@RequestParam(value = "family_activity_id", required = true) String family_activity_id,
                                                                  @RequestParam(value="family_activity_text",required = true) String family_activity_text,
                                                                      @RequestParam(value = "position_x", required = true) Integer position_x,
                                                                      @RequestParam(value = "position_y", required = true) Integer position_y
     ) {
        FamilyActivity familyActivity = familyActivityService.findOne(family_activity_id);
        if(familyActivity == null){
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        if(familyActivity.getTexts() == null){
            List<Text> texts = new ArrayList<Text>();
            familyActivity.setTexts(texts);
        }
        familyActivity.setTextIndex(familyActivity.getTextIndex()+1);
        familyActivity.getTexts().add(new Text(familyActivity.getTextIndex(), family_activity_text, position_x, position_y));
        familyActivity = familyActivityService.save(familyActivity);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("familyActivity", familyActivity.getId().toString()))
                .body(new FamilyActivityDTO(familyActivity));
    }*/

    /**
     * POST  /tasks/upload : upload the file for task.
     *
     * @param family_activity_type is the task type(1:child, 2:guardian)
     * @param family_activity_id is the task id(String)
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     *
     */
    @RequestMapping(value = "/family-activities/updatePosition",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> updateFileFamilyActivityPosition(@RequestParam(value = "family_activity_type", required = true) Integer family_activity_type,
                                                                              @RequestParam(value = "family_activity_id", required = true) String family_activity_id,
                                                                              @RequestParam(value="id",required = true) int id,
                                                                              @RequestParam(value = "position_x", required = true) Integer position_x,
                                                                              @RequestParam(value = "position_y", required = true) Integer position_y
    ) {
        FamilyActivity familyActivity = familyActivityService.findOne(family_activity_id);
        if(familyActivity==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(family_activity_type == FilePathUtil.family_activity_type_pre_audio){
            familyActivity.setPreActivityAudios(updateAudioPosition(familyActivity.getPreActivityAudios(), id, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_photo){
            familyActivity.setPhotos(updatePhotoPosition(familyActivity.getPhotos(), id, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_audio){
            familyActivity.setAudios(updateAudioPosition(familyActivity.getAudios(), id, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_video){
            familyActivity.setVideos(updateVideoPosition(familyActivity.getVideos(), id, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_text){
            familyActivity.setTexts(updateTextPosition(familyActivity.getTexts(), id, position_x, position_y));
        }
        familyActivity = familyActivityService.save(familyActivity);
        return new ResponseEntity<>(new FamilyActivityDTO(familyActivity), HttpStatus.OK);
    }

    /**
     * POST  /tasks/upload : upload the file for task.
     *
     * @param family_activity_type is the task type(0:promise oneself, 1:Personality, 2:Tag, 3:Resource, 4:Grounded, 5:Criticize, 6:FamilyMeeting)
     * @param family_activity_id is the task type(1:child, 2:guardian)
     * @return the ResponseEntity with status 200 (OK) and with body the updated task,
     * or with status 400 (Bad Request) if the task is not valid,
     * or with status 500 (Internal Server Error) if the task couldnt be updated
     *
     */
    @RequestMapping(value = "/family-activities/upload",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> uploadFileFamilyActivity(@RequestParam(value = "family_activity_type", required = true) int family_activity_type,
                                                        @RequestParam(value = "family_activity_id", required = true) String family_activity_id,
                                                        @RequestParam(value="file",required = true) MultipartFile file,
                                                        @RequestParam(value = "audio_length", required = false) Integer audio_length,
                                                        @RequestParam(value = "position_x", required = false, defaultValue = "0") Integer position_x,
                                                        @RequestParam(value = "position_y", required = false, defaultValue = "0") Integer position_y,
                                                        HttpServletRequest request
    ) {
        FamilyActivity familyActivity = familyActivityService.findOne(family_activity_id);
        String basePath =request.getSession().getServletContext().getRealPath("/");  //根路径
        String baseUrl = request.getScheme() + // "http"
            "://" +                                // "://"
            request.getServerName() +              // "myhost"
            ":" +                                  // ":"
            request.getServerPort() +              // "80"
            request.getContextPath();               // "/myContextPath" or "" if deployed in root context
        if (!file.isEmpty()) {
            //文件夹相对路径
            String folderRelativePath = uploadFileFamilyActivityRelativePathURL(family_activity_type, familyActivity.getAccountFamily().getId(), family_activity_id);
            String folderAbsolutePath = basePath + folderRelativePath;
            UUID uuid = UUID.randomUUID();
            String fileName = uuid.toString() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            String imageName = uuid +".jpg";

            //String imageBaseRelativePath = "/content/images/logo-jhipster.png";
            String imageRelativePath = folderRelativePath + FilePathUtil.pathForwardSlash+ imageName;
            String imageAbsolutePath = basePath + imageRelativePath;

            //文件相对路径
            String fileRelativePath = folderRelativePath + FilePathUtil.pathForwardSlash+ fileName;
            //文件绝对路径
            String fileAbsolutePath = basePath + fileRelativePath;

            if(saveFileFromInputStream(file, basePath, folderRelativePath, fileRelativePath)){
                //filePath = baseUrl + "/" + filePath;
                //List<String> fileURLs = new ArrayList<String>();
                //fileURLs.add(filePath);
                familyActivity = updateFamilyActivity(family_activity_type, familyActivity, baseUrl, fileRelativePath, fileAbsolutePath ,imageRelativePath, imageAbsolutePath, audio_length, position_x, position_y);
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("familyActivity", familyActivity.getId().toString()))
                    .body(new FamilyActivityDTO(familyActivity));
            }else{
                new ResponseEntity<>(new FamilyActivityDTO(familyActivity),HttpStatus.NOT_MODIFIED);
            }
        }
        //promise = updatePromiseStatus(promise);
        return new ResponseEntity<>(new FamilyActivityDTO(familyActivity),HttpStatus.NOT_MODIFIED);
    }

    /**
     * GET  /tasks : get all the tasks of the family account.
     *
     * @param accountFamilyId
     * @return the ResponseEntity with status 200 (OK) and the list of tasks in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/family-activities/familyActivities",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<FamilyActivityDTO>> getFamilyActivities(@RequestParam(value = "accountFamily_id", required = true) String accountFamilyId) {
        log.debug("REST request to get a page of familyActivities");
        if(accountFamilyId == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<FamilyActivity> familyActivities = familyActivityService.findFamilyActivitiesByAccountFamilyIdOrderByFamilyActivityTimeDesc(accountFamilyId);
        List<FamilyActivityDTO> familyActivityDTOs = familyActivities.stream()
            .map(FamilyActivityDTO::new)
            .collect(Collectors.toList());
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tasks");
        return new ResponseEntity<>(familyActivityDTOs, HttpStatus.OK);
    }

    /**
     * GET  /family-activities : get all the familyActivities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of familyActivities in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/family-activities",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<FamilyActivity>> getAllFamilyActivities(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of FamilyActivities");
        Page<FamilyActivity> page = familyActivityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/family-activities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /family-activities/:id : get the "id" familyActivity.
     *
     * @param id the id of the familyActivity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the familyActivity, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/family-activities/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> getFamilyActivity(@PathVariable String id) {
        log.debug("REST request to get FamilyActivity : {}", id);
        FamilyActivity familyActivity = familyActivityService.findOne(id);
        return Optional.ofNullable(familyActivity)
            .map(result -> new ResponseEntity<>(
                new FamilyActivityDTO(result),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(new FamilyActivityDTO(),HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /family-activities/:taskId : get the "taskId" familyActivity.
     *
     * @param taskId the id of the familyActivity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the familyActivity, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/family-activities/byTask",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> getFamilyActivityByTaskId(@RequestParam(value = "task_id", required = true) String taskId) {
        log.debug("REST request to get FamilyActivity : {}", taskId);
        FamilyActivity familyActivity = familyActivityService.findOneByTaskId(taskId);
        return Optional.ofNullable(familyActivity)
            .map(result -> new ResponseEntity<>(
                new FamilyActivityDTO(result),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(new FamilyActivityDTO(), HttpStatus.OK));
    }


    /**
     * DELETE  /family-activities/:id : delete the "id" familyActivity.
     *
     * @param id the id of the familyActivity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/family-activities/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFamilyActivity(@PathVariable String id) {
        log.debug("REST request to delete FamilyActivity : {}", id);
        familyActivityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("familyActivity", id.toString())).build();
    }

    /**
     * DELETE  /family-activities/:id : delete the "id" familyActivity.
     *
     * @param family_activity_type the id of the familyActivity to delete
     * @param family_activity_id the id of the familyActivity to delete
     * @param family_activity_type the id of the familyActivity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/family-activities/deleteAPVT",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FamilyActivityDTO> deleteFamilyActivity(@RequestParam(value = "family_activity_type", required = true) Integer family_activity_type,
                                                     @RequestParam(value = "family_activity_id", required = true) String family_activity_id,
                                                     @RequestParam(value="id",required = true) int id) {
        FamilyActivity familyActivity = familyActivityService.findOne(family_activity_id);
        if(familyActivity==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(family_activity_type == FilePathUtil.family_activity_type_pre_audio){
            familyActivity.setPreActivityAudios(deleteAudio(familyActivity.getPreActivityAudios(), id));
        }else if(family_activity_type == FilePathUtil.family_activity_type_photo){
            familyActivity.setPhotos(deletePhoto(familyActivity.getPhotos(), id));
        }else if(family_activity_type == FilePathUtil.family_activity_type_audio){
            familyActivity.setAudios(deleteAudio(familyActivity.getAudios(), id));
        }else if(family_activity_type == FilePathUtil.family_activity_type_video){
            familyActivity.setVideos(deleteVideo(familyActivity.getVideos(), id));
        }else if(family_activity_type == FilePathUtil.family_activity_type_text){
            familyActivity.setTexts(deleteText(familyActivity.getTexts(), id));
        }
        familyActivity = familyActivityService.save(familyActivity);
        return new ResponseEntity<>(new FamilyActivityDTO(familyActivity), HttpStatus.OK);
    }

    public List<Audio> deleteAudio(List<Audio> audios, int id){
        List<Audio> audioChange = null;
        if(audios!=null){
            audioChange = new ArrayList<Audio>();
            for(Audio audio : audios){
                if(audio.getId() != id){
                    audioChange.add(audio);
                }
            }
        }
        return audioChange;
    }

    public List<Photo> deletePhoto(List<Photo> photos, int id){
        List<Photo> photoChange = null;
        if(photos!=null){
            photoChange = new ArrayList<Photo>();
            for(Photo photo : photos){
                if(photo.getId() != id){
                    photoChange.add(photo);
                }
            }
        }
        return photoChange;
    }

    public List<Video> deleteVideo(List<Video> videos, int id){
        List<Video> videoChange = null;
        if(videos!=null){
            videoChange = new ArrayList<Video>();
            for(Video video : videos){
                if(video.getId() != id){
                    videoChange.add(video);
                }
            }
        }
        return videoChange;
    }

    public List<Text> deleteText(List<Text> texts, int id){
        List<Text> textChange = null;
        if(texts!=null){
            textChange = new ArrayList<Text>();
            for(Text text : texts){
                if(text.getId() != id){
                    textChange.add(text);
                }
            }
        }
        return textChange;
    }

    public List<Audio> updateAudioPosition(List<Audio> audios, int id, int positionX, int positionY){
        if(audios!=null){
            for(Audio audio : audios){
                if(audio.getId() == id){
                    audio.setPositionX(positionX);
                    audio.setPositionY(positionY);
                }
            }
        }
        return audios;
    }

    public List<Photo> updatePhotoPosition(List<Photo> photos, int id, int positionX, int positionY){
        if(photos!=null){
            for(Photo photo : photos){
                if(photo.getId() == id){
                    photo.setPositionX(positionX);
                    photo.setPositionY(positionY);
                }
            }
        }
        return photos;
    }

    public List<Video> updateVideoPosition(List<Video> videos, int id, int positionX, int positionY){
        if(videos!=null){
            for(Video video : videos){
                if(video.getId() == id){
                    video.setPositionX(positionX);
                    video.setPositionY(positionY);
                }
            }
        }
        return videos;
    }

    public List<Text> updateTextPosition(List<Text> texts, int id, int positionX, int positionY){
        if(texts!=null){
            for(Text text : texts){
                if(text.getId() == id){
                    text.setPositionX(positionX);
                    text.setPositionY(positionY);
                }
            }
        }
        return texts;
    }

    public FamilyActivity onlyUpdateFamilyActivity(FamilyActivity result, FamilyActivity familyActivityParam){

        //AccountFamily Id
        if(familyActivityParam.getAccountFamily()!=null){
            result.setAccountFamily(familyActivityParam.getAccountFamily());
        }
        if(familyActivityParam.getFamilyActivityTime()!=null){
            result.setFamilyActivityTime(familyActivityParam.getFamilyActivityTime());
        }
        if(familyActivityParam.getName()!=null){
            result.setName(familyActivityParam.getName());
        }
        if(familyActivityParam.getAudios()!=null){
            result.setAudios(familyActivityParam.getAudios());
        }
        if(familyActivityParam.getPhotos()!=null){
            result.setPhotos(familyActivityParam.getPhotos());
        }
        if(familyActivityParam.getPreActivityAudios()!=null){
            result.setPreActivityAudios(familyActivityParam.getPreActivityAudios());
        }
        if(familyActivityParam.getTask()!=null){
            result.setTask(familyActivityParam.getTask());
        }
        if(familyActivityParam.getTexts()!=null){
            result.setTexts(familyActivityParam.getTexts());
        }
        if(familyActivityParam.getVideos()!=null){
            result.setVideos(familyActivityParam.getVideos());
        }
        if(familyActivityParam.getSequenceNumber()!=0){
            result.setSequenceNumber(familyActivityParam.getSequenceNumber());
        }
        if(familyActivityParam.getPreActivityAudioIndex()!=0){
            result.setPreActivityAudioIndex(familyActivityParam.getPreActivityAudioIndex());
        }
        if(familyActivityParam.getAudioIndex()!=0){
            result.setAudioIndex(familyActivityParam.getAudioIndex());
        }
        if(familyActivityParam.getVideoIndex()!=0){
            result.setVideoIndex(familyActivityParam.getVideoIndex());
        }
        if(familyActivityParam.getTextIndex()!=0){
            result.setTextIndex(familyActivityParam.getTextIndex());
        }
        if(familyActivityParam.getPhotoIndex()!=0){
            result.setPhotoIndex(familyActivityParam.getPhotoIndex());
        }
        return result;
    }

    //文件夹相对路径
    public String uploadFileFamilyActivityRelativePathURL(int family_activity_type, String accountFamilyId, String familyActivityId){
        String filePath = FilePathUtil.pathConstantOfFamilyActivity+accountFamilyId+FilePathUtil.pathForwardSlash+familyActivityId;
        if(family_activity_type == FilePathUtil.family_activity_type_pre_audio){
            filePath = filePath  + FilePathUtil.pathOfFamilyActivityPreAudio;
        }else if(family_activity_type == FilePathUtil.family_activity_type_photo){
            filePath = filePath  + FilePathUtil.pathOfFamilyActivityPhoto;
        }else if(family_activity_type == FilePathUtil.family_activity_type_audio){
            filePath = filePath  + FilePathUtil.pathOfFamilyActivityAudio;
        }else if(family_activity_type == FilePathUtil.family_activity_type_video){
            filePath = filePath  + FilePathUtil.pathOfFamilyActivityVideo;
        }
        return filePath;
    }

    public Boolean saveFileFromInputStream(MultipartFile file,String basePath, String folderPath, String filePath) {
        Boolean result = false;
        if (file==null || file.isEmpty()) {
            return result;
        }
        File fileUpload = new File(basePath + folderPath);
        if(!fileUpload.exists()){
            if(!fileUpload.mkdirs()){
                return result;
            }
        }
        try {
            InputStream stream = file.getInputStream();
            FileOutputStream fs = new FileOutputStream(basePath + filePath);
            byte[] buffer = new byte[1024 * 1024];
            int bytesum = 0;
            int byteread = 0;
            while ((byteread = stream.read(buffer)) != -1) {
                bytesum += byteread;
                fs.write(buffer, 0, byteread);
                fs.flush();
            }
            fs.close();
            stream.close();
            result = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public FamilyActivity updateFamilyActivity(int family_activity_type, FamilyActivity familyActivity, String baseUrl, String fileRelativePath, String fileAbsolutePath
                                                ,String imageRelativePath, String imageAbsolutePath, Integer audio_length, int position_x, int position_y){
        String urlPath = baseUrl + "/" + fileRelativePath;
        if(family_activity_type == FilePathUtil.family_activity_type_pre_audio){
            if(familyActivity.getPreActivityAudios() == null){
                List<Audio> audios = new ArrayList<Audio>();
                familyActivity.setPreActivityAudios(audios);
            }
            familyActivity.setPreActivityAudioIndex(familyActivity.getPreActivityAudioIndex()+1);
            familyActivity.getPreActivityAudios().add(new Audio(familyActivity.getPreActivityAudioIndex(),urlPath, audio_length, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_photo){
            if(familyActivity.getPhotos() == null){
                List<Photo> photos = new ArrayList<Photo>();
                //photos.add(new Photo(0, urlPath, position_x, position_y));
                familyActivity.setPhotos(photos);
            }
            familyActivity.setPhotoIndex(familyActivity.getPhotoIndex()+1);
            familyActivity.getPhotos().add(new Photo(familyActivity.getPhotoIndex(), urlPath, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_audio){
            if(familyActivity.getAudios() == null){
                List<Audio> audios = new ArrayList<Audio>();
                //audios.add(new Audio(0,urlPath, audio_length, position_x, position_y));
                familyActivity.setAudios(audios);
            }
            familyActivity.setAudioIndex(familyActivity.getAudioIndex() + 1);
            familyActivity.getAudios().add(new Audio(familyActivity.getAudioIndex(),  urlPath, audio_length, position_x, position_y));
        }else if(family_activity_type == FilePathUtil.family_activity_type_video){
            if(familyActivity.getVideos() == null){
                List<Video> videos = new ArrayList<Video>();
                familyActivity.setVideos(videos);
            }
            familyActivity.setVideoIndex(familyActivity.getVideoIndex()+1);
            if(saveVideoThumbnail(fileAbsolutePath, imageAbsolutePath)){
                familyActivity.getVideos().add(new Video(familyActivity.getAudioIndex(),urlPath, baseUrl + "/"+ imageRelativePath,position_x,position_y));
                //task.getnVideoThumbnailURLStrings().add(baseUrl + "/"+ imageRelativePath);
            }else{
                familyActivity.getVideos().add(new Video(familyActivity.getAudioIndex(),urlPath, baseUrl + "/content/images/hipster.png",position_x,position_y));
            }
            //familyActivity.getVideos().add(new Video(urlPath));
        }
        familyActivity = familyActivityService.save(familyActivity);
        return familyActivity;
    }

    public Boolean saveVideoThumbnail(String videoPath,String imagePath) {
        //String videoPath = "/Users/will/Movies/W.mov";
        //String imagePath = folderRelativePath + +".jpg";
        //ffmpeg -i xxx.mp4 -y -f image2 -t 0.001 -s 125x125 xxx.jpg
        List<String> cmd = new java.util.ArrayList<String>();
        cmd.add("ffmpeg");// 视频提取工具的位置
        cmd.add("-i");
        cmd.add(videoPath);
        cmd.add("-y");
        cmd.add("-f");
        cmd.add("image2");
        cmd.add("-t");
        cmd.add("0.001");
        cmd.add("-s");
        cmd.add("137x137");
        cmd.add(imagePath);
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(cmd);
        try {
            builder.start();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
