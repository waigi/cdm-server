package com.vigo.cdm.web.rest.dto;

import com.vigo.cdm.domain.Credit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by weill on 16/12/17.
 */
public class CreditsInWeek {

    private Long startDate;
    private Long endDate;
    private List<CreditDTO> credits = new ArrayList<CreditDTO>(0);

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public List<CreditDTO> getCredits() {
        return credits;
    }

    public void setCredits(List<CreditDTO> credits) {
        this.credits = credits;
    }
}
