package com.vigo.cdm.service;

import com.vigo.cdm.domain.Guardian;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Guardian.
 */
public interface GuardianService {

    /**
     * Save a guardian.
     *
     * @param guardian the entity to save
     * @return the persisted entity
     */
    Guardian save(Guardian guardian);

    /**
     *  Get all the guardians.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Guardian> findAll(Pageable pageable);

    /**
     *  Get the "id" guardian.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Guardian findOne(String id);

    /**
     *  Delete the "id" guardian.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    List<Guardian> findGuardiansByAccountFamilyId(String accountFamilyId);
}
