package com.vigo.cdm.service;

import com.vigo.cdm.domain.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Service Interface for managing Task.
 */
public interface TaskService {

    /**
     * Save a task.
     *
     * @param task the entity to save
     * @return the persisted entity
     */
    Task save(Task task);

    /**
     *  Get all the tasks.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Task> findAll(Pageable pageable);

    /**
     *  Get the "id" task.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Task findOne(String id);

    /**
     *  Delete the "id" task.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    List<Task> findTasksByAccountFamilyId(String accountFamilyId);

    List<Task> findTasksByAccountFamilyIdAndPlannedStartTime(String accountFamilyId, String plannedStartTime);

    List<Task> findTasksByAccountFamilyIdAndPlannedStartTimeAndPlannedEndTime(String accountFamilyId, String plannedStartTime, String plannedEndTime);

    List<Task> findTasksByAccountFamilyIdAndPlannedStartTimeFromAWeek(String accountFamilyId, Date plannedStartTime);
}
