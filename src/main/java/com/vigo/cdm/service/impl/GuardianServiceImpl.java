package com.vigo.cdm.service.impl;

import com.vigo.cdm.service.GuardianService;
import com.vigo.cdm.domain.Guardian;
import com.vigo.cdm.repository.GuardianRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing Guardian.
 */
@Service
public class GuardianServiceImpl implements GuardianService{

    private final Logger log = LoggerFactory.getLogger(GuardianServiceImpl.class);

    @Inject
    private GuardianRepository guardianRepository;

    /**
     * Save a guardian.
     *
     * @param guardian the entity to save
     * @return the persisted entity
     */
    public Guardian save(Guardian guardian) {
        log.debug("Request to save Guardian : {}", guardian);
        Guardian result = guardianRepository.save(guardian);
        return result;
    }

    /**
     *  Get all the guardians.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<Guardian> findAll(Pageable pageable) {
        log.debug("Request to get all Guardians");
        Page<Guardian> result = guardianRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one guardian by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public Guardian findOne(String id) {
        log.debug("Request to get Guardian : {}", id);
        Guardian guardian = guardianRepository.findOne(id);
        return guardian;
    }

    /**
     *  Delete the  guardian by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Guardian : {}", id);
        guardianRepository.delete(id);
    }

    public List<Guardian> findGuardiansByAccountFamilyId(String accountFamilyId){
        log.debug("Request to get all Guardians : {}", accountFamilyId);
        List<Guardian> guardians = guardianRepository.findGuardiansByAccountFamilyId(new ObjectId(accountFamilyId));
        return guardians;
    }
}
