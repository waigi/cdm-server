package com.vigo.cdm.service.impl;


import com.vigo.cdm.service.TeacherService;
import com.vigo.cdm.domain.Teacher;
import com.vigo.cdm.repository.TeacherRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing Teacher.
 */
@Service
public class TeacherServiceImpl implements TeacherService{

    private final Logger log = LoggerFactory.getLogger(TeacherServiceImpl.class);

    @Inject
    private TeacherRepository teacherRepository;

    /**
     * Save a teacher.
     *
     * @param teacher the entity to save
     * @return the persisted entity
     */
    public Teacher save(Teacher teacher) {
        log.debug("Request to save Teacher : {}", teacher);
        Teacher result = teacherRepository.save(teacher);
        return result;
    }

    /**
     *  Get all the teachers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<Teacher> findAll(Pageable pageable) {
        log.debug("Request to get all Teachers");
        Page<Teacher> result = teacherRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one teacher by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public Teacher findOne(String id) {
        log.debug("Request to get Teacher : {}", id);
        Teacher teacher = teacherRepository.findOne(id);
        return teacher;
    }

    /**
     *  Delete the  teacher by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Teacher : {}", id);
        teacherRepository.delete(id);
    }

    public List<Teacher> findTeachersByAccountFamilyId(String accountFamilyId){
        log.debug("Request to get Teacher : {}", accountFamilyId);
        List<Teacher> teachers = teacherRepository.findTeachersByAccountFamiliesId(new ObjectId(accountFamilyId));
        return teachers;
    }


}
