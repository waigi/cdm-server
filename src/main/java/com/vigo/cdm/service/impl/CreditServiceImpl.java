package com.vigo.cdm.service.impl;

import com.vigo.cdm.domain.Task;
import com.vigo.cdm.service.CreditService;
import com.vigo.cdm.domain.Credit;
import com.vigo.cdm.repository.CreditRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Service Implementation for managing Credit.
 */
@Service
public class CreditServiceImpl implements CreditService{

    private final Logger log = LoggerFactory.getLogger(CreditServiceImpl.class);

    @Inject
    private CreditRepository creditRepository;

    /**
     * Save a credit.
     *
     * @param credit the entity to save
     * @return the persisted entity
     */
    public Credit save(Credit credit) {
        log.debug("Request to save Credit : {}", credit);
        Credit result = creditRepository.save(credit);
        return result;
    }

    /**
     *  Get all the credits.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<Credit> findAll(Pageable pageable) {
        log.debug("Request to get all Credits");
        Page<Credit> result = creditRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one credit by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public Credit findOne(String id) {
        log.debug("Request to get Credit : {}", id);
        Credit credit = creditRepository.findOne(id);
        return credit;
    }

    /**
     *  Delete the  credit by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Credit : {}", id);
        creditRepository.delete(id);
    }

    @Override
    public Credit findOneByTaskId(String taskId) {
        Credit credit = creditRepository.findOneByTaskId(new ObjectId(taskId));
        return credit;
    }

    public List<Credit> findCreditsByAccountFamilyIdAndTaskDateBetween(String accountFamilyId, String startDate, String endDate){
        Date startTime = new Date(Long.valueOf(startDate));
        Date endTime = new Date(Long.valueOf(endDate));
        List<Credit> result = creditRepository.findCreditsByAccountFamilyIdAndTaskDateBetweenQuery(new ObjectId(accountFamilyId), startTime, endTime);
        return result;
    }

    public List<Credit> findCreditsByAccountFamilyIdAndGraderTypeAndTaskDateBetween(String accountFamilyId, Integer graderType, String startDate, String endDate){
        Date startTime = new Date(Long.valueOf(startDate));
        Date endTime = new Date(Long.valueOf(endDate));
        List<Credit> result = creditRepository.findCreditsByAccountFamilyIdAndGraderTypeAndTaskDateBetweenQuery(new ObjectId(accountFamilyId), graderType, startTime, endTime);
        return result;
    }

    public Credit findOneByTaskIdAndGraderType(String taskId, Integer graderType) {
        Credit credit = creditRepository.findOneByTaskIdAndGraderType(new ObjectId(taskId), graderType);
        return credit;
    }
}
