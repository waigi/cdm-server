package com.vigo.cdm.service.impl;

import com.vigo.cdm.service.FamilyActivityService;
import com.vigo.cdm.domain.FamilyActivity;
import com.vigo.cdm.repository.FamilyActivityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing FamilyActivity.
 */
@Service
public class FamilyActivityServiceImpl implements FamilyActivityService{

    private final Logger log = LoggerFactory.getLogger(FamilyActivityServiceImpl.class);

    @Inject
    private FamilyActivityRepository familyActivityRepository;

    /**
     * Save a familyActivity.
     *
     * @param familyActivity the entity to save
     * @return the persisted entity
     */
    public FamilyActivity save(FamilyActivity familyActivity) {
        log.debug("Request to save FamilyActivity : {}", familyActivity);
        FamilyActivity result = familyActivityRepository.save(familyActivity);
        return result;
    }

    /**
     *  Get all the familyActivities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<FamilyActivity> findAll(Pageable pageable) {
        log.debug("Request to get all FamilyActivities");
        Page<FamilyActivity> result = familyActivityRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one familyActivity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public FamilyActivity findOne(String id) {
        log.debug("Request to get FamilyActivity : {}", id);
        FamilyActivity familyActivity = familyActivityRepository.findOne(id);
        return familyActivity;
    }

    /**
     *  Delete the  familyActivity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete FamilyActivity : {}", id);
        familyActivityRepository.delete(id);
    }

    public List<FamilyActivity> findFamilyActivitiesByAccountFamilyIdOrderByFamilyActivityTimeDesc(String accountFamilyId){
        log.debug("Request to get accountFamily all promises");
        //List<Promise> result = promiseRepository.findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(new ObjectId(accountFamilyId));
        List<FamilyActivity> result = familyActivityRepository.findFamilyActivitiesByAccountFamilyIdOrderByFamilyActivityTimeDesc(new ObjectId(accountFamilyId));
        return result;
    }

    public FamilyActivity findOneByTaskId(String taskId){
        log.debug("Request to get FamilyActivity by taskId");
        //List<Promise> result = promiseRepository.findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(new ObjectId(accountFamilyId));
        FamilyActivity result = familyActivityRepository.findOneByTaskId(new ObjectId(taskId));
        return result;
    }
}
