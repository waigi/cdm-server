package com.vigo.cdm.service.impl;

import com.vigo.cdm.service.PromiseService;
import com.vigo.cdm.domain.Promise;
import com.vigo.cdm.repository.PromiseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing Promise.
 */
@Service
public class PromiseServiceImpl implements PromiseService{

    private final Logger log = LoggerFactory.getLogger(PromiseServiceImpl.class);

    @Inject
    private PromiseRepository promiseRepository;

    /**
     * Save a promise.
     *
     * @param promise the entity to save
     * @return the persisted entity
     */
    public Promise save(Promise promise) {
        log.debug("Request to save Promise : {}", promise);
        Promise result = promiseRepository.save(promise);
        return result;
    }

    /**
     *  Get all the promises.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<Promise> findAll(Pageable pageable) {
        log.debug("Request to get all Promises");
        Page<Promise> result = promiseRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one promise by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public Promise findOne(String id) {
        log.debug("Request to get Promise : {}", id);
        Promise promise = promiseRepository.findOne(id);
        return promise;
    }

    /**
     *  Delete the  promise by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Promise : {}", id);
        promiseRepository.delete(id);
    }

    public List<Promise> findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(String accountFamilyId){
        log.debug("Request to get accountFamily all promises");
        List<Promise> result = promiseRepository.findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(new ObjectId(accountFamilyId));
        //List<Promise> result = promiseRepository.findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(accountFamilyId);
        return result;
    }

    /*public List<Promise> findPromisesByTaskIdOrderByPromiseTimeDesc(String taskId){
        log.debug("Request to get task all promises");
        List<Promise> result = promiseRepository.findPromisesByTaskIdOrderByPromiseTimeDesc(taskId);
        return result;
    }*/

    /*public Promise findOneByTaskId(String taskId){
        log.debug("Request to get promise of task");
        Promise result = promiseRepository.findOneByTaskId(new ObjectId(taskId));
        return result;
    }*/
}
