package com.vigo.cdm.service.impl;

import com.vigo.cdm.service.TaskService;
import com.vigo.cdm.domain.Task;
import com.vigo.cdm.repository.TaskRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Service Implementation for managing Task.
 */
@Service
public class TaskServiceImpl implements TaskService{

    private final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Inject
    private TaskRepository taskRepository;

    /**
     * Save a task.
     *
     * @param task the entity to save
     * @return the persisted entity
     */
    public Task save(Task task) {
        log.debug("Request to save Task : {}", task);
        Task result = taskRepository.save(task);
        return result;
    }

    /**
     *  Get all the tasks.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<Task> findAll(Pageable pageable) {
        log.debug("Request to get all Tasks");
        Page<Task> result = taskRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one task by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public Task findOne(String id) {
        log.debug("Request to get Task : {}", id);
        Task task = taskRepository.findOne(id);
        return task;
    }

    /**
     *  Delete the  task by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Task : {}", id);
        taskRepository.delete(id);
    }

    /**
     *  Get accountFamily the tasks.
     *
     *  @param accountFamilyId
     *  @return the list of entities
     */
    public List<Task> findTasksByAccountFamilyId(String accountFamilyId) {
        log.debug("Request to get accountFamily all Tasks");

        List<Task> result = taskRepository.findAllByAccountFamilyId(new ObjectId(accountFamilyId));
        return result;
    }

    public List<Task> findTasksByAccountFamilyIdAndPlannedStartTime(String accountFamilyId, String plannedStartTime){
        log.debug("Request to get accountFamily plannedStartTime all Tasks");
        Date startTime = new Date(Long.valueOf(plannedStartTime));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);

        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
        Date fromDate = fromCalendar.getTime();

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(fromDate);
        toCalendar.set(Calendar.DATE, toCalendar.get(Calendar.DATE) + 1);
        Date toDate = toCalendar.getTime();

        List<Task> result = taskRepository.findByAccountFamilyIdAndPlannedStartTimeQuery(new ObjectId(accountFamilyId), fromDate, toDate);
        return result;
    }

    public List<Task> findTasksByAccountFamilyIdAndPlannedStartTimeAndPlannedEndTime(String accountFamilyId, String plannedStartTime, String plannedEndTime){
        log.debug("Request to get accountFamily plannedStartTime all Tasks");
        Date startTime = new Date(Long.valueOf(plannedStartTime));
        Date endTime = new Date(Long.valueOf(plannedEndTime));

        List<Task> result = taskRepository.findByAccountFamilyIdAndPlannedStartTimeQuery(new ObjectId(accountFamilyId),startTime, endTime);
        return result;
    }

    public  List<Task> findTasksByAccountFamilyIdAndPlannedStartTimeFromAWeek(String accountFamilyId, Date plannedStartTime){
        //Date startTime = new Date(Long.valueOf(plannedStartTime));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(plannedStartTime);

        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)+1, 0, 0, 0);
        Date toDate = fromCalendar.getTime();

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(toDate);
        toCalendar.set(Calendar.DATE, toCalendar.get(Calendar.DATE) - 6);
        Date fromDate = toCalendar.getTime();
        List<Task> result = taskRepository.findByAccountFamilyIdAndPlannedStartTimeQuery(new ObjectId(accountFamilyId), fromDate, toDate);
        return result;
    }

}
