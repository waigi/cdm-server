package com.vigo.cdm.service.impl;

import com.vigo.cdm.service.AccountFamilyService;
import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.repository.AccountFamilyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing AccountFamily.
 */
@Service
public class AccountFamilyServiceImpl implements AccountFamilyService{

    private final Logger log = LoggerFactory.getLogger(AccountFamilyServiceImpl.class);
    
    @Inject
    private AccountFamilyRepository accountFamilyRepository;
    
    /**
     * Save a accountFamily.
     * 
     * @param accountFamily the entity to save
     * @return the persisted entity
     */
    public AccountFamily save(AccountFamily accountFamily) {
        log.debug("Request to save AccountFamily : {}", accountFamily);
        AccountFamily result = accountFamilyRepository.save(accountFamily);
        return result;
    }

    /**
     *  Get all the accountFamilies.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<AccountFamily> findAll(Pageable pageable) {
        log.debug("Request to get all AccountFamilies");
        Page<AccountFamily> result = accountFamilyRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one accountFamily by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public AccountFamily findOne(String id) {
        log.debug("Request to get AccountFamily : {}", id);
        AccountFamily accountFamily = accountFamilyRepository.findOne(id);
        return accountFamily;
    }

    /**
     *  Delete the  accountFamily by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete AccountFamily : {}", id);
        accountFamilyRepository.delete(id);
    }
}
