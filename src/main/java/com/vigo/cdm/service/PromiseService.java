package com.vigo.cdm.service;

import com.vigo.cdm.domain.Promise;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Promise.
 */
public interface PromiseService {

    /**
     * Save a promise.
     *
     * @param promise the entity to save
     * @return the persisted entity
     */
    Promise save(Promise promise);

    /**
     *  Get all the promises.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Promise> findAll(Pageable pageable);

    /**
     *  Get the "id" promise.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Promise findOne(String id);

    /**
     *  Delete the "id" promise.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    List<Promise> findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(String accountFamilyId);

    //List<Promise> findPromisesByTaskIdOrderByPromiseTimeDesc(String taskId);

    //Promise findOneByTaskId(String taskId);
}
