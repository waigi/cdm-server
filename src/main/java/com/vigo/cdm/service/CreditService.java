package com.vigo.cdm.service;

import com.vigo.cdm.domain.Credit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Credit.
 */
public interface CreditService {

    /**
     * Save a credit.
     *
     * @param credit the entity to save
     * @return the persisted entity
     */
    Credit save(Credit credit);

    /**
     *  Get all the credits.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Credit> findAll(Pageable pageable);

    /**
     *  Get the "id" credit.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Credit findOne(String id);

    /**
     *  Delete the "id" credit.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    Credit findOneByTaskId(String taskId);

    Credit findOneByTaskIdAndGraderType(String taskId, Integer graderType);

    List<Credit> findCreditsByAccountFamilyIdAndTaskDateBetween(String accountFamilyId, String startDate, String endDate);

    List<Credit> findCreditsByAccountFamilyIdAndGraderTypeAndTaskDateBetween(String accountFamilyId, Integer graderType, String startDate, String endDate);
}
