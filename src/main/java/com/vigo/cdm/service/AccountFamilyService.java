package com.vigo.cdm.service;

import com.vigo.cdm.domain.AccountFamily;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing AccountFamily.
 */
public interface AccountFamilyService {

    /**
     * Save a accountFamily.
     * 
     * @param accountFamily the entity to save
     * @return the persisted entity
     */
    AccountFamily save(AccountFamily accountFamily);

    /**
     *  Get all the accountFamilies.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AccountFamily> findAll(Pageable pageable);

    /**
     *  Get the "id" accountFamily.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    AccountFamily findOne(String id);

    /**
     *  Delete the "id" accountFamily.
     *  
     *  @param id the id of the entity
     */
    void delete(String id);
}
