package com.vigo.cdm.service;

import com.vigo.cdm.domain.FamilyActivity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing FamilyActivity.
 */
public interface FamilyActivityService {

    /**
     * Save a familyActivity.
     *
     * @param familyActivity the entity to save
     * @return the persisted entity
     */
    FamilyActivity save(FamilyActivity familyActivity);

    /**
     *  Get all the familyActivities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FamilyActivity> findAll(Pageable pageable);

    /**
     *  Get the "id" familyActivity.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FamilyActivity findOne(String id);

    /**
     *  Delete the "id" familyActivity.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    List<FamilyActivity> findFamilyActivitiesByAccountFamilyIdOrderByFamilyActivityTimeDesc(String accountFamilyId);

    FamilyActivity findOneByTaskId(String taskId);

}
