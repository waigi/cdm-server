package com.vigo.cdm.repository;

import com.vigo.cdm.domain.User;

import java.time.ZonedDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the User entity.
 */
public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(ZonedDateTime dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);

    Optional<User> findOneById(String userId);

    Optional<User> findOneByStudentId(ObjectId studentId);

    User findOneByTeacherId(ObjectId teacherId);

    User findOneByGuardianId(ObjectId guardianId);
    @Override
    void delete(User t);

}
