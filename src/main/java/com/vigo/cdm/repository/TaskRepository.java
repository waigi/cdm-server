package com.vigo.cdm.repository;

import com.vigo.cdm.domain.Task;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Spring Data MongoDB repository for the Task entity.
 */

public interface TaskRepository extends MongoRepository<Task,String> {

    //@Query(value="{'accountFamily._id':?0}")
    List<Task> findAllByAccountFamilyId(ObjectId accountFamilyId);

    //@Query(value="{'accountFamily._id':?0}, plannedStartTime : {$gte:?1, $lt:?2}}")
    @Query("{'accountFamily.id':?0,'plannedStartTime':{$gte:?1, $lt:?2}}")
    List<Task> findByAccountFamilyIdAndPlannedStartTimeQuery(ObjectId accountFamilyId,Date startTime,Date endTime);
}
