package com.vigo.cdm.repository;

import com.vigo.cdm.domain.Promise;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Promise entity.
 */

public interface PromiseRepository extends MongoRepository<Promise,String> {

    List<Promise> findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(ObjectId accountFamilyId);
    //List<Promise> findPromisesByAccountFamilyIdOrderByPromiseTimeDesc(String accountFamilyId);

    //List<Promise> findPromisesByTaskIdOrderByPromiseTimeDesc(String taskId);

    //Promise findOneByTaskId(ObjectId taskId);
}
