package com.vigo.cdm.repository;

import com.vigo.cdm.domain.AccountFamily;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the AccountFamily entity.
 */

public interface AccountFamilyRepository extends MongoRepository<AccountFamily,String> {

}
