package com.vigo.cdm.repository;

import com.vigo.cdm.domain.FamilyActivity;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the FamilyActivity entity.
 */

public interface FamilyActivityRepository extends MongoRepository<FamilyActivity,String> {

    List<FamilyActivity> findFamilyActivitiesByAccountFamilyIdOrderByFamilyActivityTimeDesc(ObjectId accountFamilyId);

    FamilyActivity findOneByTaskId(ObjectId taskId);
}
