package com.vigo.cdm.repository;

import com.vigo.cdm.domain.Credit;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Spring Data MongoDB repository for the Credit entity.
 */

public interface CreditRepository extends MongoRepository<Credit,String> {

    Credit findOneByTaskId(ObjectId taskId);

    Credit findOneByTaskIdAndGraderType(ObjectId taskId, Integer graderType);

    @Query("{'task.accountFamily.id':?0,'taskDate':{$gte:?1, $lt:?2}}")
    List<Credit> findCreditsByAccountFamilyIdAndTaskDateBetweenQuery(ObjectId accountFamilyId, Date startDate, Date endDate);

    @Query("{'task.accountFamily.id':?0,'graderType':?1,'taskDate':{$gte:?2, $lt:?3}}")
    List<Credit> findCreditsByAccountFamilyIdAndGraderTypeAndTaskDateBetweenQuery(ObjectId accountFamilyId, Integer graderType, Date startDate, Date endDate);
}
