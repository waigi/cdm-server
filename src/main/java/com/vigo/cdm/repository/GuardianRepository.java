package com.vigo.cdm.repository;

import com.vigo.cdm.domain.Guardian;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Guardian entity.
 */

public interface GuardianRepository extends MongoRepository<Guardian,String> {

    List<Guardian> findGuardiansByAccountFamilyId(ObjectId accountFamilyId);
}
