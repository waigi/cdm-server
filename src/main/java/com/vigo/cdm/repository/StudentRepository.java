package com.vigo.cdm.repository;

import com.vigo.cdm.domain.Student;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Student entity.
 */

public interface StudentRepository extends MongoRepository<Student,String> {

    Student findOneByAccountFamilyId(ObjectId accountFamilyId);
}
