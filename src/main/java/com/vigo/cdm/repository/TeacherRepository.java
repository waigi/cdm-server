package com.vigo.cdm.repository;

import com.vigo.cdm.domain.Teacher;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Teacher entity.
 */

public interface TeacherRepository extends MongoRepository<Teacher,String> {

    List<Teacher> findTeachersByAccountFamiliesId(ObjectId accountFamilyId);
}
