(function() {
    'use strict';

    angular
        .module('cdmApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
