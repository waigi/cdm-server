(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('PromiseDialogController', PromiseDialogController);

    PromiseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Promise'];

    function PromiseDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Promise) {
        var vm = this;

        vm.promise = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.promise.id !== null) {
                Promise.update(vm.promise, onSaveSuccess, onSaveError);
            } else {
                Promise.save(vm.promise, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cdmApp:promiseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
