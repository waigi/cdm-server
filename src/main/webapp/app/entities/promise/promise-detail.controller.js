(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('PromiseDetailController', PromiseDetailController);

    PromiseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Promise'];

    function PromiseDetailController($scope, $rootScope, $stateParams, entity, Promise) {
        var vm = this;

        vm.promise = entity;

        var unsubscribe = $rootScope.$on('cdmApp:promiseUpdate', function(event, result) {
            vm.promise = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
