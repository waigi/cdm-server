(function() {
    'use strict';
    angular
        .module('cdmApp')
        .factory('Promise', Promise);

    Promise.$inject = ['$resource'];

    function Promise ($resource) {
        var resourceUrl =  'api/promises/:accountFamilyId';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
