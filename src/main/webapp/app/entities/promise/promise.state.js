(function() {
    'use strict';

    angular
        .module('cdmApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('promise', {
            parent: 'entity',
            url: '/promise?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.promise.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/promise/promises.html',
                    controller: 'PromiseController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('promise');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('promise-detail', {
            parent: 'entity',
            url: '/promise/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.promise.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/promise/promise-detail.html',
                    controller: 'PromiseDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('promise');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Promise', function($stateParams, Promise) {
                    return Promise.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('promise.new', {
            parent: 'promise',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/promise/promise-dialog.html',
                    controller: 'PromiseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('promise', null, { reload: true });
                }, function() {
                    $state.go('promise');
                });
            }]
        })
        .state('promise.edit', {
            parent: 'promise',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/promise/promise-dialog.html',
                    controller: 'PromiseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Promise', function(Promise) {
                            return Promise.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('promise', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('promise.delete', {
            parent: 'promise',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/promise/promise-delete-dialog.html',
                    controller: 'PromiseDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Promise', function(Promise) {
                            return Promise.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('promise', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
