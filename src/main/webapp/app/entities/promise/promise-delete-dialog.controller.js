(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('PromiseDeleteController',PromiseDeleteController);

    PromiseDeleteController.$inject = ['$uibModalInstance', 'entity', 'Promise'];

    function PromiseDeleteController($uibModalInstance, entity, Promise) {
        var vm = this;

        vm.promise = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Promise.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
