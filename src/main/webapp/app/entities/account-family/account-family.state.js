(function() {
    'use strict';

    angular
        .module('cdmApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('account-family', {
            parent: 'entity',
            url: '/account-family?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.accountFamily.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/account-family/account-families.html',
                    controller: 'AccountFamilyController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('accountFamily');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('account-family-detail', {
            parent: 'entity',
            url: '/account-family/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.accountFamily.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/account-family/account-family-detail.html',
                    controller: 'AccountFamilyDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('accountFamily');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AccountFamily', function($stateParams, AccountFamily) {
                    return AccountFamily.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('account-family.new', {
            parent: 'account-family',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            views: {
                    'content@': {
                        templateUrl: 'app/entities/account-family/account-family-create.html',
                        controller: 'AccountFamilyCreateController',
                        controllerAs: 'vm',
                    }
            },
            resolve: {
                entity: function () {
                    return {
                        name:null,
                        description: null,
                        id: null,
                        child:{name:null, login:null},
                        guardianDTOList:[{name:null, login:null, email:null, relationship:null}]
                    };
                }
            }
            /*onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-family/account-family-dialog.html',
                    controller: 'AccountFamilyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('account-family', null, { reload: true });
                }, function() {
                    $state.go('account-family');
                });
            }]*/
        })
        .state('account-family.edit', {
            parent: 'account-family',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-family/account-family-dialog.html',
                    controller: 'AccountFamilyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AccountFamily', function(AccountFamily) {
                            return AccountFamily.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('account-family', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('account-family.delete', {
            parent: 'account-family',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-family/account-family-delete-dialog.html',
                    controller: 'AccountFamilyDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AccountFamily', function(AccountFamily) {
                            return AccountFamily.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('account-family', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
