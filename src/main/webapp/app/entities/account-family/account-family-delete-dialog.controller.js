(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('AccountFamilyDeleteController',AccountFamilyDeleteController);

    AccountFamilyDeleteController.$inject = ['$uibModalInstance', 'entity', 'AccountFamily'];

    function AccountFamilyDeleteController($uibModalInstance, entity, AccountFamily) {
        var vm = this;

        vm.accountFamily = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AccountFamily.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
