(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('AccountFamilyDialogController', AccountFamilyDialogController);

    AccountFamilyDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$state', 'entity', 'AccountFamily'];

    function AccountFamilyDialogController ($timeout, $scope, $stateParams, $state,entity, AccountFamily) {
        var vm = this;

        vm.accountFamily = entity;
        vm.clear = clear;
        vm.save = save;
        vm.addGuardian = addGuardian;
        vm.removeGuardian = removeGuardian;
        vm.getNextTab = getNextTab;
        vm.onTabClick = onTabClick;
        vm.getPreTab = getPreTab;
        vm.pass = false;
        vm.passFunction = passFunction;
        vm.relationships = [{name:'father'},{name: 'mother'},{name:'brother'},{name:'sister'}]

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });


        function clear () {
            //$uibModalInstance.dismiss('cancel');
            $state.go('account-family', null, { reload: true });

        }

        function save () {
            vm.isSaving = true;
            if (vm.accountFamily.id !== null) {
                AccountFamily.update(vm.accountFamily, onSaveSuccess, onSaveError);
            } else {
                AccountFamily.save(vm.accountFamily, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cdmApp:accountFamilyUpdate', result);
            //$uibModalInstance.close(result);
            $state.go('account-family', null, { reload: true });
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
            /*console.log("save failed called "+response.data.message);
            if (response.data.message==='Name cannot be empty'){
                $scope.error = 'ERROR';
            }else if (response.data.message==='Spouse Relationship error'){
                $scope.Spouseerror = 'Spouseerror';//Please select the Relationship of the spouse/Partner in Step 2
            }else if (response.data.message==='Living Children Signatory Error'){
                $scope.childerror = 'childerror';//Please select the Relationship of the Living Children in Step 2
            }else if (response.data.message==='Signatory Relationship error'){
                $scope.signerror = 'signerror';//Please select the Relationship of the Signatory in Step 3
            }*/
        }




    }
})();
