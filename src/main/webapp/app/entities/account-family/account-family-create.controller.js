(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('AccountFamilyCreateController', AccountFamilyCreateController);

    AccountFamilyCreateController.$inject = ['$timeout', '$scope', '$stateParams', '$state', 'entity', 'AccountFamily'];

    function AccountFamilyCreateController ($timeout, $scope, $stateParams, $state,entity, AccountFamily) {
        var vm = this;

        vm.accountFamily = entity;
        vm.clear = clear;
        vm.save = save;
        vm.addGuardian = addGuardian;
        vm.removeGuardian = removeGuardian;
        vm.getNextTab = getNextTab;
        vm.onTabClick = onTabClick;
        vm.getPreTab = getPreTab;
        vm.pass = false;
        vm.passFunction = passFunction;
        vm.relationships = [{name:'father'},{name: 'mother'},{name:'brother'},{name:'sister'}]

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
            passFunction();
        });


        function clear () {
            //$uibModalInstance.dismiss('cancel');
            $state.go('account-family', null, { reload: true });

        }

        function save () {
            vm.isSaving = true;
            if (vm.accountFamily.id !== null) {
                AccountFamily.update(vm.accountFamily, onSaveSuccess, onSaveError);
            } else {
                AccountFamily.save(vm.accountFamily, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cdmApp:accountFamilyUpdate', result);
            //$uibModalInstance.close(result);
            $state.go('account-family', null, { reload: true });
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
            console.log("save failed called "+response.data.message);
            if (response.data.message==='Name cannot be empty'){
                $scope.error = 'ERROR';
            }else if (response.data.message==='Spouse Relationship error'){
                $scope.Spouseerror = 'Spouseerror';//Please select the Relationship of the spouse/Partner in Step 2
            }else if (response.data.message==='Living Children Signatory Error'){
                $scope.childerror = 'childerror';//Please select the Relationship of the Living Children in Step 2
            }else if (response.data.message==='Signatory Relationship error'){
                $scope.signerror = 'signerror';//Please select the Relationship of the Signatory in Step 3
            }
        }
        function addGuardian () {
            vm.accountFamily.guardianDTOList.push({
                name: "",
                login: "",
                email:"",
                relationship:""
            });
        }
        function removeGuardian (key) {
            vm.accountFamily.guardianDTOList.splice(key, 1);
        }
        vm.tabIndex=1;
        vm.tabIndexTotal=1;
        vm.nextLabel="Next";
        vm.preLabel="First";
        vm.currentTab=1;
        vm.stepTotel=3;

        function passFunction(){
            //alert(vm.currentTab);
            if(vm.currentTab==2){
                //alert($scope.form.child_name.$invalid);
                if($scope.form.child_name.$invalid || $scope.form.child_login.$invalid){
                    //alert("123");
                    vm.pass = true;
                }else{
                    vm.pass = false;
                }
            }else if(vm.currentTab==1){
                if($scope.form.accountFamily_name.$invalid){
                    //alert("123");
                    vm.pass = true;
                }else{
                    vm.pass = false;
                }
            }
            /*else if(vm.currentTab==3){
                alert(vm.pass);
                if($scope.form.relationship.$invalid || $scope.form.guardian_Name.$invalid || $scope.form.guardian_login.$invalid){
                    //alert("123");
                    vm.pass = true;
                }else{
                    vm.pass = false;
                }

            }*/
        }

        function getNextTab () {
            //alert($scope.form.login.$invalid);
            if(vm.nextLabel=="Last"){
                //
                save();

            }else if(vm.currentTab< vm.stepTotel && vm.currentTab+1 !=vm.stepTotel){
                vm.preLabel="Previous";
                vm.currentTab++;
                angular.element('[data-target="#tab'+vm.currentTab+'"]').tab('show');
            }else if(vm.currentTab< vm.stepTotel && vm.currentTab+1 ==vm.stepTotel){
                vm.nextLabel="Last";
                vm.currentTab++;
                angular.element('[data-target="#tab'+vm.currentTab+'"]').tab('show');
                vm.pass = true;
            }
            passFunction();
        };

        function onTabClick (tabNumber) {
            //angular.forEach($scope.stepsAndTabsMapping,function(currentMap){
            //    if(currentMap.tab==tabNumber){
            vm.currentTab=tabNumber;
            //    }
            //});

            if(tabNumber==1){
                vm.currentTab =1;
            }
            if(vm.currentTab ==1){
                vm.preLabel="First";
            }else{
                vm.preLabel="Previous";
            }

            if(vm.currentTab ==vm.stepTotel){
                vm.nextLabel="Last";
            }else{
                vm.nextLabel="Next";
            }

        };

        function getPreTab () {
            //alert($scope.form.$valid);
            if(vm.nextLabel=="Last" && vm.currentTab== vm.stepTotel){
                vm.nextLabel="Next";
                vm.currentTab--;
                if(vm.currentTab==1){
                    vm.preLabel="First";
                    angular.element('[data-target="#tab1"]').tab('show');
                }else{
                    angular.element('[data-target="#tab'+vm.currentTab+'"]').tab('show');
                }

            }else if(vm.currentTab>1 && vm.currentTab<=vm.stepTotel){
                vm.currentTab--;
                if(vm.currentTab==1){
                    vm.preLabel="First";
                    angular.element('[data-target="#tab1"]').tab('show');
                }else{
                    //angular.forEach($scope.stepsAndTabsMapping,function(currentMap){
                    //    if(currentMap.step==$scope.currentTab){
                    angular.element('[data-target="#tab'+vm.currentTab+'"]').tab('show');
                    //    }
                    //});
                }
            }
            passFunction();
        };

    }
})();
