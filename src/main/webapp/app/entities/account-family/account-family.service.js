(function() {
    'use strict';
    angular
        .module('cdmApp')
        .factory('AccountFamily', AccountFamily);

    AccountFamily.$inject = ['$resource'];

    function AccountFamily ($resource) {
        var resourceUrl =  'api/account-families/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
