(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('AccountFamilyDetailController', AccountFamilyDetailController);

    AccountFamilyDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'AccountFamily'];

    function AccountFamilyDetailController($scope, $rootScope, $stateParams, entity, AccountFamily) {
        var vm = this;

        vm.accountFamilyMemberDTO = entity;

        var unsubscribe = $rootScope.$on('cdmApp:accountFamilyUpdate', function(event, result) {
            vm.accountFamilyMemberDTO = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
