(function() {
    'use strict';
    angular
        .module('cdmApp')
        .factory('FamilyActivity', FamilyActivity);

    FamilyActivity.$inject = ['$resource'];

    function FamilyActivity ($resource) {
        var resourceUrl =  'api/family-activities/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
