(function() {
    'use strict';

    angular
        .module('cdmApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('family-activity', {
            parent: 'entity',
            url: '/family-activity?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.familyActivity.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/family-activity/family-activities.html',
                    controller: 'FamilyActivityController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('familyActivity');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('family-activity-detail', {
            parent: 'entity',
            url: '/family-activity/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.familyActivity.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/family-activity/family-activity-detail.html',
                    controller: 'FamilyActivityDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('familyActivity');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'FamilyActivity', function($stateParams, FamilyActivity) {
                    return FamilyActivity.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('family-activity.new', {
            parent: 'family-activity',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/family-activity/family-activity-dialog.html',
                    controller: 'FamilyActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('family-activity', null, { reload: true });
                }, function() {
                    $state.go('family-activity');
                });
            }]
        })
        .state('family-activity.edit', {
            parent: 'family-activity',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/family-activity/family-activity-dialog.html',
                    controller: 'FamilyActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FamilyActivity', function(FamilyActivity) {
                            return FamilyActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('family-activity', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('family-activity.delete', {
            parent: 'family-activity',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/family-activity/family-activity-delete-dialog.html',
                    controller: 'FamilyActivityDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['FamilyActivity', function(FamilyActivity) {
                            return FamilyActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('family-activity', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
