(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('FamilyActivityDetailController', FamilyActivityDetailController);

    FamilyActivityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'FamilyActivity'];

    function FamilyActivityDetailController($scope, $rootScope, $stateParams, entity, FamilyActivity) {
        var vm = this;

        vm.familyActivity = entity;

        var unsubscribe = $rootScope.$on('cdmApp:familyActivityUpdate', function(event, result) {
            vm.familyActivity = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
