(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('FamilyActivityDialogController', FamilyActivityDialogController);

    FamilyActivityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'FamilyActivity'];

    function FamilyActivityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, FamilyActivity) {
        var vm = this;

        vm.familyActivity = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.familyActivity.id !== null) {
                FamilyActivity.update(vm.familyActivity, onSaveSuccess, onSaveError);
            } else {
                FamilyActivity.save(vm.familyActivity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cdmApp:familyActivityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
