(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('FamilyActivityDeleteController',FamilyActivityDeleteController);

    FamilyActivityDeleteController.$inject = ['$uibModalInstance', 'entity', 'FamilyActivity'];

    function FamilyActivityDeleteController($uibModalInstance, entity, FamilyActivity) {
        var vm = this;

        vm.familyActivity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            FamilyActivity.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
