(function() {
    'use strict';
    angular
        .module('cdmApp')
        .factory('Credit', Credit);

    Credit.$inject = ['$resource'];

    function Credit ($resource) {
        var resourceUrl =  'api/credits/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
