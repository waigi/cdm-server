(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('CreditDialogController', CreditDialogController);

    CreditDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Credit'];

    function CreditDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Credit) {
        var vm = this;

        vm.credit = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.credit.id !== null) {
                Credit.update(vm.credit, onSaveSuccess, onSaveError);
            } else {
                Credit.save(vm.credit, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cdmApp:creditUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
