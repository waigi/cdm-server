(function() {
    'use strict';

    angular
        .module('cdmApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('credit', {
            parent: 'entity',
            url: '/credit?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.credit.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/credit/credits.html',
                    controller: 'CreditController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('credit');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('credit-detail', {
            parent: 'entity',
            url: '/credit/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.credit.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/credit/credit-detail.html',
                    controller: 'CreditDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('credit');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Credit', function($stateParams, Credit) {
                    return Credit.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('credit.new', {
            parent: 'credit',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credit/credit-dialog.html',
                    controller: 'CreditDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                review: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('credit', null, { reload: true });
                }, function() {
                    $state.go('credit');
                });
            }]
        })
        .state('credit.edit', {
            parent: 'credit',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credit/credit-dialog.html',
                    controller: 'CreditDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Credit', function(Credit) {
                            return Credit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('credit', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('credit.delete', {
            parent: 'credit',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credit/credit-delete-dialog.html',
                    controller: 'CreditDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Credit', function(Credit) {
                            return Credit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('credit', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
