(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('CreditDetailController', CreditDetailController);

    CreditDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Credit'];

    function CreditDetailController($scope, $rootScope, $stateParams, entity, Credit) {
        var vm = this;

        vm.credit = entity;

        var unsubscribe = $rootScope.$on('cdmApp:creditUpdate', function(event, result) {
            vm.credit = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
