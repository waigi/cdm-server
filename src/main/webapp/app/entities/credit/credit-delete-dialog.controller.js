(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('CreditDeleteController',CreditDeleteController);

    CreditDeleteController.$inject = ['$uibModalInstance', 'entity', 'Credit'];

    function CreditDeleteController($uibModalInstance, entity, Credit) {
        var vm = this;

        vm.credit = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Credit.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
