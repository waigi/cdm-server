(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('GuardianDetailController', GuardianDetailController);

    GuardianDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Guardian'];

    function GuardianDetailController($scope, $rootScope, $stateParams, entity, Guardian) {
        var vm = this;

        vm.guardian = entity;

        var unsubscribe = $rootScope.$on('cdmApp:guardianUpdate', function(event, result) {
            vm.guardian = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
