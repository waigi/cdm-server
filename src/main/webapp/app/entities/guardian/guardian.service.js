(function() {
    'use strict';
    angular
        .module('cdmApp')
        .factory('Guardian', Guardian);

    Guardian.$inject = ['$resource'];

    function Guardian ($resource) {
        var resourceUrl =  'api/guardians/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
