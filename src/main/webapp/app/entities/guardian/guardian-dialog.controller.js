(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('GuardianDialogController', GuardianDialogController);

    GuardianDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Guardian','AccountFamily'];

    function GuardianDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Guardian,AccountFamily) {
        var vm = this;

        vm.guardian = entity;
        vm.clear = clear;
        vm.save = save;
        vm.accountFamilies = AccountFamily.query();
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.guardian.id !== null) {
                Guardian.update(vm.guardian, onSaveSuccess, onSaveError);
            } else {
                Guardian.save(vm.guardian, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cdmApp:guardianUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
