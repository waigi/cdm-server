(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('GuardianDeleteController',GuardianDeleteController);

    GuardianDeleteController.$inject = ['$uibModalInstance', 'entity', 'Guardian'];

    function GuardianDeleteController($uibModalInstance, entity, Guardian) {
        var vm = this;

        vm.guardian = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Guardian.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
