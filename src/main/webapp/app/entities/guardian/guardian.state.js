(function() {
    'use strict';

    angular
        .module('cdmApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('guardian', {
            parent: 'entity',
            url: '/guardian?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.guardian.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/guardian/guardians.html',
                    controller: 'GuardianController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('guardian');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('guardian-detail', {
            parent: 'entity',
            url: '/guardian/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cdmApp.guardian.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/guardian/guardian-detail.html',
                    controller: 'GuardianDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('guardian');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Guardian', function($stateParams, Guardian) {
                    return Guardian.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('guardian.new', {
            parent: 'guardian',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/guardian/guardian-dialog.html',
                    controller: 'GuardianDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('guardian', null, { reload: true });
                }, function() {
                    $state.go('guardian');
                });
            }]
        })
        .state('guardian.edit', {
            parent: 'guardian',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/guardian/guardian-dialog.html',
                    controller: 'GuardianDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Guardian', function(Guardian) {
                            return Guardian.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('guardian', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('guardian.delete', {
            parent: 'guardian',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/guardian/guardian-delete-dialog.html',
                    controller: 'GuardianDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Guardian', function(Guardian) {
                            return Guardian.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('guardian', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
