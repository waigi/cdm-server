(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('TeacherDetailController', TeacherDetailController);

    TeacherDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Teacher'];

    function TeacherDetailController($scope, $rootScope, $stateParams, entity, Teacher) {
        var vm = this;

        vm.teacher = entity;

        var unsubscribe = $rootScope.$on('cdmApp:teacherUpdate', function(event, result) {
            vm.teacher = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
