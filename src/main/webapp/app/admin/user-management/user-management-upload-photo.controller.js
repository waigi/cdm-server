(function() {
    'use strict';

    angular
        .module('cdmApp')
        .controller('UserManagementUploadPhotoController',UserManagementUploadPhotoController);

    UserManagementUploadPhotoController.$inject = ['$stateParams', '$uibModalInstance', 'entity', 'User', 'JhiLanguageService','UserFileUpload'];

    function UserManagementUploadPhotoController ($stateParams, $uibModalInstance, entity, User, JhiLanguageService, UserFileUpload) {
        var vm = this;

        vm.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        vm.clear = clear;
        vm.languages = null;
        vm.save = save;
        vm.userDTO = entity;


        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess (result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function save () {
            /*vm.isSaving = true;
            if (vm.user.id !== null) {
                User.update(vm.user, onSaveSuccess, onSaveError);
            } else {
                User.save(vm.user, onSaveSuccess, onSaveError);
            }*/
            UserFileUpload.post(vm.userDTO,onSaveSuccess,onSaveError);
        }
    }
})();
