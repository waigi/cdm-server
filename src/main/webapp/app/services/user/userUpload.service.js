(function () {
    'use strict';

    angular
        .module('cdmApp').service('UserFileUpload', ['$http', function ($http) {
            "ngInject";
            var uploadUrl = "api/userFileUpload";
            this.post = function(data) {
                var fd = new FormData();
                for(var key in data ){
                    fd.append(key, data[key]);
                }
                $http.post(uploadUrl,fd,{
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function(){
                  return true;
                }).error(function(){
                    return false;
                });
            }

        }]);
})();
/*(function () {
    'use strict';

    angular
        .module('cdmApp')
        .factory('UserFileUpload', UserFileUpload);

    User.$inject = ['$resource'];

    function User ($resource) {
        var service = $resource('api/userFileUpload/:login', {}, {
            'save': { method:'POST', function(data)
        {
            var fd = new FormData();
            for (var key in data) {
                fd.append(key, data[key]);
            }
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function () {
                return true;
            }).error(function () {
                return false;
            });

        });

        return service;
    }
})();*/
