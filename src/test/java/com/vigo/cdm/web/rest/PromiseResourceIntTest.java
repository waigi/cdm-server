package com.vigo.cdm.web.rest;

import com.vigo.cdm.CdmApp;
import com.vigo.cdm.domain.Promise;
import com.vigo.cdm.repository.PromiseRepository;
import com.vigo.cdm.service.PromiseService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PromiseResource REST controller.
 *
 * @see PromiseResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CdmApp.class)
@WebAppConfiguration
@IntegrationTest
public class PromiseResourceIntTest {


    @Inject
    private PromiseRepository promiseRepository;

    @Inject
    private PromiseService promiseService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPromiseMockMvc;

    private Promise promise;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PromiseResource promiseResource = new PromiseResource();
        ReflectionTestUtils.setField(promiseResource, "promiseService", promiseService);
        this.restPromiseMockMvc = MockMvcBuilders.standaloneSetup(promiseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        promiseRepository.deleteAll();
        promise = new Promise();
    }

    @Test
    public void createPromise() throws Exception {
        int databaseSizeBeforeCreate = promiseRepository.findAll().size();

        // Create the Promise

        restPromiseMockMvc.perform(post("/api/promises")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(promise)))
                .andExpect(status().isCreated());

        // Validate the Promise in the database
        List<Promise> promises = promiseRepository.findAll();
        assertThat(promises).hasSize(databaseSizeBeforeCreate + 1);
        Promise testPromise = promises.get(promises.size() - 1);
    }

    @Test
    public void getAllPromises() throws Exception {
        // Initialize the database
        promiseRepository.save(promise);

        // Get all the promises
        restPromiseMockMvc.perform(get("/api/promises?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(promise.getId())));
    }

    @Test
    public void getPromise() throws Exception {
        // Initialize the database
        promiseRepository.save(promise);

        // Get the promise
        restPromiseMockMvc.perform(get("/api/promises/{id}", promise.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(promise.getId()));
    }

    @Test
    public void getNonExistingPromise() throws Exception {
        // Get the promise
        restPromiseMockMvc.perform(get("/api/promises/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updatePromise() throws Exception {
        // Initialize the database
        promiseService.save(promise);

        int databaseSizeBeforeUpdate = promiseRepository.findAll().size();

        // Update the promise
        Promise updatedPromise = new Promise();
        updatedPromise.setId(promise.getId());

        restPromiseMockMvc.perform(put("/api/promises")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedPromise)))
                .andExpect(status().isOk());

        // Validate the Promise in the database
        List<Promise> promises = promiseRepository.findAll();
        assertThat(promises).hasSize(databaseSizeBeforeUpdate);
        Promise testPromise = promises.get(promises.size() - 1);
    }

    @Test
    public void deletePromise() throws Exception {
        // Initialize the database
        promiseService.save(promise);

        int databaseSizeBeforeDelete = promiseRepository.findAll().size();

        // Get the promise
        restPromiseMockMvc.perform(delete("/api/promises/{id}", promise.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Promise> promises = promiseRepository.findAll();
        assertThat(promises).hasSize(databaseSizeBeforeDelete - 1);
    }
}
