package com.vigo.cdm.web.rest;

import com.vigo.cdm.CdmApp;
import com.vigo.cdm.domain.FamilyActivity;
import com.vigo.cdm.repository.FamilyActivityRepository;
import com.vigo.cdm.service.FamilyActivityService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FamilyActivityResource REST controller.
 *
 * @see FamilyActivityResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CdmApp.class)
@WebAppConfiguration
@IntegrationTest
public class FamilyActivityResourceIntTest {


    @Inject
    private FamilyActivityRepository familyActivityRepository;

    @Inject
    private FamilyActivityService familyActivityService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFamilyActivityMockMvc;

    private FamilyActivity familyActivity;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FamilyActivityResource familyActivityResource = new FamilyActivityResource();
        ReflectionTestUtils.setField(familyActivityResource, "familyActivityService", familyActivityService);
        this.restFamilyActivityMockMvc = MockMvcBuilders.standaloneSetup(familyActivityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        familyActivityRepository.deleteAll();
        familyActivity = new FamilyActivity();
    }

    @Test
    public void createFamilyActivity() throws Exception {
        int databaseSizeBeforeCreate = familyActivityRepository.findAll().size();

        // Create the FamilyActivity

        restFamilyActivityMockMvc.perform(post("/api/family-activities")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(familyActivity)))
                .andExpect(status().isCreated());

        // Validate the FamilyActivity in the database
        List<FamilyActivity> familyActivities = familyActivityRepository.findAll();
        assertThat(familyActivities).hasSize(databaseSizeBeforeCreate + 1);
        FamilyActivity testFamilyActivity = familyActivities.get(familyActivities.size() - 1);
    }

    @Test
    public void getAllFamilyActivities() throws Exception {
        // Initialize the database
        familyActivityRepository.save(familyActivity);

        // Get all the familyActivities
        restFamilyActivityMockMvc.perform(get("/api/family-activities?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(familyActivity.getId())));
    }

    @Test
    public void getFamilyActivity() throws Exception {
        // Initialize the database
        familyActivityRepository.save(familyActivity);

        // Get the familyActivity
        restFamilyActivityMockMvc.perform(get("/api/family-activities/{id}", familyActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(familyActivity.getId()));
    }

    @Test
    public void getNonExistingFamilyActivity() throws Exception {
        // Get the familyActivity
        restFamilyActivityMockMvc.perform(get("/api/family-activities/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateFamilyActivity() throws Exception {
        // Initialize the database
        familyActivityService.save(familyActivity);

        int databaseSizeBeforeUpdate = familyActivityRepository.findAll().size();

        // Update the familyActivity
        FamilyActivity updatedFamilyActivity = new FamilyActivity();
        updatedFamilyActivity.setId(familyActivity.getId());

        restFamilyActivityMockMvc.perform(put("/api/family-activities")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedFamilyActivity)))
                .andExpect(status().isOk());

        // Validate the FamilyActivity in the database
        List<FamilyActivity> familyActivities = familyActivityRepository.findAll();
        assertThat(familyActivities).hasSize(databaseSizeBeforeUpdate);
        FamilyActivity testFamilyActivity = familyActivities.get(familyActivities.size() - 1);
    }

    @Test
    public void deleteFamilyActivity() throws Exception {
        // Initialize the database
        familyActivityService.save(familyActivity);

        int databaseSizeBeforeDelete = familyActivityRepository.findAll().size();

        // Get the familyActivity
        restFamilyActivityMockMvc.perform(delete("/api/family-activities/{id}", familyActivity.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<FamilyActivity> familyActivities = familyActivityRepository.findAll();
        assertThat(familyActivities).hasSize(databaseSizeBeforeDelete - 1);
    }
}
