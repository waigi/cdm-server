package com.vigo.cdm.web.rest;

import com.vigo.cdm.CdmApp;
import com.vigo.cdm.domain.Credit;
import com.vigo.cdm.repository.CreditRepository;
import com.vigo.cdm.service.CreditService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CreditResource REST controller.
 *
 * @see CreditResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CdmApp.class)
@WebAppConfiguration
@IntegrationTest
public class CreditResourceIntTest {

    private static final String DEFAULT_REVIEW = "AAAAA";
    private static final String UPDATED_REVIEW = "BBBBB";

    @Inject
    private CreditRepository creditRepository;

    @Inject
    private CreditService creditService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCreditMockMvc;

    private Credit credit;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CreditResource creditResource = new CreditResource();
        ReflectionTestUtils.setField(creditResource, "creditService", creditService);
        this.restCreditMockMvc = MockMvcBuilders.standaloneSetup(creditResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        creditRepository.deleteAll();
        credit = new Credit();
        credit.setReview(DEFAULT_REVIEW);
    }

    @Test
    public void createCredit() throws Exception {
        int databaseSizeBeforeCreate = creditRepository.findAll().size();

        // Create the Credit

        restCreditMockMvc.perform(post("/api/credits")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(credit)))
                .andExpect(status().isCreated());

        // Validate the Credit in the database
        List<Credit> credits = creditRepository.findAll();
        assertThat(credits).hasSize(databaseSizeBeforeCreate + 1);
        Credit testCredit = credits.get(credits.size() - 1);
        assertThat(testCredit.getReview()).isEqualTo(DEFAULT_REVIEW);
    }

    @Test
    public void getAllCredits() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        // Get all the credits
        restCreditMockMvc.perform(get("/api/credits?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(credit.getId())))
                .andExpect(jsonPath("$.[*].review").value(hasItem(DEFAULT_REVIEW.toString())));
    }

    @Test
    public void getCredit() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        // Get the credit
        restCreditMockMvc.perform(get("/api/credits/{id}", credit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(credit.getId()))
            .andExpect(jsonPath("$.review").value(DEFAULT_REVIEW.toString()));
    }

    @Test
    public void getNonExistingCredit() throws Exception {
        // Get the credit
        restCreditMockMvc.perform(get("/api/credits/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateCredit() throws Exception {
        // Initialize the database
        creditService.save(credit);

        int databaseSizeBeforeUpdate = creditRepository.findAll().size();

        // Update the credit
        Credit updatedCredit = new Credit();
        updatedCredit.setId(credit.getId());
        updatedCredit.setReview(UPDATED_REVIEW);

        restCreditMockMvc.perform(put("/api/credits")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCredit)))
                .andExpect(status().isOk());

        // Validate the Credit in the database
        List<Credit> credits = creditRepository.findAll();
        assertThat(credits).hasSize(databaseSizeBeforeUpdate);
        Credit testCredit = credits.get(credits.size() - 1);
        assertThat(testCredit.getReview()).isEqualTo(UPDATED_REVIEW);
    }

    @Test
    public void deleteCredit() throws Exception {
        // Initialize the database
        creditService.save(credit);

        int databaseSizeBeforeDelete = creditRepository.findAll().size();

        // Get the credit
        restCreditMockMvc.perform(delete("/api/credits/{id}", credit.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Credit> credits = creditRepository.findAll();
        assertThat(credits).hasSize(databaseSizeBeforeDelete - 1);
    }
}
