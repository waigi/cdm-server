package com.vigo.cdm.web.rest;

import com.vigo.cdm.CdmApp;
import com.vigo.cdm.domain.AccountFamily;
import com.vigo.cdm.repository.AccountFamilyRepository;
import com.vigo.cdm.service.AccountFamilyService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the AccountFamilyResource REST controller.
 *
 * @see AccountFamilyResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CdmApp.class)
@WebAppConfiguration
@IntegrationTest
public class AccountFamilyResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private AccountFamilyRepository accountFamilyRepository;

    @Inject
    private AccountFamilyService accountFamilyService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAccountFamilyMockMvc;

    private AccountFamily accountFamily;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AccountFamilyResource accountFamilyResource = new AccountFamilyResource();
        ReflectionTestUtils.setField(accountFamilyResource, "accountFamilyService", accountFamilyService);
        this.restAccountFamilyMockMvc = MockMvcBuilders.standaloneSetup(accountFamilyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        accountFamilyRepository.deleteAll();
        accountFamily = new AccountFamily();
        accountFamily.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createAccountFamily() throws Exception {
        int databaseSizeBeforeCreate = accountFamilyRepository.findAll().size();

        // Create the AccountFamily

        restAccountFamilyMockMvc.perform(post("/api/account-families")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(accountFamily)))
                .andExpect(status().isCreated());

        // Validate the AccountFamily in the database
        List<AccountFamily> accountFamilies = accountFamilyRepository.findAll();
        assertThat(accountFamilies).hasSize(databaseSizeBeforeCreate + 1);
        AccountFamily testAccountFamily = accountFamilies.get(accountFamilies.size() - 1);
        assertThat(testAccountFamily.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void getAllAccountFamilies() throws Exception {
        // Initialize the database
        accountFamilyRepository.save(accountFamily);

        // Get all the accountFamilies
        restAccountFamilyMockMvc.perform(get("/api/account-families?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(accountFamily.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    public void getAccountFamily() throws Exception {
        // Initialize the database
        accountFamilyRepository.save(accountFamily);

        // Get the accountFamily
        restAccountFamilyMockMvc.perform(get("/api/account-families/{id}", accountFamily.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(accountFamily.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    public void getNonExistingAccountFamily() throws Exception {
        // Get the accountFamily
        restAccountFamilyMockMvc.perform(get("/api/account-families/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateAccountFamily() throws Exception {
        // Initialize the database
        accountFamilyService.save(accountFamily);

        int databaseSizeBeforeUpdate = accountFamilyRepository.findAll().size();

        // Update the accountFamily
        AccountFamily updatedAccountFamily = new AccountFamily();
        updatedAccountFamily.setId(accountFamily.getId());
        updatedAccountFamily.setDescription(UPDATED_DESCRIPTION);

        restAccountFamilyMockMvc.perform(put("/api/account-families")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedAccountFamily)))
                .andExpect(status().isOk());

        // Validate the AccountFamily in the database
        List<AccountFamily> accountFamilies = accountFamilyRepository.findAll();
        assertThat(accountFamilies).hasSize(databaseSizeBeforeUpdate);
        AccountFamily testAccountFamily = accountFamilies.get(accountFamilies.size() - 1);
        assertThat(testAccountFamily.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void deleteAccountFamily() throws Exception {
        // Initialize the database
        accountFamilyService.save(accountFamily);

        int databaseSizeBeforeDelete = accountFamilyRepository.findAll().size();

        // Get the accountFamily
        restAccountFamilyMockMvc.perform(delete("/api/account-families/{id}", accountFamily.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<AccountFamily> accountFamilies = accountFamilyRepository.findAll();
        assertThat(accountFamilies).hasSize(databaseSizeBeforeDelete - 1);
    }
}
