package com.vigo.cdm.web.rest;

import com.vigo.cdm.CdmApp;
import com.vigo.cdm.domain.Guardian;
import com.vigo.cdm.repository.GuardianRepository;
import com.vigo.cdm.service.GuardianService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GuardianResource REST controller.
 *
 * @see GuardianResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CdmApp.class)
@WebAppConfiguration
@IntegrationTest
public class GuardianResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private GuardianRepository guardianRepository;

    @Inject
    private GuardianService guardianService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGuardianMockMvc;

    private Guardian guardian;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GuardianResource guardianResource = new GuardianResource();
        ReflectionTestUtils.setField(guardianResource, "guardianService", guardianService);
        this.restGuardianMockMvc = MockMvcBuilders.standaloneSetup(guardianResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        guardianRepository.deleteAll();
        guardian = new Guardian();
        guardian.setName(DEFAULT_NAME);
    }

    @Test
    public void createGuardian() throws Exception {
        int databaseSizeBeforeCreate = guardianRepository.findAll().size();

        // Create the Guardian

        restGuardianMockMvc.perform(post("/api/guardians")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(guardian)))
                .andExpect(status().isCreated());

        // Validate the Guardian in the database
        List<Guardian> guardians = guardianRepository.findAll();
        assertThat(guardians).hasSize(databaseSizeBeforeCreate + 1);
        Guardian testGuardian = guardians.get(guardians.size() - 1);
        assertThat(testGuardian.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    public void getAllGuardians() throws Exception {
        // Initialize the database
        guardianRepository.save(guardian);

        // Get all the guardians
        restGuardianMockMvc.perform(get("/api/guardians?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(guardian.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    public void getGuardian() throws Exception {
        // Initialize the database
        guardianRepository.save(guardian);

        // Get the guardian
        restGuardianMockMvc.perform(get("/api/guardians/{id}", guardian.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(guardian.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    public void getNonExistingGuardian() throws Exception {
        // Get the guardian
        restGuardianMockMvc.perform(get("/api/guardians/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateGuardian() throws Exception {
        // Initialize the database
        guardianService.save(guardian);

        int databaseSizeBeforeUpdate = guardianRepository.findAll().size();

        // Update the guardian
        Guardian updatedGuardian = new Guardian();
        updatedGuardian.setId(guardian.getId());
        updatedGuardian.setName(UPDATED_NAME);

        restGuardianMockMvc.perform(put("/api/guardians")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedGuardian)))
                .andExpect(status().isOk());

        // Validate the Guardian in the database
        List<Guardian> guardians = guardianRepository.findAll();
        assertThat(guardians).hasSize(databaseSizeBeforeUpdate);
        Guardian testGuardian = guardians.get(guardians.size() - 1);
        assertThat(testGuardian.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    public void deleteGuardian() throws Exception {
        // Initialize the database
        guardianService.save(guardian);

        int databaseSizeBeforeDelete = guardianRepository.findAll().size();

        // Get the guardian
        restGuardianMockMvc.perform(delete("/api/guardians/{id}", guardian.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Guardian> guardians = guardianRepository.findAll();
        assertThat(guardians).hasSize(databaseSizeBeforeDelete - 1);
    }
}
