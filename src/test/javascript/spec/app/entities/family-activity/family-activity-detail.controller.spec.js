'use strict';

describe('Controller Tests', function() {

    describe('FamilyActivity Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockFamilyActivity;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockFamilyActivity = jasmine.createSpy('MockFamilyActivity');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'FamilyActivity': MockFamilyActivity
            };
            createController = function() {
                $injector.get('$controller')("FamilyActivityDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'cdmApp:familyActivityUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
