'use strict';

describe('Controller Tests', function() {

    describe('Guardian Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockGuardian;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockGuardian = jasmine.createSpy('MockGuardian');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Guardian': MockGuardian
            };
            createController = function() {
                $injector.get('$controller')("GuardianDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'cdmApp:guardianUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
